-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2016 at 12:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_adjustments`
--

INSERT INTO `sma_adjustments` (`id`, `date`, `product_id`, `option_id`, `quantity`, `warehouse_id`, `note`, `created_by`, `updated_by`, `type`) VALUES
(1, '2016-07-14 13:40:00', 504, NULL, '50.0000', 1, '&lt;p&gt;Add Qty&lt;&sol;p&gt;', 1, NULL, 'addition');

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

CREATE TABLE `sma_calendar` (
  `date` date NOT NULL,
  `data` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_categories`
--

INSERT INTO `sma_categories` (`id`, `code`, `name`, `image`) VALUES
(2, '0101010100015', 'TEST', NULL),
(3, '0026010100012', 'TRADING', NULL),
(4, '0008010100005', 'Cotton Clothes', NULL),
(5, '0007010100004', 'Clothes', NULL),
(6, '0010010100007', 'SOKO', NULL),
(7, '0006010100003', 'Machines', NULL),
(8, '0024010100011', 'Computers', NULL),
(9, '0011010100008', 'Raw Material', NULL),
(10, '0012010100009', 'Four Wheeler', NULL),
(11, '0004010100001', 'Commercial Vehicle', NULL),
(12, '0061010100014', 'STATIONARY', NULL),
(13, '0016010100010', 'Two Wheeler', NULL),
(14, '0102010100016', 'TEST2', NULL),
(15, '0005010100002', 'Capital Items Group', NULL),
(16, '0009010100006', 'Silk Clothes', NULL),
(17, '0041010100013', 'NEW GROUP', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `upd_flg` tinyint(1) DEFAULT NULL,
  `eo_type` varchar(1) DEFAULT NULL,
  `eo_id` varchar(20) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `upd_flg`, `eo_type`, `eo_id`, `sync_flg`) VALUES
(1, 3, 'customer', 1, 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'New Delhi', 'Selangor', '46000', 'Malaysia', '0123456789', 'xyz@essindia.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(2, 4, 'supplier', NULL, NULL, 'Test Supplier', 'Supplier Company Name', NULL, 'Supplier Address', 'New Delhi', 'Selangor', '46050', 'Malaysia', '0123456789', 'xyz@essindia.com', '-', '-', '-', '-', '-', '-', NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(6, NULL, 'biller', NULL, NULL, ' ACME TELECOM', ' ACME TELECOM', NULL, '', 'New Delhi', NULL, NULL, 'Ghana', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(7, NULL, 'biller', NULL, NULL, ' ACME TELECOM', ' ACME TELECOM', NULL, '', 'New Delhi', NULL, NULL, 'Ghana', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(8, NULL, 'biller', NULL, NULL, ' ACME INDUSTRIES', ' ACME INDUSTRIES', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(9, NULL, 'biller', NULL, NULL, ' ACME INDUSTRIES', ' ACME INDUSTRIES', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(10, NULL, 'biller', NULL, NULL, ' ACME INDUSTRIES', ' ACME INDUSTRIES', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(11, NULL, 'biller', NULL, NULL, ' ACME INDUSTRIES', ' ACME INDUSTRIES', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(12, NULL, 'biller', NULL, NULL, ' ACME INDUSTRIES', ' ACME INDUSTRIES', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(13, NULL, 'biller', NULL, NULL, ' ACME CHEMICALS', ' ACME CHEMICALS', NULL, '', 'New Delhi', NULL, NULL, 'INDIA', '0123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(14, 3, 'customer', 1, 'General', 'WIPRO', 'ESS Group', NULL, 'SANT KABIR NAGAR\nHOUSE NO-231\nBELTHARA ROAD\nCity - GHAZIABAD \nState - UTTAR PRADESH \nINDIA\n221715', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '1', '1'),
(15, 3, 'customer', 1, 'General', 'AFRO INDUSTRIES LTD', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '2', '1'),
(16, 3, 'customer', 1, 'General', 'GHANA WATER CORPORATIONS', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '2', '1'),
(17, 3, 'customer', 1, 'General', 'INFOTECH CUSTOMER', 'ESS Group', NULL, 'TEST ADDRESS,\nCity - HONG KONG \nState - HONG KONG \nChina\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '3', '1'),
(18, 3, 'customer', 1, 'General', 'BAJAJ AUTO', 'ESS Group', NULL, 'TEST ADDRESS,\nCity - HONG KONG \nState - HONG KONG \nChina\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '3', '1'),
(19, 3, 'customer', 1, 'General', 'I CARE INDUSTRIES LTD', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '4', '1'),
(20, 3, 'customer', 1, 'General', 'FORD MOTOR COMPANY', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '4', '1'),
(21, 3, 'customer', 1, 'General', 'ACME INDUSTRIES LTD', 'ESS Group', '', 'B-65sector- 63City - NOIDA State - UTTAR PRADESH INDIA201317', 'New Delhi', '', '', '', '8123456789', 'xyz@essindia.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, NULL, 'C', '5', '1'),
(22, 3, 'customer', 1, 'General', 'INTERPLAST LTD', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '5', '1'),
(23, 3, 'customer', 1, 'General', 'TATA INDUSTRIES LTD', 'ESS Group', NULL, 'TEST ADDRESS,\nCity - HONG KONG \nState - HONG KONG \nChina\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '6', '1'),
(24, 3, 'customer', 1, 'General', 'Aquatouch Pvt LTD (C)', 'ESS Group', NULL, 'mjk hospital \nbettaih\nwest champaaran\nCity - BETTIAH \nState - BIHAR \nINDIA\n845438', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '26', '1'),
(25, 3, 'customer', 1, 'General', 'DANUBE BUILDMART', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '38', '1'),
(26, 3, 'customer', 1, 'General', 'AXIS BANK LTD', 'ESS Group', NULL, 'b-65. Sec 63\nNear Fortis \nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201301', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '55', '1'),
(27, 3, 'customer', 1, 'General', 'Amrit Enterprises', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '56', '1'),
(28, 3, 'customer', 1, 'General', 'ICICI BANK LIMITED-MEERUT', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '58', '1'),
(29, 3, 'customer', 1, 'General', 'AXIS BANK LTD 1001', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '62', '1'),
(30, 3, 'customer', 1, 'General', 'BIDCO LIMITED', 'ESS Group', NULL, 'a123\n234\nCity - KASHIPUR \nState - UTTARAKHAND \nINDIA\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '66', '1'),
(31, 3, 'customer', 1, 'General', 'AKS tech', 'ESS Group', NULL, 'mjk hospital \nbettaih\nwest champaaran\nCity - BETTIAH \nState - BIHAR \nINDIA\n845438', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '103', '1'),
(32, 3, 'customer', 1, 'General', 'RAHUL SINGH', 'ESS Group', NULL, 'SANT KABIR NAGAR\nHOUSE NO-231\nBELTHARA ROAD\nCity - GHAZIABAD \nState - UTTAR PRADESH \nINDIA\n221715', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '104', '1'),
(33, 3, 'customer', 1, 'General', 'RAMESH KUMAR', 'ESS Group', NULL, 'ADEKUNLE FAJUYI STREET\nCity - VI \nState - LAGOS \nNigeria\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '105', '1'),
(34, 3, 'customer', 1, 'General', 'ALOK SHARMA', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '107', '1'),
(35, 3, 'customer', 1, 'General', 'ALOK SHARMA', 'ESS Group', NULL, 'b-65. Sec 63\nNear Fortis \nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201301', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '107', '1'),
(36, 3, 'customer', 1, 'General', 'ANIMESH SINHA', 'ESS Group', NULL, 'ADEKUNLE FAJUYI STREET\nCity - VI \nState - LAGOS \nNigeria\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '108', '1'),
(37, 3, 'customer', 1, 'General', 'SHYAM GUPTA', 'ESS Group', NULL, 'B BLOCK, B-31\nIN FRONT OF FORTIS HOSPITAL\nBEHIND HALDIRAM FACTORY\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n220102', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '112', '1'),
(38, 3, 'customer', 1, 'General', 'ANI COMMUNICATION CONSIGNEE', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '119', '1'),
(39, 3, 'customer', 1, 'General', 'ANI FARMA CONSIGNEE', 'ESS Group', NULL, 'Sec 56,Patna Road\nBihar\nCity - PATNA \nState - BIHAR \nINDIA\n1203758', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '121', '1'),
(40, 3, 'customer', 1, 'General', 'ABC', 'ESS Group', NULL, 'MJK HOSPITAL HOSPITAL ROAD \nHOUSE NO -08\nBETTIAH\nCity - BETTIAH \nState - BIHAR \nINDIA\n805438', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '162', '1'),
(41, 3, 'customer', 1, 'General', 'WIPRO', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '1', '1'),
(42, 3, 'customer', 1, 'General', 'AFRO INDUSTRIES LTD', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '2', '1'),
(43, 3, 'customer', 1, 'General', 'GHANA WATER CORPORATIONS', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '2', '1'),
(44, 3, 'customer', 1, 'General', 'INFOTECH CUSTOMER', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '3', '1'),
(45, 3, 'customer', 1, 'General', 'BAJAJ AUTO', 'ESS Group', NULL, 'B-65\nsector- 63\nCity - NOIDA \nState - UTTAR PRADESH \nINDIA\n201317', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '3', '1'),
(46, 3, 'customer', 1, 'General', 'I CARE INDUSTRIES LTD', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - HASLINGDEN \nState - LANCASHIRE \nUnited Kingdom\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '4', '1'),
(47, 3, 'customer', 1, 'General', 'FORD MOTOR COMPANY', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - HASLINGDEN \nState - LANCASHIRE \nUnited Kingdom\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '4', '1'),
(48, 3, 'customer', 1, 'General', 'ACME INDUSTRIES LTD', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '5', '1'),
(49, 3, 'customer', 1, 'General', 'INTERPLAST LTD', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '5', '1'),
(50, 3, 'customer', 1, 'General', 'TATA INDUSTRIES LTD', 'ESS Group', NULL, 'TEST ADDRESS 1\nCity - Kakumono \nState - ACCRA \nGhana\n', 'New Delhi', NULL, NULL, NULL, '9123456789', 'xyz@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, NULL, 'C', '6', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_costing`
--

INSERT INTO `sma_costing` (`id`, `date`, `product_id`, `sale_item_id`, `sale_id`, `purchase_item_id`, `quantity`, `purchase_net_unit_cost`, `purchase_unit_cost`, `sale_net_unit_price`, `sale_unit_price`, `quantity_balance`, `inventory`, `overselling`, `option_id`) VALUES
(1, '2016-01-06', 1, 1, 1, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(2, '2016-01-28', 1, 2, 2, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(3, '2016-07-13', 3, 3, 3, 3, '1.0000', '27272.7273', '30000.0000', '31818.1800', '35000.0000', '49.0000', 1, 0, 0),
(4, '2016-07-13', 2, 4, 3, 1, '1.0000', '43636.3636', '48000.0000', '45454.5500', '50000.0000', '49.0000', 1, 0, 0),
(5, '2016-07-13', 2, 5, 4, 1, '1.0000', '43636.3636', '48000.0000', '45454.5500', '50000.0000', '48.0000', 1, 0, 0),
(6, '2016-07-13', 2, 6, 5, 1, '1.0000', '43636.3636', '48000.0000', '50000.0000', '55000.0000', '47.0000', 1, 0, 0),
(7, '2016-07-14', 504, 7, 6, 1024, '1.0000', '0.0000', '0.0000', '332.0000', '332.0000', '49.0000', 1, 0, NULL),
(8, '2016-07-15', 170, 8, 7, 170, '1.0000', '22.7273', '25.0000', '25.0000', '25.0000', '1.0000', 1, 0, 0),
(9, '2016-07-15', 170, 9, 8, 170, '0.0000', '22.7273', '25.0000', '4.5500', '5.0000', '0.0000', 1, 0, 0),
(10, '2016-07-15', 170, 10, 9, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '0.0000', 1, 0, 0),
(11, '2016-07-15', 170, 11, 10, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '0.0000', 1, 0, 0),
(12, '2016-07-15', 170, 12, 11, 170, '0.0000', '22.7273', '25.0000', '22.7300', '25.0000', '0.0000', 1, 0, 0),
(13, '2016-07-15', 170, 13, 12, 170, '0.0000', '22.7273', '25.0000', '13.6400', '15.0000', '0.0000', 1, 0, 0),
(14, '2016-07-15', 170, 14, 13, 170, '0.0000', '22.7273', '25.0000', '16.3600', '18.0000', '0.0000', 1, 0, 0),
(15, '2016-07-15', 170, 15, 14, 170, '0.0000', '22.7273', '25.0000', '13.6400', '15.0000', '0.0000', 1, 0, 0),
(16, '2016-07-15', 170, 16, 15, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '0.0000', 1, 0, 0),
(17, '2016-07-15', 170, 17, 16, 170, '0.0000', '22.7273', '25.0000', '9.0900', '10.0000', '0.0000', 1, 0, 0),
(18, '2016-07-15', 170, 18, 17, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '0.0000', 1, 0, 0),
(19, '2016-07-15', 170, 19, 18, 170, '0.0000', '22.7273', '25.0000', '16.3600', '18.0000', '0.0000', 1, 0, 0),
(20, '2016-07-15', 170, 20, 19, 170, '0.0000', '22.7273', '25.0000', '13.6400', '15.0000', '0.0000', 1, 0, 0),
(21, '2016-07-15', 170, 21, 20, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '0.0000', 1, 0, 0),
(22, '2016-07-15', 170, 22, 21, 170, '0.0000', '22.7273', '25.0000', '16.3600', '18.0000', '0.0000', 1, 0, 0),
(23, '2016-07-15', 45, 23, 22, 45, '1.0000', '23.6364', '26.0000', '14.5500', '16.0000', '3.0000', 1, 0, 0),
(24, '2016-07-15', 45, 24, 22, 45, '1.0000', '23.6364', '26.0000', '18.1800', '20.0000', '3.0000', 1, 0, 0),
(25, '2016-07-16', 170, 25, 23, 170, '0.0000', '22.7273', '25.0000', '13.6400', '15.0000', '98.0000', 1, 0, 0),
(26, '2016-07-16', 170, 26, 23, 170, '0.0000', '22.7273', '25.0000', '16.3600', '18.0000', '98.0000', 1, 0, 0),
(27, '2016-07-16', 170, 27, 23, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '98.0000', 1, 0, 0),
(28, '2016-07-16', 170, 28, 24, 170, '0.0000', '22.7273', '25.0000', '16.3600', '18.0000', '98.0000', 1, 0, 0),
(29, '2016-07-16', 170, 29, 24, 170, '0.0000', '22.7273', '25.0000', '17.2700', '19.0000', '98.0000', 1, 0, 0),
(30, '2016-07-16', 170, 30, 24, 170, '0.0000', '22.7273', '25.0000', '18.1800', '20.0000', '98.0000', 1, 0, 0),
(31, '2016-07-16', 170, 31, 25, 170, '0.0000', '22.7273', '25.0000', '22.7300', '25.0000', '98.0000', 1, 0, 0),
(32, '2016-07-16', 170, 32, 25, 170, '0.0000', '22.7273', '25.0000', '22.7300', '25.0000', '98.0000', 1, 0, 0),
(33, '2016-07-16', 170, 33, 26, 170, '1.0000', '22.7273', '25.0000', '13.6400', '15.0000', '99.0000', 1, 0, 0),
(34, '2016-07-16', 170, 34, 26, 170, '1.0000', '22.7273', '25.0000', '22.7300', '25.0000', '99.0000', 1, 0, 0),
(35, '2016-07-19', 174, 35, 27, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '9.0000', 1, 0, 0),
(36, '2016-07-19', 174, 36, 28, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '8.0000', 1, 0, 0),
(37, '2016-07-19', 174, 37, 29, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '7.0000', 1, 0, 0),
(38, '2016-07-19', 175, 38, 30, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '9.0000', 1, 0, 0),
(39, '2016-07-19', 175, 39, 31, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '8.0000', 1, 0, 0),
(40, '2016-07-19', 174, 40, 32, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '6.0000', 1, 0, 0),
(41, '2016-07-19', 175, 41, 33, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '7.0000', 1, 0, 0),
(42, '2016-07-19', 175, 42, 34, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '6.0000', 1, 0, 0),
(43, '2016-07-19', 175, 43, 35, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '5.0000', 1, 0, 0),
(44, '2016-07-19', 174, 44, 36, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '5.0000', 1, 0, 0),
(45, '2016-07-19', 174, 45, 37, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '4.0000', 1, 0, 0),
(46, '2016-07-19', 174, 46, 38, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '3.0000', 1, 0, 0),
(47, '2016-07-19', 174, 47, 39, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '2.0000', 1, 0, 0),
(48, '2016-07-19', 174, 48, 40, 174, '0.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(49, '2016-07-20', 174, 49, 41, 174, '0.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(50, '2016-07-20', 174, 50, 42, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '1.0000', 1, 0, 0),
(51, '2016-07-20', 177, 51, 43, 177, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '9.0000', 1, 0, 0),
(52, '2016-07-20', 177, 52, 43, 177, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '9.0000', 1, 0, 0),
(53, '2016-07-20', 177, 53, 43, 177, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '9.0000', 1, 0, 0),
(54, '2016-07-20', 177, 54, 43, 177, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '9.0000', 1, 0, 0),
(55, '2016-07-20', 177, 55, 43, 177, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '9.0000', 1, 0, 0),
(56, '2016-07-20', 174, 56, 43, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(57, '2016-07-20', 174, 57, 43, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(58, '2016-07-20', 174, 58, 43, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(59, '2016-07-20', 174, 59, 43, 174, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '0.0000', 1, 0, 0),
(60, '2016-07-20', 175, 60, 43, 175, '1.0000', '301.8182', '332.0000', '301.8200', '332.0000', '4.0000', 1, 0, 0),
(61, '2016-07-20', 185, 61, 43, NULL, '1.0000', NULL, NULL, '0.0000', '0.0000', NULL, 1, 1, NULL),
(62, '2016-07-20', 185, 62, 43, NULL, '1.0000', NULL, NULL, '0.0000', '0.0000', NULL, 1, 1, NULL),
(63, '2016-07-20', 185, 63, 43, NULL, '1.0000', NULL, NULL, '0.0000', '0.0000', NULL, 1, 1, NULL),
(64, '2016-07-20', 185, 64, 43, NULL, '1.0000', NULL, NULL, '0.0000', '0.0000', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_credit_voucher`
--

CREATE TABLE `sma_credit_voucher` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_credit_voucher`
--

INSERT INTO `sma_credit_voucher` (`id`, `date`, `card_no`, `value`, `customer_id`, `customer`, `balance`, `expiry`, `created_by`) VALUES
(2, '2016-07-19 09:15:27', '3710698030860265', '50000.0000', 19, 'ESS Group', '50000.0000', '2016-11-30', '1'),
(5, '2016-07-19 10:56:55', '7670627949073657', '60000.0000', 17, 'ESS Group', '4634.8000', '2016-11-30', '23'),
(6, '2016-07-20 09:44:58', '4112802851476964', '365.2000', 0, NULL, '365.2000', '2016-11-30', '23'),
(7, '2016-07-20 09:49:34', '4324070348246240', '365.2000', 17, 'ESS Group', '365.2000', '2016-10-22', '23'),
(8, '2016-07-20 09:53:40', '6018147142714694', '365.2000', 17, 'ESS Group', '0.0000', '2016-10-31', '23');

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `curr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_currencies`
--

INSERT INTO `sma_currencies` (`id`, `code`, `name`, `rate`, `auto_update`, `curr_id`) VALUES
(1, 'SUR', 'USSR ROUBLE', '1.0000', 0, 147),
(2, 'SAR', 'SAUDI RIYAL', '1.0000', 0, 136),
(3, 'TWD', 'NEW TAIWAN DOLLAR', '1.0000', 0, 158),
(4, 'MYR', 'RINGGIT', '1.0000', 0, 112),
(5, 'CNY', 'YUAN RENMINBI', '1.0000', 0, 34),
(6, 'QAR', 'QATARI RIYAL', '1.0000', 0, 131),
(7, 'KES', 'KENYAN SHILLING', '1.0000', 0, 80),
(8, 'NAD', 'NAMIBIA DOLLAR', '1.0000', 0, 114),
(9, 'XOF', 'WEST AFRICAN FRANC', '1.0000', 0, 173),
(10, 'INR', 'INDIAN RUPEE', '1.0000', 0, 73),
(11, 'OMR', 'RIAL OMANI', '1.0000', 0, 120),
(12, 'VND', 'DÔNG', '1.0000', 0, 167),
(13, 'ACU', 'ACU DOLLAR', '1.0000', 0, 179),
(14, 'GHC', 'GHANA CEDI', '1.0000', 0, 57),
(15, 'NGN', 'NAIRA', '1.0000', 0, 115),
(16, 'ETB', 'ETHIOPIAN BIRR', '1.0000', 0, 51),
(17, 'SGD', 'SINGAPORE DOLLAR', '1.0000', 0, 141),
(18, 'MUR', 'MAURITIUS RUPEE', '1.0000', 0, 107),
(19, 'AED', 'UAE DIRHAM', '1.0000', 0, 1),
(20, 'THB', 'BAHT', '1.0000', 0, 150),
(21, 'USD', 'US DOLLAR', '1.0000', 0, 163),
(22, 'ZAR', 'SOUTH AFRICAN RAND', '1.0000', 0, 177),
(23, 'IDR', 'RUPIAH', '1.0000', 0, 71),
(24, 'GBP', 'POUND STERLING', '1.0000', 0, 55),
(25, 'ACE', 'ACU EURO', '1.0000', 0, 180),
(26, 'UGS', 'UGANDAN SHILLING', '1.0000', 0, 162),
(27, 'ZMK', 'ZAMBIAN KWACHA', '1.0000', 0, 178),
(28, 'CDZ ', 'NEW ZAÏRE', '1.0000', 0, 30),
(29, 'JPY', 'YEN', '1.0000', 0, 79),
(30, 'HKD', 'HONG KONG DOLLAR', '1.0000', 0, 65),
(31, 'SDG', 'SUDANESE POUND', '1.0000', 0, 139),
(32, 'TZS', 'TANZANIAN SHILLING', '1.0000', 0, 159),
(33, 'CAD', 'CANADIAN DOLLAR', '1.0000', 0, 29),
(34, 'EUR', 'EURO', '1.0000', 0, 52),
(35, 'XAF', 'FRANC DE LA COMMUNAUTÉ FINANCIÈRE AFRICAINE', '1.0000', 0, 170);

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_groups`
--

CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_customer_groups`
--

INSERT INTO `sma_customer_groups` (`id`, `name`, `percent`) VALUES
(1, 'General', 0),
(2, 'Reseller', -5),
(3, 'Distributor', -15),
(4, 'New Customer (+10)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_date_format`
--

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_gift_cards`
--

INSERT INTO `sma_gift_cards` (`id`, `date`, `card_no`, `value`, `customer_id`, `customer`, `balance`, `expiry`, `created_by`) VALUES
(1, '2016-07-19 07:37:06', '4503455133966717', '5000.0000', 45, 'ESS Group', '5000.0000', '2016-10-20', '1'),
(2, '2016-07-19 10:15:57', '6783218314798729', '5000.0000', 47, 'ESS Group', '4634.8000', '2016-11-30', '23'),
(3, '2016-07-20 05:11:40', '3114224033161680', '5000.0000', 17, 'ESS Group', '4634.8000', '2016-12-31', '23'),
(4, '2016-07-20 07:33:07', '4380751779707239', '365.2000', 17, 'ESS Group', '365.2000', NULL, '23'),
(5, '2016-07-20 08:51:46', '6493520059895081', '365.2000', 17, 'ESS Group', '365.2000', '2016-07-09', '23'),
(6, '2016-07-20 08:59:20', '4757830159028921', '365.2000', NULL, NULL, '365.2000', NULL, '23'),
(7, '2016-07-20 09:22:48', '1632031494223536', '365.2000', 0, NULL, '365.2000', '2016-07-23', '23'),
(8, '2016-07-20 09:27:42', '6337622070298042', '365.2000', 0, NULL, '365.2000', NULL, '23'),
(9, '2016-07-20 09:28:08', '0915423129663762', '365.2000', 0, NULL, '365.2000', '2016-07-30', '23');

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

CREATE TABLE `sma_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_groups`
--

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(5, 'sales', 'Sales Staff'),
(6, 'manager', 'manager');

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_migrations`
--

INSERT INTO `sma_migrations` (`version`) VALUES
(308);

-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_order_ref`
--

INSERT INTO `sma_order_ref` (`ref_id`, `date`, `so`, `qu`, `po`, `to`, `pos`, `do`, `pay`, `re`, `ex`) VALUES
(1, '2015-03-01', 1, 1, 1, 1, 44, 1, 92, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cv_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_payments`
--

INSERT INTO `sma_payments` (`id`, `date`, `sale_id`, `return_id`, `purchase_id`, `reference_no`, `transaction_id`, `paid_by`, `cheque_no`, `cc_no`, `cv_no`, `cc_holder`, `cc_month`, `cc_year`, `cc_type`, `amount`, `currency`, `created_by`, `attachment`, `type`, `note`, `pos_paid`, `pos_balance`) VALUES
(1, '2016-01-06 05:41:41', 1, NULL, NULL, 'IPAY/2016/01/0001', NULL, 'cash', '', '', NULL, '', '', '', '', '1083.5000', NULL, 1, NULL, 'received', '', '1083.5000', '0.0000'),
(2, '2016-01-28 08:08:11', 2, NULL, NULL, 'IPAY/2016/01/0003', NULL, 'cash', '', '', NULL, '', '', '', '', '100.0000', NULL, 1, NULL, 'received', 'cash', '100.0000', '-900.0000'),
(3, '2016-07-13 13:07:55', 3, NULL, NULL, 'IPAY/2016/07/0005', NULL, 'cash', '', '', NULL, '', '', '', '', '84990.0000', NULL, 1, NULL, 'received', '', '84990.0000', '0.0000'),
(4, '2016-07-13 13:20:49', 4, NULL, NULL, 'IPAY/2016/07/0007', NULL, 'cash', '', '', NULL, '', '', '', '', '50000.0000', NULL, 2, NULL, 'received', '', '50000.0000', '0.0000'),
(5, '2016-07-13 13:30:45', 5, NULL, NULL, 'IPAY/2016/07/0009', NULL, 'cash', '', '', NULL, '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000'),
(6, '2016-07-14 16:18:23', 6, NULL, NULL, 'IPAY/2016/07/0012', NULL, 'cash', '', '', NULL, '', '', '', '', '322.0000', NULL, 1, NULL, 'received', '', '322.0000', '0.0000'),
(7, '2016-07-15 11:51:42', 7, NULL, NULL, 'IPAY/2016/07/0018', NULL, 'cash', '', '', NULL, '', '', '', '', '27.5000', NULL, 1, NULL, 'received', '', '27.5000', '0.0000'),
(8, '2016-07-15 10:54:56', 8, NULL, NULL, 'IPAY/2016/07/0021', NULL, 'cash', '', '', NULL, '', '', '', '', '5.5000', NULL, 18, NULL, 'received', '', '5.5000', '0.0000'),
(9, '2016-07-15 11:08:11', 8, 1, NULL, 'IPAY/2016/07/0023', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '5.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(10, '2016-07-15 11:18:54', 9, NULL, NULL, 'IPAY/2016/07/0023', NULL, 'cash', '', '', NULL, '', '', '', '', '22.0000', NULL, 18, NULL, 'received', '', '27.5000', '5.5000'),
(11, '2016-07-15 11:39:26', 9, 2, NULL, 'IPAY/2016/07/0025', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '22.0000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(12, '2016-07-15 11:53:37', 10, NULL, NULL, 'IPAY/2016/07/0025', NULL, 'cash', '', '', NULL, '', '', '', '', '22.0000', NULL, 18, NULL, 'received', '', '27.5000', '5.5000'),
(13, '2016-07-15 11:54:12', 10, 3, NULL, 'IPAY/2016/07/0027', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '22.0000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(14, '2016-07-15 11:55:45', 11, NULL, NULL, 'IPAY/2016/07/0027', NULL, 'cash', '', '', NULL, '', '', '', '', '27.5000', NULL, 18, NULL, 'received', '', '27.5000', '0.0000'),
(15, '2016-07-15 12:06:23', 11, 4, NULL, 'IPAY/2016/07/0029', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '27.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(16, '2016-07-15 12:07:31', 12, NULL, NULL, 'IPAY/2016/07/0029', NULL, 'cash', '', '', NULL, '', '', '', '', '16.5000', NULL, 18, NULL, 'received', '', '16.5000', '0.0000'),
(17, '2016-07-15 12:22:26', 12, 5, NULL, 'IPAY/2016/07/0031', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '16.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(18, '2016-07-15 12:23:55', 13, NULL, NULL, 'IPAY/2016/07/0031', NULL, 'cash', '', '', NULL, '', '', '', '', '19.8000', NULL, 18, NULL, 'received', '', '19.8000', '0.0000'),
(19, '2016-07-15 12:40:10', 13, 6, NULL, 'IPAY/2016/07/0033', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '19.8000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(20, '2016-07-15 12:45:33', 14, NULL, NULL, 'IPAY/2016/07/0033', NULL, 'cash', '', '', NULL, '', '', '', '', '16.5000', NULL, 18, NULL, 'received', '', '16.5000', '0.0000'),
(21, '2016-07-15 12:57:49', 14, 7, NULL, 'IPAY/2016/07/0035', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '16.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(22, '2016-07-15 13:05:29', 15, NULL, NULL, 'IPAY/2016/07/0035', NULL, 'cash', '', '', NULL, '', '', '', '', '22.0000', NULL, 18, NULL, 'received', '', '22.0000', '0.0000'),
(23, '2016-07-15 13:06:31', 15, 8, NULL, 'IPAY/2016/07/0037', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '22.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(24, '2016-07-15 13:07:47', 16, NULL, NULL, 'IPAY/2016/07/0037', NULL, 'cash', '', '', NULL, '', '', '', '', '11.0000', NULL, 18, NULL, 'received', '', '11.0000', '0.0000'),
(25, '2016-07-15 13:09:15', 16, 9, NULL, 'IPAY/2016/07/0039', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '12.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(26, '2016-07-15 13:10:36', 17, NULL, NULL, 'IPAY/2016/07/0039', NULL, 'cash', '', '', NULL, '', '', '', '', '22.0000', NULL, 18, NULL, 'received', '', '22.0000', '0.0000'),
(27, '2016-07-15 13:16:42', 17, 10, NULL, 'IPAY/2016/07/0041', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '22.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(28, '2016-07-15 13:18:47', 18, NULL, NULL, 'IPAY/2016/07/0041', NULL, 'cash', '', '', NULL, '', '', '', '', '19.8000', NULL, 18, NULL, 'received', '', '19.8000', '0.0000'),
(29, '2016-07-15 13:22:13', 18, 11, NULL, 'IPAY/2016/07/0043', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '20.5000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(30, '2016-07-15 13:30:05', 19, NULL, NULL, 'IPAY/2016/07/0043', NULL, 'cash', '', '', NULL, '', '', '', '', '15.0000', NULL, 18, NULL, 'received', '', '15.0000', '0.0000'),
(31, '2016-07-15 13:30:33', 19, 12, NULL, 'IPAY/2016/07/0045', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '15.0000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(32, '2016-07-15 13:35:31', 20, NULL, NULL, 'IPAY/2016/07/0045', NULL, 'cash', '', '', NULL, '', '', '', '', '20.0000', NULL, 18, NULL, 'received', '', '20.0000', '0.0000'),
(33, '2016-07-15 13:36:41', 20, 13, NULL, 'IPAY/2016/07/0047', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '20.0000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(34, '2016-07-15 13:37:20', 21, NULL, NULL, 'IPAY/2016/07/0047', NULL, 'cash', '', '', NULL, '', '', '', '', '19.8000', NULL, 18, NULL, 'received', '', '19.8000', '0.0000'),
(35, '2016-07-15 14:03:16', 21, 14, NULL, 'IPAY/2016/07/0049', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '19.8000', NULL, 18, NULL, 'returned', NULL, '0.0000', '0.0000'),
(36, '2016-07-15 14:09:38', 22, NULL, NULL, 'IPAY/2016/07/0050', NULL, 'cash', '', '', NULL, '', '', '', '', '39.6000', NULL, 18, NULL, 'received', '', '39.6000', '0.0000'),
(37, '2016-07-16 05:16:29', 23, NULL, NULL, 'IPAY/2016/07/0053', NULL, 'cash', '', '', NULL, '', '', '', '', '58.3000', NULL, 1, NULL, 'received', '', '58.3000', '0.0000'),
(38, '2016-07-16 05:20:00', 23, 15, NULL, 'IPAY/2016/07/0055', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '58.3000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000'),
(39, '2016-07-16 05:23:22', 24, NULL, NULL, 'IPAY/2016/07/0055', NULL, 'cash', '', '', NULL, '', '', '', '', '62.7000', NULL, 1, NULL, 'received', '', '62.7000', '0.0000'),
(40, '2016-07-16 05:24:00', 24, 16, NULL, 'IPAY/2016/07/0057', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '62.7000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000'),
(41, '2016-07-16 05:28:09', 25, NULL, NULL, 'IPAY/2016/07/0057', NULL, 'cash', '', '', NULL, '', '', '', '', '55.0000', NULL, 1, NULL, 'received', '', '55.0000', '0.0000'),
(42, '2016-07-16 05:28:00', 25, 17, NULL, 'IPAY/2016/07/0059', NULL, 'cash', '', '', NULL, '', '', '', 'Visa', '55.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000'),
(43, '2016-07-16 09:12:45', 26, NULL, NULL, 'IPAY/2016/07/0059', NULL, 'cash', '', '', NULL, '', '', '', '', '44.0000', NULL, 18, NULL, 'received', '', '44.0000', '0.0000'),
(44, '2016-07-19 10:18:18', 27, NULL, NULL, 'IPAY/2016/07/0061', NULL, 'gift_card', '', '6783218314798729', NULL, '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '5000.0000', '4634.8000'),
(45, '2016-07-19 10:58:30', 28, NULL, NULL, 'IPAY/2016/07/0063', NULL, 'credit_voucher', '', '', NULL, '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '60000.0000', '59634.8000'),
(46, '2016-07-19 11:44:56', 29, NULL, NULL, 'IPAY/2016/07/0065', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(47, '2016-07-19 11:51:55', 30, NULL, NULL, 'IPAY/2016/07/0067', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(48, '2016-07-19 12:01:35', 31, NULL, NULL, 'IPAY/2016/07/0069', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(49, '2016-07-19 12:02:56', 32, NULL, NULL, 'IPAY/2016/07/0071', NULL, 'cash', '', '', '', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(50, '2016-07-19 12:27:32', 33, NULL, NULL, 'IPAY/2016/07/0073', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(51, '2016-07-19 12:29:55', 34, NULL, NULL, 'IPAY/2016/07/0075', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(52, '2016-07-19 12:36:25', 35, NULL, NULL, 'IPAY/2016/07/0077', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(53, '2016-07-19 12:43:38', 40, NULL, NULL, 'IPAY/2016/07/0084', NULL, 'credit_voucher', '', '', '7670627949073657', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(54, '2016-07-20 05:12:54', 41, NULL, NULL, 'IPAY/2016/07/0086', NULL, 'gift_card', '', '3114224033161680', '', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(55, '2016-07-20 05:21:51', 41, 18, NULL, 'IPAY/2016/07/0088', NULL, 'gift_card', '', '', NULL, '', '', '', 'Visa', '365.2000', NULL, 23, NULL, 'returned', NULL, '0.0000', '0.0000'),
(56, '2016-07-20 09:54:16', 40, 19, NULL, 'IPAY/2016/07/0088', NULL, 'credit_voucher', '', '', NULL, '', '', '', 'Visa', '365.2000', NULL, 23, NULL, 'returned', NULL, '0.0000', '0.0000'),
(57, '2016-07-20 10:05:26', 42, NULL, NULL, 'IPAY/2016/07/0088', NULL, 'credit_voucher', '', '', '6018147142714694', '', '', '', '', '365.2000', NULL, 23, NULL, 'received', '', '365.2000', '0.0000'),
(58, '2016-07-20 10:30:48', 43, NULL, NULL, 'IPAY/2016/07/0090', NULL, 'cash', '', '', '', '', '', '', '', '12826.0000', NULL, 23, NULL, 'received', '', '12826.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_paypal`
--

INSERT INTO `sma_paypal` (`id`, `active`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `till-addTill` tinyint(1) NOT NULL DEFAULT '0',
  `till-manageTill` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_permissions`
--

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`, `till-addTill`, `till-manageTill`) VALUES
(1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 1, NULL, 1, 0, 0),
(2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_register`
--

INSERT INTO `sma_pos_register` (`id`, `date`, `user_id`, `cash_in_hand`, `status`, `total_cash`, `total_cheques`, `total_cc_slips`, `total_cash_submitted`, `total_cheques_submitted`, `total_cc_slips_submitted`, `note`, `closed_at`, `transfer_opened_bills`, `closed_by`) VALUES
(1, '2016-01-06 04:47:14', 1, '10000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2016-07-13 13:20:26', 2, '1000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2016-07-14 16:50:26', 18, '10000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '2016-07-16 11:04:40', 15, '1000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2016-07-18 10:33:20', 20, '1000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '2016-07-19 04:59:27', 23, '1000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.0.1.21'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_settings`
--

INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`) VALUES
(1, 22, 20, 15, 1, 6, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', 1, 'BIXOLON SRP-350II, BIXOLON SRP-350II', 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.0.1.21');

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '1.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `sr_no` varchar(20) DEFAULT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `trsfr_flg` varchar(1) DEFAULT NULL,
  `tupd_flg` varchar(1) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_products`
--

INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(1, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000004', 3, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(2, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000005', 3, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(3, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000007', 3, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(4, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000001', 3, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(5, 'NEW..0000003', 'Das Bags', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000003', 3, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(6, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'TEST ITEM1', 3, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(7, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'NEW..0000006', 3, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', '1', NULL, NULL),
(8, 'NEW..0000003', 'Das Bags', '10', '0.0000', '602.0000', '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, '257000.0000', 3, 1, 'NEW..0000003', 4, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(9, 'NEW..0000003', 'Das Bags', '10', '0.0000', '602.0000', '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, '257000.0000', 3, 1, 'NEW..0000003', 4, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(10, 'NEW..0000003', 'Das Bags', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 3, 1, 'NEW..0000003', 4, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(11, 'NEW..0000003', 'Das Bags', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 3, 1, 'NEW..0000003', 4, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(12, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000004', 4, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(13, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000004', 4, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(14, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000005', 4, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(15, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000005', 4, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(16, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000007', 4, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(17, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000007', 4, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(18, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000001', 4, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(19, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000001', 4, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(20, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'TEST ITEM1', 4, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(21, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'TEST ITEM1', 4, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(22, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000006', 4, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(23, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'NEW..0000006', 4, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', '1', NULL, NULL),
(24, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000004', 5, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(25, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000005', 5, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(26, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000007', 5, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(27, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000001', 5, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(28, 'NEW..0000003', 'Das Bags', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000003', 5, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(29, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'TEST ITEM1', 5, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(30, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 'NEW..0000006', 5, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', '1', NULL, NULL),
(31, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000004', 6, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(32, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000005', 6, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(33, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000007', 6, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(34, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000001', 6, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(35, 'NEW..0000003', 'Das Bags', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000003', 6, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(36, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'TEST ITEM1', 6, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(37, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'NEW..0000006', 6, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', '1', NULL, NULL),
(38, 'NEW..0000004', 'COOKER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000004', 7, 'NEW..0000004', NULL, 'NEW..0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(39, 'NEW..0000005', 'GK BOOKS', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000005', 7, 'NEW..0000005', NULL, 'NEW..0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(40, 'NEW..0000007', 'Das Bags Green', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000007', 7, 'NEW..0000007', NULL, 'NEW..0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(41, 'NEW..0000001', 'ITEM 5', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000001', 7, 'NEW..0000001', NULL, 'NEW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(42, 'NEW..0000003', 'Das Bags', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000003', 7, 'NEW..0000003', NULL, 'NEW..0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(43, 'NEW..0000002', 'TEST ITEM1', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'TEST ITEM1', 7, 'TEST ITEM1', NULL, 'TEST ITEM1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(44, 'NEW..0000006', 'SCOOTER', '10', NULL, NULL, '1.0000', 'no_image.png', 17, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 'NEW..0000006', 7, 'NEW..0000006', NULL, 'NEW..0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', '1', NULL, NULL),
(45, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '26.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 10, 1, 'CC-RD-0000001', 3, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0008010100005', '1', NULL, NULL),
(46, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '502.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(47, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '502.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(48, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1135.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(49, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1135.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(50, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '854.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(51, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '854.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(52, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '6.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(53, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '6.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(54, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(55, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(56, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(57, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(58, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(59, 'CC-RD-0000001', 'Cotton T- Shirt', '10', '0.0000', '1000.0000', '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 11, 1, 'CC-RD-0000001', 4, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0008010100005', '1', NULL, NULL),
(60, 'CC-RD-0000001', 'Cotton T- Shirt', '10', NULL, NULL, '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 1, 'CC-RD-0000001', 5, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0008010100005', '1', NULL, NULL),
(61, 'CC-RD-0000001', 'Cotton T- Shirt', '10', NULL, NULL, '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 1, 'CC-RD-0000001', 6, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0008010100005', '1', NULL, NULL),
(62, 'CC-RD-0000001', 'Cotton T- Shirt', '10', NULL, NULL, '1.0000', 'no_image.png', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 1, 'CC-RD-0000001', 7, 'CC-RD-0000001', NULL, 'CC-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0008010100005', '1', NULL, NULL),
(63, 'SC-M-0000001', 'Silk Suit', '10', '0.0000', '48.0000', '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 18, 1, 'SC-M-0000001', 3, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0009010100006', '1', NULL, NULL),
(64, 'SC-M-0000001', 'Silk Suit', '10', '0.0000', '1905.0000', '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 18, 1, 'SC-M-0000001', 3, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0009010100006', '1', NULL, NULL),
(65, 'SC-M-0000001', 'Silk Suit', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, '6.0000', 19, 1, 'SC-M-0000001', 4, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0009010100006', '1', NULL, NULL),
(66, 'SC-M-0000001', 'Silk Suit', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, '6.0000', 19, 1, 'SC-M-0000001', 4, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0009010100006', '1', NULL, NULL),
(67, 'SC-M-0000001', 'Silk Suit', '10', NULL, NULL, '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 1, 'SC-M-0000001', 5, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0009010100006', '1', NULL, NULL),
(68, 'SC-M-0000001', 'Silk Suit', '10', NULL, NULL, '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, 1, 'SC-M-0000001', 6, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0009010100006', '1', NULL, NULL),
(69, 'SC-M-0000001', 'Silk Suit', '10', NULL, NULL, '1.0000', 'no_image.png', 16, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 1, 'SC-M-0000001', 7, 'SC-M-0000001', NULL, 'SC-M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0009010100006', '1', NULL, NULL),
(70, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26, 1, 'ITM..GRN.0000001', 3, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0102010100016', '1', NULL, NULL),
(71, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27, 1, 'ITM..GRN.0000001', 4, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0102010100016', '1', NULL, NULL),
(72, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27, 1, 'ITM..GRN.0000001', 4, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0102010100016', '1', NULL, NULL),
(73, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 28, 1, 'ITM..GRN.0000001', 5, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0102010100016', '1', NULL, NULL),
(74, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 1, 'ITM..GRN.0000001', 6, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0102010100016', '1', NULL, NULL),
(75, 'ITM..GRN.0000001', 'TEST2', '10', NULL, NULL, '1.0000', 'no_image.png', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 1, 'ITM..GRN.0000001', 7, 'ITM..GRN.0000001', NULL, 'ITM..GRN.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0102010100016', '1', NULL, NULL),
(76, 'COMP..0000003', 'MOULD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'MOULD', 3, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(77, 'COMP-0000008', 'charger', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP-0000008', 3, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(78, 'COMP..0000001', 'HP ProBook', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP..0000001', 3, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(79, 'COMP-0000003', 'KEYBOARD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'KEYBOARD', 3, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(80, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP-0000006', 3, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(81, 'COMP-0000002', 'CD/ROM', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'CD/ROM', 3, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(82, 'COMP-0000001', 'RAM 2GB', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'RAM 2GB', 3, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(83, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP-0000007', 3, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(84, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP-0000005', 3, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(85, 'COMP..0000002', 'Lenevo 201', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'COMP..0000002', 3, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(86, 'COMP-0000004', 'MOUSE', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 'MOUSE', 3, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', '1', NULL, NULL),
(87, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '26.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(88, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '26.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(89, 'COMP..0000001', 'HP ProBook', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 35, 1, 'COMP..0000001', 4, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(90, 'COMP..0000001', 'HP ProBook', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 35, 1, 'COMP..0000001', 4, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(91, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(92, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(93, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(94, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(95, 'COMP..0000001', 'HP ProBook', '10', '0.0000', '1258.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'COMP..0000001', 4, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(96, 'COMP..0000001', 'HP ProBook', '10', '0.0000', '1258.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'COMP..0000001', 4, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(97, 'COMP..0000003', 'MOULD', '10', '0.0000', '26842.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 35, 1, 'MOULD', 4, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(98, 'COMP..0000003', 'MOULD', '10', '0.0000', '26842.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 35, 1, 'MOULD', 4, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(99, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '250.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(100, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '250.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(101, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(102, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(103, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '26.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(104, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '26.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(105, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(106, 'COMP-0000004', 'MOUSE', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(107, 'COMP..0000002', 'Lenevo 201', '10', '0.0000', '1258.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'COMP..0000002', 4, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(108, 'COMP..0000002', 'Lenevo 201', '10', '0.0000', '1258.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'COMP..0000002', 4, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(109, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '171.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(110, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '171.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(111, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '8.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(112, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '8.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(113, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '105093.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(114, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '105093.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(115, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '251.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(116, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '251.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(117, 'COMP-0000008', 'charger', '10', '0.0000', '3012.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 35, 1, 'COMP-0000008', 4, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(118, 'COMP-0000008', 'charger', '10', '0.0000', '3012.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 35, 1, 'COMP-0000008', 4, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(119, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '250.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(120, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '250.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(121, 'COMP-0000004', 'MOUSE', '10', '0.0000', '46.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '11.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(122, 'COMP-0000004', 'MOUSE', '10', '0.0000', '46.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '11.0000', 35, 1, 'MOUSE', 4, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(123, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '151.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(124, 'COMP-0000001', 'RAM 2GB', '10', '0.0000', '151.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'RAM 2GB', 4, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(125, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '284.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(126, 'COMP-0000003', 'KEYBOARD', '10', '0.0000', '284.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'KEYBOARD', 4, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(127, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '30.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(128, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '30.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(129, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '10010.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(130, 'COMP-0000002', 'CD/ROM', '10', '0.0000', '10010.0000', '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 35, 1, 'CD/ROM', 4, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(131, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000006', 4, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(132, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000006', 4, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(133, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000007', 4, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(134, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000007', 4, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(135, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000005', 4, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(136, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 'COMP-0000005', 4, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', '1', NULL, NULL),
(137, 'COMP..0000003', 'MOULD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'MOULD', 5, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(138, 'COMP-0000008', 'charger', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP-0000008', 5, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(139, 'COMP..0000001', 'HP ProBook', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP..0000001', 5, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(140, 'COMP-0000003', 'KEYBOARD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'KEYBOARD', 5, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(141, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP-0000006', 5, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(142, 'COMP-0000002', 'CD/ROM', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'CD/ROM', 5, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(143, 'COMP-0000001', 'RAM 2GB', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'RAM 2GB', 5, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(144, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP-0000007', 5, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(145, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP-0000005', 5, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(146, 'COMP..0000002', 'Lenevo 201', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'COMP..0000002', 5, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(147, 'COMP-0000004', 'MOUSE', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 'MOUSE', 5, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', '1', NULL, NULL),
(148, 'COMP..0000003', 'MOULD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'MOULD', 6, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(149, 'COMP-0000008', 'charger', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP-0000008', 6, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(150, 'COMP..0000001', 'HP ProBook', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP..0000001', 6, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(151, 'COMP-0000003', 'KEYBOARD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'KEYBOARD', 6, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(152, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP-0000006', 6, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(153, 'COMP-0000002', 'CD/ROM', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'CD/ROM', 6, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(154, 'COMP-0000001', 'RAM 2GB', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'RAM 2GB', 6, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(155, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP-0000007', 6, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(156, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP-0000005', 6, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(157, 'COMP..0000002', 'Lenevo 201', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'COMP..0000002', 6, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(158, 'COMP-0000004', 'MOUSE', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 'MOUSE', 6, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', '1', NULL, NULL),
(159, 'COMP..0000003', 'MOULD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'MOULD', 7, 'MOULD', NULL, 'MOULD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(160, 'COMP-0000008', 'charger', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP-0000008', 7, 'COMP-0000008', NULL, 'COMP-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(161, 'COMP..0000001', 'HP ProBook', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP..0000001', 7, 'COMP..0000001', NULL, 'COMP..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(162, 'COMP-0000003', 'KEYBOARD', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'KEYBOARD', 7, 'KEYBOARD', NULL, 'KEYBOARD', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(163, 'COMP-0000006', 'HP computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP-0000006', 7, 'COMP-0000006', NULL, 'COMP-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(164, 'COMP-0000002', 'CD/ROM', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'CD/ROM', 7, 'CD/ROM', NULL, 'CD/ROM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(165, 'COMP-0000001', 'RAM 2GB', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'RAM 2GB', 7, 'RAM 2GB', NULL, 'RAM 2GB', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(166, 'COMP-0000007', 'wip computer', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP-0000007', 7, 'COMP-0000007', NULL, 'COMP-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(167, 'COMP-0000005', 'Ram', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP-0000005', 7, 'COMP-0000005', NULL, 'COMP-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(168, 'COMP..0000002', 'Lenevo 201', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'COMP..0000002', 7, 'COMP..0000002', NULL, 'COMP..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(169, 'COMP-0000004', 'MOUSE', '10', NULL, NULL, '1.0000', 'no_image.png', 8, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 'MOUSE', 7, 'MOUSE', NULL, 'MOUSE', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', '1', NULL, NULL),
(170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', '0.0000', '25.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '98.0000', 42, 1, 'CIG-L-0000001', 3, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0005010100002', '1', NULL, NULL),
(171, 'CIG-BLK-0000001', 'Computer', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, 'CIG-BLK-0000001', 3, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', '1', NULL, NULL),
(172, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, 'CIG-BLK-0000002', 3, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', '1', NULL, NULL),
(173, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, 'CIG-0000001', 3, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', '1', NULL, NULL),
(174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '-3.0000', 43, 1, 'CIG-L-0000001', 4, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 43, 1, 'CIG-L-0000001', 4, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(176, 'CIG-BLK-0000001', 'Computer', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 43, 1, 'CIG-BLK-0000001', 4, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(177, 'CIG-BLK-0000001', 'Computer', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 43, 1, 'CIG-BLK-0000001', 4, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(178, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', '0.0000', '14174.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 43, 1, 'CIG-0000001', 4, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(179, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', '0.0000', '14174.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 43, 1, 'CIG-0000001', 4, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(180, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', '0.0000', '13213.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 43, 1, 'CIG-L-0000001', 4, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(181, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', '0.0000', '13213.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 43, 1, 'CIG-L-0000001', 4, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(182, 'CIG-BLK-0000001', 'Computer', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 43, 1, 'CIG-BLK-0000001', 4, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(183, 'CIG-BLK-0000001', 'Computer', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 43, 1, 'CIG-BLK-0000001', 4, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(184, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43, 1, 'CIG-BLK-0000002', 4, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(185, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, '-1.0000', 43, 1, 'CIG-BLK-0000002', 4, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0005010100002', '1', NULL, NULL),
(186, 'CIG-BLK-0000001', 'Computer', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, 'CIG-BLK-0000001', 5, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', '1', NULL, NULL),
(187, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, 'CIG-BLK-0000002', 5, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', '1', NULL, NULL),
(188, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, 'CIG-0000001', 5, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', '1', NULL, NULL),
(189, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, 'CIG-L-0000001', 5, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', '1', NULL, NULL),
(190, 'CIG-BLK-0000001', 'Computer', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 1, 'CIG-BLK-0000001', 6, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', '1', NULL, NULL),
(191, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 1, 'CIG-BLK-0000002', 6, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', '1', NULL, NULL),
(192, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 1, 'CIG-0000001', 6, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', '1', NULL, NULL),
(193, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 1, 'CIG-L-0000001', 6, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', '1', NULL, NULL),
(194, 'CIG-BLK-0000001', 'Computer', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 'CIG-BLK-0000001', 7, 'CIG-BLK-0000001', NULL, 'CIG-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', '1', NULL, NULL),
(195, 'CIG-BLK-0000002', 'Motorcycle', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 'CIG-BLK-0000002', 7, 'CIG-BLK-0000002', NULL, 'CIG-BLK-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', '1', NULL, NULL),
(196, 'CIG-0000001', 'WALL PLATE 1100X3000MM', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 'CIG-0000001', 7, 'CIG-0000001', NULL, 'CIG-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', '1', NULL, NULL),
(197, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', '10', NULL, NULL, '1.0000', 'no_image.png', 15, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 'CIG-L-0000001', 7, 'CIG-L-0000001', NULL, 'CIG-L-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', '1', NULL, NULL),
(198, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', '0.0000', '751.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '40.0000', 50, 1, 'RM-G-0000002', 3, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(199, 'RM-0000005', 'Test Item Manul', '10', '0.0000', '2333.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 50, 1, 'RM-0000005', 3, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(200, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '12.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(201, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '901.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(202, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '10.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '90.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(203, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '7.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(204, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '9.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '40.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(205, 'RM-A-0000001', 'AC', '10', '0.0000', '1279.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 50, 1, 'RM-A-0000001', 3, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00001', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(206, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '11.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '25.0000', 50, 1, 'AJH001MIUJAN', 3, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(207, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'WBN02', 3, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(208, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '00002', 3, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(209, 'RM-A-0000003', 'AC Moserbaer', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-A-0000003', 3, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(210, 'RM-0000004', 'Test Item QC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000004', 3, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(211, 'RM..0000002', '28MM PILFER CAP RED', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'PCR2', 3, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(212, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-A-0000002', 3, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(213, 'RM-0000011', 'STRAPING ROLLS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000011', 3, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(214, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '0002', 3, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(215, 'RM-0000009', 'Test Item Co Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000009', 3, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(216, 'RM-0000010', 'Test Item Scrap', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000010', 3, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(217, 'RM..0000003', '2LTR NATURAL PLUGS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'NP1', 3, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(218, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-G-0000001', 3, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(219, 'RM-0000003', 'Test Item Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000003', 3, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(220, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '003', 3, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(221, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'PWC01', 3, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(222, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '0004', 3, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(223, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '0007', 3, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(224, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'OPQ21', 3, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(225, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'BPRC03', 3, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(226, 'RM-0000002', 'Test Item Normal', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000002', 3, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(227, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-A-0000004', 3, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(228, 'RM-0000006', 'Test Item BackFlush', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000006', 3, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(229, 'RM-0000007', 'Test Item QC Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000007', 3, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(230, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '0006', 3, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(231, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM..A.0000001', 3, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(232, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, '0003', 3, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(233, 'RM-0000008', 'Test Item By Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM-0000008', 3, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(234, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 'RM..0000008', 3, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', '1', NULL, NULL),
(235, 'RM-A-0000001', 'AC', '10', '0.0000', '9608.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(236, 'RM-A-0000001', 'AC', '10', '0.0000', '9608.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(237, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(238, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(239, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', '0.0000', '310.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'PWC01', 4, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(240, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', '0.0000', '310.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'PWC01', 4, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(241, 'RM-0000005', 'Test Item Manul', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '578.0000', 51, 1, 'RM-0000005', 4, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(242, 'RM-0000005', 'Test Item Manul', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '578.0000', 51, 1, 'RM-0000005', 4, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(243, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '45.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(244, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '45.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(245, 'RM-A-0000001', 'AC', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00006', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(246, 'RM-A-0000001', 'AC', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00006', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(247, 'RM-0000004', 'Test Item QC', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '80.0000', 51, 1, 'RM-0000004', 4, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(248, 'RM-0000004', 'Test Item QC', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '80.0000', 51, 1, 'RM-0000004', 4, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(249, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '999.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00005', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(250, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '999.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00005', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(251, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(252, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(253, 'RM-0000005', 'Test Item Manul', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-0000005', 4, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(254, 'RM-0000005', 'Test Item Manul', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-0000005', 4, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(255, 'RM-0000009', 'Test Item Co Product', '10', '0.0000', '123.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'RM-0000009', 4, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(256, 'RM-0000009', 'Test Item Co Product', '10', '0.0000', '123.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'RM-0000009', 4, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(257, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '11.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(258, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '11.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(259, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '15.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(260, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '15.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(261, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '40.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(262, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '40.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(263, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(264, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(265, 'RM-0000008', 'Test Item By Product', '10', '0.0000', '130.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '80.0000', 51, 1, 'RM-0000008', 4, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(266, 'RM-0000008', 'Test Item By Product', '10', '0.0000', '130.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '80.0000', 51, 1, 'RM-0000008', 4, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(267, 'RM-0000002', 'Test Item Normal', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '103.0000', 51, 1, 'RM-0000002', 4, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(268, 'RM-0000002', 'Test Item Normal', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '103.0000', 51, 1, 'RM-0000002', 4, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(269, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '52.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(270, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '52.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(271, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '1.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(272, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '1.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(273, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(274, 'RM-A-0000001', 'AC', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(275, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '500.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(276, 'RM-0000006', 'Test Item BackFlush', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '500.0000', 51, 1, 'RM-0000006', 4, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(277, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(278, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '12220.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(279, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '23.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(280, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '23.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(281, 'RM-A-0000001', 'AC', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00004', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(282, 'RM-A-0000001', 'AC', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 51, 1, 'RM-A-0000001', 4, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00004', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(283, 'RM-0000010', 'Test Item Scrap', '10', '0.0000', '1234.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'RM-0000010', 4, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(284, 'RM-0000010', 'Test Item Scrap', '10', '0.0000', '1234.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 51, 1, 'RM-0000010', 4, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(285, 'RM-0000007', 'Test Item QC Serial', '10', '0.0000', '130.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 51, 1, 'RM-0000007', 4, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(286, 'RM-0000007', 'Test Item QC Serial', '10', '0.0000', '130.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 51, 1, 'RM-0000007', 4, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(287, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '49801003.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00009', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(288, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '49801003.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00009', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(289, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(290, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(291, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '12.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(292, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '12.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(293, 'RM-0000002', 'Test Item Normal', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 51, 1, 'RM-0000002', 4, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(294, 'RM-0000002', 'Test Item Normal', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 51, 1, 'RM-0000002', 4, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(295, 'RM-0000011', 'STRAPING ROLLS', '10', '0.0000', '406.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 51, 1, 'RM-0000011', 4, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(296, 'RM-0000011', 'STRAPING ROLLS', '10', '0.0000', '406.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 51, 1, 'RM-0000011', 4, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(297, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '846.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(298, 'RM-A-0000003', 'AC Moserbaer', '10', '0.0000', '846.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 51, 1, 'RM-A-0000003', 4, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(299, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '10.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(300, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '10.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(301, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(302, 'RM-0000003', 'Test Item Serial', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 51, 1, 'RM-0000003', 4, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(303, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '15.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(304, 'RM-0000001', 'GORILLA GLASS 5MM', '10', '0.0000', '15.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 51, 1, 'AJH001MIUJAN', 4, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(305, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'WBN02', 4, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(306, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'WBN02', 4, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(307, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '00002', 4, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(308, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '00002', 4, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(309, 'RM..0000002', '28MM PILFER CAP RED', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'PCR2', 4, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(310, 'RM..0000002', '28MM PILFER CAP RED', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'PCR2', 4, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(311, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-A-0000002', 4, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(312, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-A-0000002', 4, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(313, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0002', 4, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(314, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0002', 4, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(315, 'RM..0000003', '2LTR NATURAL PLUGS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'NP1', 4, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(316, 'RM..0000003', '2LTR NATURAL PLUGS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'NP1', 4, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(317, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-G-0000001', 4, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(318, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-G-0000001', 4, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(319, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '003', 4, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(320, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '003', 4, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(321, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0004', 4, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(322, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0004', 4, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(323, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0007', 4, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(324, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0007', 4, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(325, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'OPQ21', 4, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(326, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'OPQ21', 4, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(327, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-G-0000002', 4, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(328, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-G-0000002', 4, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(329, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'BPRC03', 4, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(330, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'BPRC03', 4, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(331, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-A-0000004', 4, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(332, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM-A-0000004', 4, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(333, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0006', 4, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(334, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0006', 4, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(335, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM..A.0000001', 4, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(336, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM..A.0000001', 4, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(337, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0003', 4, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(338, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, '0003', 4, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(339, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM..0000008', 4, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(340, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 'RM..0000008', 4, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', '1', NULL, NULL),
(341, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', '0.0000', '2.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 52, 1, 'BPRC03', 5, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(342, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', '0.0000', '6.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 52, 1, 'WBN02', 5, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(343, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '00002', 5, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(344, 'RM-A-0000003', 'AC Moserbaer', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-A-0000003', 5, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(345, 'RM-0000004', 'Test Item QC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000004', 5, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(346, 'RM..0000002', '28MM PILFER CAP RED', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'PCR2', 5, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(347, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-A-0000002', 5, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(348, 'RM-0000011', 'STRAPING ROLLS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000011', 5, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(349, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '0002', 5, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(350, 'RM-0000009', 'Test Item Co Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000009', 5, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(351, 'RM-0000010', 'Test Item Scrap', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000010', 5, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(352, 'RM..0000003', '2LTR NATURAL PLUGS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'NP1', 5, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(353, 'RM-0000001', 'GORILLA GLASS 5MM', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'AJH001MIUJAN', 5, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(354, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-G-0000001', 5, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(355, 'RM-0000003', 'Test Item Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000003', 5, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(356, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '003', 5, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(357, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'PWC01', 5, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(358, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '0004', 5, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(359, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '0007', 5, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(360, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'OPQ21', 5, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(361, 'RM-0000005', 'Test Item Manul', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000005', 5, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(362, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-G-0000002', 5, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(363, 'RM-0000002', 'Test Item Normal', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000002', 5, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(364, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-A-0000004', 5, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(365, 'RM-0000006', 'Test Item BackFlush', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000006', 5, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(366, 'RM-0000007', 'Test Item QC Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000007', 5, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(367, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '0006', 5, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(368, 'RM-A-0000001', 'AC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-A-0000001', 5, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(369, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM..A.0000001', 5, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(370, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, '0003', 5, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(371, 'RM-0000008', 'Test Item By Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM-0000008', 5, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(372, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 'RM..0000008', 5, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', '1', NULL, NULL),
(373, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', '0.0000', '6.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 53, 1, 'PWC01', 6, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(374, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', '0.0000', '6.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 53, 1, 'PWC01', 6, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(375, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'WBN02', 6, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(376, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '00002', 6, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(377, 'RM-A-0000003', 'AC Moserbaer', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-A-0000003', 6, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(378, 'RM-0000004', 'Test Item QC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000004', 6, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(379, 'RM..0000002', '28MM PILFER CAP RED', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'PCR2', 6, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(380, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-A-0000002', 6, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(381, 'RM-0000011', 'STRAPING ROLLS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000011', 6, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(382, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '0002', 6, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(383, 'RM-0000009', 'Test Item Co Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000009', 6, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(384, 'RM-0000010', 'Test Item Scrap', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000010', 6, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(385, 'RM..0000003', '2LTR NATURAL PLUGS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'NP1', 6, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(386, 'RM-0000001', 'GORILLA GLASS 5MM', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'AJH001MIUJAN', 6, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(387, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-G-0000001', 6, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(388, 'RM-0000003', 'Test Item Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000003', 6, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(389, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '003', 6, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(390, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '0004', 6, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(391, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '0007', 6, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(392, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'OPQ21', 6, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(393, 'RM-0000005', 'Test Item Manul', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000005', 6, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(394, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-G-0000002', 6, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(395, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'BPRC03', 6, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(396, 'RM-0000002', 'Test Item Normal', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000002', 6, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(397, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-A-0000004', 6, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(398, 'RM-0000006', 'Test Item BackFlush', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000006', 6, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(399, 'RM-0000007', 'Test Item QC Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000007', 6, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(400, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '0006', 6, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(401, 'RM-A-0000001', 'AC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-A-0000001', 6, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(402, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM..A.0000001', 6, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(403, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, '0003', 6, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(404, 'RM-0000008', 'Test Item By Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM-0000008', 6, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(405, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 'RM..0000008', 6, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', '1', NULL, NULL),
(406, 'RM..0000002', '28MM PILFER CAP RED', '10', '0.0000', '2.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '300000.0000', 56, 1, 'PCR2', 7, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(407, 'RM..0000002', '28MM PILFER CAP RED', '10', '0.0000', '2.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '287000.0000', 56, 1, 'PCR2', 7, 'PCR2', NULL, 'PCR2', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(408, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', '10', '0.0000', '11.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '287000.0000', 56, 1, 'OPQ21', 7, 'OPQ21', NULL, 'OPQ21', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(409, 'RM..0000003', '2LTR NATURAL PLUGS', '10', '0.0000', '1.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '300000.0000', 56, 1, 'NP1', 7, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(410, 'RM..0000003', '2LTR NATURAL PLUGS', '10', '0.0000', '1.0000', '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, '287000.0000', 56, 1, 'NP1', 7, 'NP1', NULL, 'NP1', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(411, 'RM..0000006', '60ML WHITE BOTTLE NW', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'WBN02', 7, 'WBN02', NULL, 'WBN02', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(412, 'RM-0000014', 'PP-PREMIX OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '00002', 7, '00002', NULL, '00002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(413, 'RM-A-0000003', 'AC Moserbaer', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-A-0000003', 7, 'RM-A-0000003', NULL, 'RM-A-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(414, 'RM-0000004', 'Test Item QC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000004', 7, 'RM-0000004', NULL, 'RM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(415, 'RM-A-0000002', 'AC Bajaj', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-A-0000002', 7, 'RM-A-0000002', NULL, 'RM-A-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(416, 'RM-0000011', 'STRAPING ROLLS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000011', 7, 'RM-0000011', NULL, 'RM-0000011', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(417, 'RM-0000012', 'Raw paint one', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '0002', 7, '0002', NULL, '0002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(418, 'RM-0000009', 'Test Item Co Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000009', 7, 'RM-0000009', NULL, 'RM-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(419, 'RM-0000010', 'Test Item Scrap', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000010', 7, 'RM-0000010', NULL, 'RM-0000010', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(420, 'RM-0000001', 'GORILLA GLASS 5MM', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'AJH001MIUJAN', 7, 'AJH001MIUJAN', NULL, 'AJH001MIUJAN', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(421, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-G-0000001', 7, 'RM-G-0000001', NULL, 'RM-G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(422, 'RM-0000003', 'Test Item Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000003', 7, 'RM-0000003', NULL, 'RM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(423, 'RM-0000013', 'Raw paint two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '003', 7, '003', NULL, '003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(424, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'PWC01', 7, 'PWC01', NULL, 'PWC01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(425, 'RM-0000016', 'PP-GRINDING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '0004', 7, '0004', NULL, '0004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(426, 'RM-0000018', 'PP- WEIGHT -OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '0007', 7, '0007', NULL, '0007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(427, 'RM-0000005', 'Test Item Manul', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000005', 7, 'RM-0000005', NULL, 'RM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(428, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-G-0000002', 7, 'RM-G-0000002', NULL, 'RM-G-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(429, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'BPRC03', 7, 'BPRC03', NULL, 'BPRC03', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(430, 'RM-0000002', 'Test Item Normal', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000002', 7, 'RM-0000002', NULL, 'RM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(431, 'RM-A-0000004', 'AC Starline', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-A-0000004', 7, 'RM-A-0000004', NULL, 'RM-A-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(432, 'RM-0000006', 'Test Item BackFlush', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000006', 7, 'RM-0000006', NULL, 'RM-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(433, 'RM-0000007', 'Test Item QC Serial', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000007', 7, 'RM-0000007', NULL, 'RM-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(434, 'RM-0000017', 'PP-SIEVING OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '0006', 7, '0006', NULL, '0006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(435, 'RM-A-0000001', 'AC', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-A-0000001', 7, 'RM-A-0000001', NULL, 'RM-A-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(436, 'RM..A.0000001', 'AC Moserbaer two', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM..A.0000001', 7, 'RM..A.0000001', NULL, 'RM..A.0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(437, 'RM-0000015', 'PP-EXTRUSION OUTPUT', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, '0003', 7, '0003', NULL, '0003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(438, 'RM-0000008', 'Test Item By Product', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM-0000008', 7, 'RM-0000008', NULL, 'RM-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(439, 'RM..0000008', 'AKS Bottel', '10', NULL, NULL, '1.0000', 'no_image.png', 9, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 'RM..0000008', 7, 'RM..0000008', NULL, 'RM..0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', '1', NULL, NULL),
(440, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, 1, 'ITM-0000005', 3, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', '1', NULL, NULL),
(441, 'ITM-0000004', 'SOKO 10 KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, 1, 'ITM-0000004', 3, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', '1', NULL, NULL),
(442, 'ITM-0000003', 'SOKO 5KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, 1, 'ITM-0000003', 3, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', '1', NULL, NULL),
(443, 'ITM-0000004', 'SOKO 10 KG BALER', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, '85.0000', 59, 1, 'ITM-0000004', 4, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(444, 'ITM-0000004', 'SOKO 10 KG BALER', '10', '0.0000', '200.0000', '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, '85.0000', 59, 1, 'ITM-0000004', 4, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(445, 'ITM-0000003', 'SOKO 5KG BALER', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, '86.0000', 59, 1, 'ITM-0000003', 4, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(446, 'ITM-0000003', 'SOKO 5KG BALER', '10', '0.0000', '120.0000', '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, '86.0000', 59, 1, 'ITM-0000003', 4, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(447, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 1, 'ITM-0000005', 4, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(448, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 1, 'ITM-0000005', 4, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0010010100007', '1', NULL, NULL),
(449, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 1, 'ITM-0000005', 5, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', '1', NULL, NULL);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(450, 'ITM-0000004', 'SOKO 10 KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 1, 'ITM-0000004', 5, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', '1', NULL, NULL),
(451, 'ITM-0000003', 'SOKO 5KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 1, 'ITM-0000003', 5, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', '1', NULL, NULL),
(452, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, 1, 'ITM-0000005', 6, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', '1', NULL, NULL),
(453, 'ITM-0000004', 'SOKO 10 KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, 1, 'ITM-0000004', 6, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', '1', NULL, NULL),
(454, 'ITM-0000003', 'SOKO 5KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, 1, 'ITM-0000003', 6, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', '1', NULL, NULL),
(455, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 64, 1, 'ITM-0000005', 7, 'ITM-0000005', NULL, 'ITM-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', '1', NULL, NULL),
(456, 'ITM-0000004', 'SOKO 10 KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 64, 1, 'ITM-0000004', 7, 'ITM-0000004', NULL, 'ITM-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', '1', NULL, NULL),
(457, 'ITM-0000003', 'SOKO 5KG BALER', '10', NULL, NULL, '1.0000', 'no_image.png', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 64, 1, 'ITM-0000003', 7, 'ITM-0000003', NULL, 'ITM-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', '1', NULL, NULL),
(458, 'TRA-0000001', 'NILAY', '10', NULL, NULL, '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, 1, 'TRA-0000001', 3, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0026010100012', '1', NULL, NULL),
(459, 'TRA-0000001', 'NILAY', '10', '0.0000', '1683.0000', '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 67, 1, 'TRA-0000001', 4, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0026010100012', '1', NULL, NULL),
(460, 'TRA-0000001', 'NILAY', '10', '0.0000', '1683.0000', '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 67, 1, 'TRA-0000001', 4, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0026010100012', '1', NULL, NULL),
(461, 'TRA-0000001', 'NILAY', '10', NULL, NULL, '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 68, 1, 'TRA-0000001', 5, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0026010100012', '1', NULL, NULL),
(462, 'TRA-0000001', 'NILAY', '10', NULL, NULL, '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69, 1, 'TRA-0000001', 6, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0026010100012', '1', NULL, NULL),
(463, 'TRA-0000001', 'NILAY', '10', NULL, NULL, '1.0000', 'no_image.png', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 72, 1, 'TRA-0000001', 7, 'TRA-0000001', NULL, 'TRA-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0026010100012', '1', NULL, NULL),
(464, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '3232.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 74, 1, 'TEST LIFO ITEM', 3, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0007010100004', '1', NULL, NULL),
(465, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '35.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 74, 1, 'TEST LIFO ITEM', 3, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0007010100004', '1', NULL, NULL),
(466, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 74, 1, 'TEST LIFO ITEM', 3, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0007010100004', '1', NULL, NULL),
(467, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(468, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(469, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '867367.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(470, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '867367.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(471, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '12.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT_1', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(472, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '12.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT_1', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(473, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '29.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(474, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '29.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(475, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(476, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(477, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '50.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '118.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(478, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '50.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '118.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(479, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '500.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(480, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '40.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '500.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(481, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '623.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(482, 'CL-0000001', 'TEST LIFO ITEM', '10', '0.0000', '623.0000', '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(483, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(484, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75, 1, 'TEST LIFO ITEM', 4, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0007010100004', '1', NULL, NULL),
(485, 'CL-0000001', 'TEST LIFO ITEM', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 76, 1, 'TEST LIFO ITEM', 5, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0007010100004', '1', NULL, NULL),
(486, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 76, 1, 'TEST LIFO ITEM', 5, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0007010100004', '1', NULL, NULL),
(487, 'CL-0000001', 'TEST LIFO ITEM', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 77, 1, 'TEST LIFO ITEM', 6, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0007010100004', '1', NULL, NULL),
(488, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 77, 1, 'TEST LIFO ITEM', 6, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0007010100004', '1', NULL, NULL),
(489, 'CL-0000001', 'TEST LIFO ITEM', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 80, 1, 'TEST LIFO ITEM', 7, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0007010100004', '1', NULL, NULL),
(490, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', '10', NULL, NULL, '1.0000', 'no_image.png', 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 80, 1, 'TEST LIFO ITEM', 7, 'TEST LIFO ITEM', NULL, 'TEST LIFO ITEM', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0007010100004', '1', NULL, NULL),
(491, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '255.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 82, 1, 'BLK-0000001', 3, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(492, 'BL-0000001', 'Swing machin', '10', '0.0000', '75.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 82, 1, 'BL-0000001', 3, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(493, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '38.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 82, 1, 'ITM-0000002', 3, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(494, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '1200.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 82, 1, 'BLK-0000001', 3, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(495, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2718.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 82, 1, 'ITM-0000002', 3, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(496, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2324.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '17.0000', 82, 1, 'ITM-0000002', 3, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(497, 'ITM-0000001', 'grinder mixer', '10', '0.0000', '386.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 82, 1, 'ITM-0000001', 3, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(498, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'G-0000001', 3, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(499, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'M-0000001', 3, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(500, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'Printer', 3, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(501, 'ITM-0000008', 'Laptop', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'Laptop', 3, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(502, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'CIG..0000001', 3, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(503, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 'Dryer', 3, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', '1', NULL, NULL),
(504, 'BL-0000001', 'Swing machin', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '59.0000', 83, 1, 'BL-0000001', 4, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(505, 'BL-0000001', 'Swing machin', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'BL-0000001', 4, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(506, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1780.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(507, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1780.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(508, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'BLK-0000001', 4, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(509, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'BLK-0000001', 4, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(510, 'ITM-0000008', 'Laptop', '10', '0.0000', '2300.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 83, 1, 'Laptop', 4, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(511, 'ITM-0000008', 'Laptop', '10', '0.0000', '2300.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 83, 1, 'Laptop', 4, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(512, 'ITM-0000001', 'grinder mixer', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 83, 1, 'ITM-0000001', 4, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(513, 'ITM-0000001', 'grinder mixer', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 83, 1, 'ITM-0000001', 4, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(514, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1010.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '7.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(515, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1010.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '7.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(516, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1835.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(517, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1835.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(518, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2280.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2000000.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(519, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2280.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2000000.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(520, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(521, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(522, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', '0.0000', '1069.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 83, 1, 'M-0000001', 4, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(523, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', '0.0000', '1069.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '9.0000', 83, 1, 'M-0000001', 4, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(524, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1882.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '50000.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(525, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1882.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '50000.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(526, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '759.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'BLK-0000001', 4, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(527, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', '0.0000', '759.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'BLK-0000001', 4, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(528, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '4500.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT_1', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(529, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '4500.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT_1', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(530, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1886.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(531, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1886.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 83, 1, 'ITM-0000002', 4, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(532, 'ITM-0000001', 'grinder mixer', '10', '0.0000', '406.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 83, 1, 'ITM-0000001', 4, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(533, 'ITM-0000001', 'grinder mixer', '10', '0.0000', '406.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 83, 1, 'ITM-0000001', 4, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(534, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'G-0000001', 4, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(535, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'G-0000001', 4, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(536, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'Printer', 4, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(537, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'Printer', 4, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(538, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'CIG..0000001', 4, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(539, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'CIG..0000001', 4, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(540, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'Dryer', 4, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(541, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 'Dryer', 4, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', '1', NULL, NULL),
(542, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1012.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 84, 1, 'ITM-0000002', 5, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(543, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'G-0000001', 5, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(544, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'BLK-0000001', 5, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(545, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'M-0000001', 5, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(546, 'BL-0000001', 'Swing machin', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'BL-0000001', 5, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(547, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'Printer', 5, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(548, 'ITM-0000001', 'grinder mixer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'ITM-0000001', 5, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(549, 'ITM-0000008', 'Laptop', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'Laptop', 5, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(550, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'CIG..0000001', 5, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(551, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 'Dryer', 5, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', '1', NULL, NULL),
(552, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1012.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 85, 1, 'ITM-0000002', 6, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(553, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'G-0000001', 6, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(554, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'BLK-0000001', 6, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(555, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'M-0000001', 6, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(556, 'BL-0000001', 'Swing machin', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'BL-0000001', 6, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(557, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'Printer', 6, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(558, 'ITM-0000001', 'grinder mixer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'ITM-0000001', 6, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(559, 'ITM-0000008', 'Laptop', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'Laptop', 6, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(560, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'CIG..0000001', 6, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(561, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 'Dryer', 6, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', '1', NULL, NULL),
(562, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1012.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 88, 1, 'ITM-0000002', 7, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(563, 'ITM-0000002', 'bike tool kit', '10', '0.0000', '1018.0000', '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 88, 1, 'ITM-0000002', 7, 'ITM-0000002', NULL, 'ITM-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(564, 'G-0000001', 'Machine Condenser', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'G-0000001', 7, 'G-0000001', NULL, 'G-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(565, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'BLK-0000001', 7, 'BLK-0000001', NULL, 'BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(566, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'M-0000001', 7, 'M-0000001', NULL, 'M-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(567, 'BL-0000001', 'Swing machin', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'BL-0000001', 7, 'BL-0000001', NULL, 'BL-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(568, 'ITM-0000006', 'Printer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'Printer', 7, 'Printer', NULL, 'Printer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(569, 'ITM-0000001', 'grinder mixer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'ITM-0000001', 7, 'ITM-0000001', NULL, 'ITM-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(570, 'ITM-0000008', 'Laptop', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'Laptop', 7, 'Laptop', NULL, 'Laptop', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(571, 'CIG..0000001', 'TEST ITN', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'CIG..0000001', 7, 'CIG..0000001', NULL, 'CIG..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(572, 'ITM-0000007', 'Dryer', '10', NULL, NULL, '1.0000', 'no_image.png', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 'Dryer', 7, 'Dryer', NULL, 'Dryer', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', '1', NULL, NULL),
(573, 'CV-BLK-0000001', 'Car', '10', '0.0000', '180.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 90, 1, 'CV-BLK-0000001', 3, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(574, 'CV-BLK-0000001', 'Car', '10', '0.0000', '20000.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 90, 1, 'CV-BLK-0000001', 3, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(575, 'FW-0000002', 'Mini Truck', '10', '0.0000', '76042.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '8.0000', 90, 1, 'FW-0000002', 3, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(576, 'FW-0000005', 'MNF Input1', '10', '0.0000', '1221.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 90, 1, 'FW-0000005', 3, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(577, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'CV.FW..0000001', 3, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(578, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'FW-0000003', 3, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(579, 'FW-RD-0000001', 'Hero Honda Splendor', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'Hero Honda Splendor', 3, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(580, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'FW-0000001', 3, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(581, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'FW-0000004', 3, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(582, 'CV.FW..0000002', 'Fortuner', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'CV.FW..0000002', 3, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(583, 'FV-0000001', 'AULTO 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 'FV-0000001', 3, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', '1', NULL, NULL),
(584, 'CV-BLK-0000001', 'Car', '10', '0.0000', '20000.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 91, 1, 'CV-BLK-0000001', 4, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(585, 'CV-BLK-0000001', 'Car', '10', '0.0000', '20000.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 91, 1, 'CV-BLK-0000001', 4, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(586, 'FW-RD-0000001', 'Hero Honda Splendor', '10', '0.0000', '2160.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 91, 1, 'Hero Honda Splendor', 4, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(587, 'FW-RD-0000001', 'Hero Honda Splendor', '10', '0.0000', '2160.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 91, 1, 'Hero Honda Splendor', 4, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(588, 'FW-0000005', 'MNF Input1', '10', '0.0000', '734.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '97.0000', 91, 1, 'FW-0000005', 4, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(589, 'FW-0000005', 'MNF Input1', '10', '0.0000', '734.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '97.0000', 91, 1, 'FW-0000005', 4, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(590, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', '0.0000', '322.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '257005.0000', 91, 1, 'CV.FW..0000001', 4, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(591, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', '0.0000', '322.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '257005.0000', 91, 1, 'CV.FW..0000001', 4, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(592, 'FW-0000002', 'Mini Truck', '10', '0.0000', '215.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '300000.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(593, 'FW-0000002', 'Mini Truck', '10', '0.0000', '215.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '300000.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(594, 'FV-0000001', 'AULTO 800', '10', '0.0000', '25000.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 91, 1, 'FV-0000001', 4, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(595, 'FV-0000001', 'AULTO 800', '10', '0.0000', '25000.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 91, 1, 'FV-0000001', 4, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(596, 'FW-0000002', 'Mini Truck', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(597, 'FW-0000002', 'Mini Truck', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(598, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'CV.FW..0000001', 4, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(599, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'CV.FW..0000001', 4, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(600, 'FW-0000005', 'MNF Input1', '10', '0.0000', '1661.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'FW-0000005', 4, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(601, 'FW-0000005', 'MNF Input1', '10', '0.0000', '1661.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 91, 1, 'FW-0000005', 4, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(602, 'CV.FW..0000002', 'Fortuner', '10', '0.0000', '12053.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 91, 1, 'CV.FW..0000002', 4, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(603, 'CV.FW..0000002', 'Fortuner', '10', '0.0000', '12053.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 91, 1, 'CV.FW..0000002', 4, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(604, 'FW-0000002', 'Mini Truck', '10', '0.0000', '4751.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(605, 'FW-0000002', 'Mini Truck', '10', '0.0000', '4751.0000', '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 91, 1, 'FW-0000002', 4, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(606, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000003', 4, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(607, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000003', 4, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(608, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000001', 4, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(609, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000001', 4, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(610, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000004', 4, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(611, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 'FW-0000004', 4, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', '1', NULL, NULL),
(612, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'CV.FW..0000001', 5, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(613, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FW-0000003', 5, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(614, 'FW-RD-0000001', 'Hero Honda Splendor', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'Hero Honda Splendor', 5, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(615, 'FW-0000002', 'Mini Truck', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FW-0000002', 5, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(616, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FW-0000001', 5, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(617, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FW-0000004', 5, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(618, 'CV.FW..0000002', 'Fortuner', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'CV.FW..0000002', 5, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(619, 'FW-0000005', 'MNF Input1', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FW-0000005', 5, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(620, 'FV-0000001', 'AULTO 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'FV-0000001', 5, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(621, 'CV-BLK-0000001', 'Car', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 'CV-BLK-0000001', 5, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', '1', NULL, NULL),
(622, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'CV.FW..0000001', 6, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(623, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FW-0000003', 6, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(624, 'FW-RD-0000001', 'Hero Honda Splendor', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'Hero Honda Splendor', 6, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(625, 'FW-0000002', 'Mini Truck', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FW-0000002', 6, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(626, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FW-0000001', 6, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(627, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FW-0000004', 6, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(628, 'CV.FW..0000002', 'Fortuner', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'CV.FW..0000002', 6, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(629, 'FW-0000005', 'MNF Input1', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FW-0000005', 6, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(630, 'FV-0000001', 'AULTO 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'FV-0000001', 6, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(631, 'CV-BLK-0000001', 'Car', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 'CV-BLK-0000001', 6, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', '1', NULL, NULL),
(632, 'CV.FW..0000001', 'Ashok Leyland Trucks', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'CV.FW..0000001', 7, 'CV.FW..0000001', NULL, 'CV.FW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(633, 'FW-0000003', 'AULTO LC 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FW-0000003', 7, 'FW-0000003', NULL, 'FW-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(634, 'FW-RD-0000001', 'Hero Honda Splendor', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'Hero Honda Splendor', 7, 'Hero Honda Splendor', NULL, 'Hero Honda Splendor', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(635, 'FW-0000002', 'Mini Truck', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FW-0000002', 7, 'FW-0000002', NULL, 'FW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(636, 'FW-0000001', 'Tata ACE 407', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FW-0000001', 7, 'FW-0000001', NULL, 'FW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(637, 'FW-0000004', 'Marauti Baleno', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FW-0000004', 7, 'FW-0000004', NULL, 'FW-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(638, 'CV.FW..0000002', 'Fortuner', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'CV.FW..0000002', 7, 'CV.FW..0000002', NULL, 'CV.FW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(639, 'FW-0000005', 'MNF Input1', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FW-0000005', 7, 'FW-0000005', NULL, 'FW-0000005', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(640, 'FV-0000001', 'AULTO 800', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'FV-0000001', 7, 'FV-0000001', NULL, 'FV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(641, 'CV-BLK-0000001', 'Car', '10', NULL, NULL, '1.0000', 'no_image.png', 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 'CV-BLK-0000001', 7, 'CV-BLK-0000001', NULL, 'CV-BLK-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', '1', NULL, NULL),
(642, 'CV-0000001', 'Tata Indica', '10', '0.0000', '3255.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 98, 1, 'CV-0000001', 3, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0004010100001', '1', NULL, NULL),
(643, 'CV-0000005', 'Paint', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 1, '0001', 3, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', '1', NULL, NULL),
(644, 'CV-0000002', 'TATA 247', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 1, 'CV-0000002', 3, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', '1', NULL, NULL),
(645, 'CV-0000004', 'INNOVA', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 1, 'CV-0000004', 3, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', '1', NULL, NULL),
(646, 'CV-0000003', 'Marauti Van Cab', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 1, 'CV-0000003', 3, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', '1', NULL, NULL),
(647, 'CV-0000004', 'INNOVA', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(648, 'CV-0000004', 'INNOVA', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(649, 'CV-0000005', 'Paint', '10', '0.0000', '6626.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 99, 1, '0001', 4, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(650, 'CV-0000005', 'Paint', '10', '0.0000', '6626.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 99, 1, '0001', 4, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(651, 'CV-0000003', 'Marauti Van Cab', '10', '0.0000', '1200.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 99, 1, 'CV-0000003', 4, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(652, 'CV-0000003', 'Marauti Van Cab', '10', '0.0000', '1200.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 99, 1, 'CV-0000003', 4, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(653, 'CV-0000002', 'TATA 247', '10', '0.0000', '1140000.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 99, 1, 'CV-0000002', 4, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(654, 'CV-0000002', 'TATA 247', '10', '0.0000', '1140000.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 99, 1, 'CV-0000002', 4, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(655, 'CV-0000004', 'INNOVA', '10', '0.0000', '215.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '250005.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(656, 'CV-0000004', 'INNOVA', '10', '0.0000', '215.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '250005.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(657, 'CV-0000003', 'Marauti Van Cab', '10', '0.0000', '1220.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 99, 1, 'CV-0000003', 4, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(658, 'CV-0000003', 'Marauti Van Cab', '10', '0.0000', '1220.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 99, 1, 'CV-0000003', 4, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(659, 'CV-0000004', 'INNOVA', '10', '0.0000', '531621.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(660, 'CV-0000004', 'INNOVA', '10', '0.0000', '531621.0000', '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 99, 1, 'CV-0000004', 4, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(661, 'CV-0000001', 'Tata Indica', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 1, 'CV-0000001', 4, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(662, 'CV-0000001', 'Tata Indica', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 1, 'CV-0000001', 4, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0004010100001', '1', NULL, NULL),
(663, 'CV-0000005', 'Paint', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, '0001', 5, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', '1', NULL, NULL),
(664, 'CV-0000002', 'TATA 247', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, 'CV-0000002', 5, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', '1', NULL, NULL),
(665, 'CV-0000004', 'INNOVA', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, 'CV-0000004', 5, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', '1', NULL, NULL),
(666, 'CV-0000001', 'Tata Indica', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, 'CV-0000001', 5, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', '1', NULL, NULL),
(667, 'CV-0000003', 'Marauti Van Cab', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, 'CV-0000003', 5, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', '1', NULL, NULL),
(668, 'CV-0000005', 'Paint', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, '0001', 6, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', '1', NULL, NULL),
(669, 'CV-0000002', 'TATA 247', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, 'CV-0000002', 6, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', '1', NULL, NULL),
(670, 'CV-0000004', 'INNOVA', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, 'CV-0000004', 6, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', '1', NULL, NULL),
(671, 'CV-0000001', 'Tata Indica', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, 'CV-0000001', 6, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', '1', NULL, NULL),
(672, 'CV-0000003', 'Marauti Van Cab', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, 'CV-0000003', 6, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', '1', NULL, NULL),
(673, 'CV-0000005', 'Paint', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, '0001', 7, '0001', NULL, '0001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', '1', NULL, NULL),
(674, 'CV-0000002', 'TATA 247', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, 'CV-0000002', 7, 'CV-0000002', NULL, 'CV-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', '1', NULL, NULL),
(675, 'CV-0000004', 'INNOVA', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, 'CV-0000004', 7, 'CV-0000004', NULL, 'CV-0000004', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', '1', NULL, NULL),
(676, 'CV-0000001', 'Tata Indica', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, 'CV-0000001', 7, 'CV-0000001', NULL, 'CV-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', '1', NULL, NULL),
(677, 'CV-0000003', 'Marauti Van Cab', '10', NULL, NULL, '1.0000', 'no_image.png', 11, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, 'CV-0000003', 7, 'CV-0000003', NULL, 'CV-0000003', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', '1', NULL, NULL),
(678, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '397.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '190.0000', 122, 1, 'Super Splendor Front Brake Nut', 3, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(679, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '3595.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'Honda Activa Bend Pipe', 3, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(680, 'CV-RD-0000001', 'Bike', '10', '0.0000', '138.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 122, 1, 'CV-RD-0000001', 3, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(681, 'CV-RD-0000001', 'Bike', '10', '0.0000', '1346.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'CV-RD-0000001', 3, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(682, 'CV-RD-0000001', 'Bike', '10', '0.0000', '2944.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4.0000', 122, 1, 'CV-RD-0000001', 3, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(683, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2020.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '16.0000', 122, 1, 'TW-0000002', 3, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(684, 'TW-0000002', 'Bike accessories', '10', '0.0000', '57.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 122, 1, 'TW-0000002', 3, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(685, 'TW-RD-0000001', 'Bajaj Chetak', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 122, 1, 'TW-RD-0000001', 3, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(686, 'TW-0000002', 'Bike accessories', '10', '0.0000', '3445.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'TW-0000002', 3, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(687, 'CV-RD-0000001', 'Bike', '10', '0.0000', '15000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 122, 1, 'CV-RD-0000001', 3, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(688, 'TW-0000002', 'Bike accessories', '10', '0.0000', '5695.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '97.0000', 122, 1, 'TW-0000002', 3, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(689, 'CV-RD-0000001', 'Bike', '10', '0.0000', '15000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '5.0000', 122, 1, 'CV-RD-0000001', 3, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(690, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '361.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'Super Splendor Front Brake Nut', 3, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(691, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '2272.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '80.0000', 122, 1, 'Honda Activa Bend Pipe', 3, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(692, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '1140.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'Super Splendor Front Brake Nut', 3, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(693, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '24.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 122, 1, 'Honda Activa Bend Pipe', 3, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(694, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-0000008', 3, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(695, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'CV.TW..0000002', 3, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(696, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'CV.TW..0000001', 3, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(697, 'TW-0000009', 'MNF tool', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-0000009', 3, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(698, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'PBW01', 3, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(699, 'TW-0000006', 'MNF Input 2', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-0000006', 3, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(700, 'TW-RD-0000002', 'CBZ', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-RD-0000002', 3, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(701, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-0000007', 3, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(702, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'Rubber Indicator Stays', 3, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(703, 'TW-0000001', 'WIP Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 'TW-0000001', 3, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', '1', NULL, NULL),
(704, 'TW-0000009', 'MNF tool', '10', '0.0000', '78.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 123, 1, 'TW-0000009', 4, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(705, 'TW-0000009', 'MNF tool', '10', '0.0000', '78.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '100.0000', 123, 1, 'TW-0000009', 4, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(706, 'TW-RD-0000002', 'CBZ', '10', '0.0000', '736.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-RD-0000002', 4, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(707, 'TW-RD-0000002', 'CBZ', '10', '0.0000', '736.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-RD-0000002', 4, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(708, 'TW-RD-0000001', 'Bajaj Chetak', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 123, 1, 'TW-RD-0000001', 4, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(709, 'TW-RD-0000001', 'Bajaj Chetak', '10', '0.0000', '1436.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 123, 1, 'TW-RD-0000001', 4, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(710, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '110.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(711, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '110.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(712, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '360.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(713, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '360.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(714, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2065.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(715, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2065.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(716, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(717, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '1288.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(718, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2131.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(719, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2131.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(720, 'TW-0000002', 'Bike accessories', '10', '0.0000', '4872.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00010', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(721, 'TW-0000002', 'Bike accessories', '10', '0.0000', '4872.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00010', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(722, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '600.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(723, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '600.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(724, 'TW-0000002', 'Bike accessories', '10', '0.0000', '5288.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(725, 'TW-0000002', 'Bike accessories', '10', '0.0000', '5288.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(726, 'TW-0000002', 'Bike accessories', '10', '0.0000', '3523.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(727, 'TW-0000002', 'Bike accessories', '10', '0.0000', '3523.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(728, 'CV-RD-0000001', 'Bike', '10', '0.0000', '15000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(729, 'CV-RD-0000001', 'Bike', '10', '0.0000', '15000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(730, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2523.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(731, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2523.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(732, 'TW-0000006', 'MNF Input 2', '10', '0.0000', '364.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '97.0000', 123, 1, 'TW-0000006', 4, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(733, 'TW-0000006', 'MNF Input 2', '10', '0.0000', '364.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '97.0000', 123, 1, 'TW-0000006', 4, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(734, 'TW-0000002', 'Bike accessories', '10', '0.0000', '4500.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(735, 'TW-0000002', 'Bike accessories', '10', '0.0000', '4500.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(736, 'CV-RD-0000001', 'Bike', '10', '0.0000', '2446.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(737, 'CV-RD-0000001', 'Bike', '10', '0.0000', '2446.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(738, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '241.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(739, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '241.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) VALUES
(740, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '202.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(741, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '202.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(742, 'TW-0000009', 'MNF tool', '10', '0.0000', '473.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000009', 4, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(743, 'TW-0000009', 'MNF tool', '10', '0.0000', '473.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000009', 4, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(744, 'CV-RD-0000001', 'Bike', '10', '0.0000', '1999.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(745, 'CV-RD-0000001', 'Bike', '10', '0.0000', '1999.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(746, 'CV-RD-0000001', 'Bike', '10', '0.0000', '3271.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(747, 'CV-RD-0000001', 'Bike', '10', '0.0000', '3271.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(748, 'CV-RD-0000001', 'Bike', '10', '0.0000', '5936.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(749, 'CV-RD-0000001', 'Bike', '10', '0.0000', '5936.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(750, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '133.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(751, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '133.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(752, 'CV-RD-0000001', 'Bike', '10', '0.0000', '700.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(753, 'CV-RD-0000001', 'Bike', '10', '0.0000', '700.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(754, 'TW-0000006', 'MNF Input 2', '10', '0.0000', '2904.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000006', 4, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(755, 'TW-0000006', 'MNF Input 2', '10', '0.0000', '2904.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000006', 4, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(756, 'CV-RD-0000001', 'Bike', '10', '0.0000', '7763.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(757, 'CV-RD-0000001', 'Bike', '10', '0.0000', '7763.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(758, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '160046.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(759, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '160046.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(760, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '180.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(761, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '180.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(762, 'CV-RD-0000001', 'Bike', '10', '0.0000', '99.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(763, 'CV-RD-0000001', 'Bike', '10', '0.0000', '99.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(764, 'TW-0000002', 'Bike accessories', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(765, 'TW-0000002', 'Bike accessories', '10', '0.0000', '332.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(766, 'TW-0000002', 'Bike accessories', '10', '0.0000', '7501.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(767, 'TW-0000002', 'Bike accessories', '10', '0.0000', '7501.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(768, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'CV.TW..0000001', 4, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(769, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2.0000', 123, 1, 'CV.TW..0000001', 4, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(770, 'TW-0000002', 'Bike accessories', '10', '0.0000', '5072.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00009', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(771, 'TW-0000002', 'Bike accessories', '10', '0.0000', '5072.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 123, 1, 'TW-0000002', 4, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00009', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(772, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '30042.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(773, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '30000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '30042.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(774, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '257.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(775, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '257.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(776, 'CV-RD-0000001', 'Bike', '10', '0.0000', '1999.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(777, 'CV-RD-0000001', 'Bike', '10', '0.0000', '1999.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'CV-RD-0000001', 4, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(778, 'TW-0000001', 'WIP Bike', '10', '0.0000', '16000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'TW-0000001', 4, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(779, 'TW-0000001', 'WIP Bike', '10', '0.0000', '16000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1.0000', 123, 1, 'TW-0000001', 4, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(780, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '412.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(781, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '412.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(782, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '642.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(783, 'TW-0000003', 'Honda Activa Bend Pipe', '10', '0.0000', '642.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Honda Activa Bend Pipe', 4, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(784, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '1704.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(785, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', '0.0000', '1704.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'Super Splendor Front Brake Nut', 4, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(786, 'TW-RD-0000001', 'Bajaj Chetak', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-RD-0000001', 4, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(787, 'TW-RD-0000001', 'Bajaj Chetak', '10', '0.0000', '2000.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 123, 1, 'TW-RD-0000001', 4, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(788, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'TW-0000008', 4, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(789, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'TW-0000008', 4, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(790, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'CV.TW..0000002', 4, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(791, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'CV.TW..0000002', 4, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(792, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'PBW01', 4, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(793, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'PBW01', 4, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(794, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'TW-0000007', 4, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(795, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'TW-0000007', 4, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(796, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'Rubber Indicator Stays', 4, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(797, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 'Rubber Indicator Stays', 4, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', '1', NULL, NULL),
(798, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2528.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 124, 1, 'TW-0000002', 5, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(799, 'TW-RD-0000001', 'Bajaj Chetak', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-RD-0000001', 5, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(800, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-0000008', 5, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(801, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'CV.TW..0000002', 5, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(802, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'CV.TW..0000001', 5, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(803, 'TW-0000009', 'MNF tool', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-0000009', 5, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(804, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'Super Splendor Front Brake Nut', 5, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(805, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'PBW01', 5, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(806, 'TW-0000006', 'MNF Input 2', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-0000006', 5, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(807, 'TW-RD-0000002', 'CBZ', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-RD-0000002', 5, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(808, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-0000007', 5, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(809, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'Rubber Indicator Stays', 5, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(810, 'TW-0000001', 'WIP Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'TW-0000001', 5, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(811, 'TW-0000003', 'Honda Activa Bend Pipe', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'Honda Activa Bend Pipe', 5, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(812, 'CV-RD-0000001', 'Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 'CV-RD-0000001', 5, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', '1', NULL, NULL),
(813, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', '0.0000', '28.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 125, 1, 'PBW01', 6, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(814, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', '0.0000', '28.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', 125, 1, 'PBW01', 6, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(815, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2528.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 125, 1, 'TW-0000002', 6, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(816, 'TW-RD-0000001', 'Bajaj Chetak', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-RD-0000001', 6, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(817, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-0000008', 6, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(818, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'CV.TW..0000002', 6, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(819, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'CV.TW..0000001', 6, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(820, 'TW-0000009', 'MNF tool', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-0000009', 6, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(821, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'Super Splendor Front Brake Nut', 6, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(822, 'TW-0000006', 'MNF Input 2', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-0000006', 6, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(823, 'TW-RD-0000002', 'CBZ', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-RD-0000002', 6, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(824, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-0000007', 6, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(825, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'Rubber Indicator Stays', 6, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(826, 'TW-0000001', 'WIP Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'TW-0000001', 6, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(827, 'TW-0000003', 'Honda Activa Bend Pipe', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'Honda Activa Bend Pipe', 6, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(828, 'CV-RD-0000001', 'Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 'CV-RD-0000001', 6, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', '1', NULL, NULL),
(829, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2528.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 128, 1, 'TW-0000002', 7, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00001', NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(830, 'TW-0000002', 'Bike accessories', '10', '0.0000', '2541.0000', '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, '10.0000', 128, 1, 'TW-0000002', 7, 'TW-0000002', NULL, 'TW-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LOT#00002', NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(831, 'TW-RD-0000001', 'Bajaj Chetak', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-RD-0000001', 7, 'TW-RD-0000001', NULL, 'TW-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(832, 'TW-0000008', 'MNF Output', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-0000008', 7, 'TW-0000008', NULL, 'TW-0000008', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(833, 'CV.TW..0000002', 'Bike new', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'CV.TW..0000002', 7, 'CV.TW..0000002', NULL, 'CV.TW..0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(834, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'CV.TW..0000001', 7, 'CV.TW..0000001', NULL, 'CV.TW..0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(835, 'TW-0000009', 'MNF tool', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-0000009', 7, 'TW-0000009', NULL, 'TW-0000009', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(836, 'TW-0000005', 'Super Splendor Front Brake Nut', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'Super Splendor Front Brake Nut', 7, 'Super Splendor Front Brake Nut', NULL, 'Super Splendor Front Brake Nut', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(837, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'PBW01', 7, 'PBW01', NULL, 'PBW01', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(838, 'TW-0000006', 'MNF Input 2', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-0000006', 7, 'TW-0000006', NULL, 'TW-0000006', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(839, 'TW-RD-0000002', 'CBZ', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-RD-0000002', 7, 'TW-RD-0000002', NULL, 'TW-RD-0000002', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(840, 'TW-0000007', 'MNF WIP', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-0000007', 7, 'TW-0000007', NULL, 'TW-0000007', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(841, 'TW-0000004', 'Rubber Indicator Stays', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'Rubber Indicator Stays', 7, 'Rubber Indicator Stays', NULL, 'Rubber Indicator Stays', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(842, 'TW-0000001', 'WIP Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'TW-0000001', 7, 'TW-0000001', NULL, 'TW-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(843, 'TW-0000003', 'Honda Activa Bend Pipe', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'Honda Activa Bend Pipe', 7, 'Honda Activa Bend Pipe', NULL, 'Honda Activa Bend Pipe', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL),
(844, 'CV-RD-0000001', 'Bike', '10', NULL, NULL, '1.0000', 'no_image.png', 13, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 'CV-RD-0000001', 7, 'CV-RD-0000001', NULL, 'CV-RD-0000001', 0, 'standard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `sr_no` varchar(20) DEFAULT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_purchase_items`
--

INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `psale`) VALUES
(1, NULL, NULL, 1, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(2, NULL, NULL, 2, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(3, NULL, NULL, 3, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(4, NULL, NULL, 4, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(5, NULL, NULL, 5, 'NEW..0000003', 'Das Bags', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(6, NULL, NULL, 6, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(7, NULL, NULL, 7, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 3, NULL, 2, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0041010100013', NULL),
(8, NULL, NULL, 8, 'NEW..0000003', 'Das Bags', NULL, '547.2727', '257000.0000', 4, '54.7273', 3, '10.0000', NULL, NULL, NULL, '602.0000', '257000.0000', '0000-00-00', 'received', '602.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0041010100013', NULL),
(9, NULL, NULL, 9, 'NEW..0000003', 'Das Bags', NULL, '547.2727', '257000.0000', 4, '54.7273', 3, '10.0000', NULL, NULL, NULL, '602.0000', '257000.0000', '0000-00-00', 'received', '602.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0041010100013', NULL),
(10, NULL, NULL, 10, 'NEW..0000003', 'Das Bags', NULL, '1170.9091', '0.0000', 4, '117.0909', 3, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0041010100013', NULL),
(11, NULL, NULL, 11, 'NEW..0000003', 'Das Bags', NULL, '1170.9091', '0.0000', 4, '117.0909', 3, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0041010100013', NULL),
(12, NULL, NULL, 12, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(13, NULL, NULL, 13, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(14, NULL, NULL, 14, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(15, NULL, NULL, 15, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(16, NULL, NULL, 16, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(17, NULL, NULL, 17, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(18, NULL, NULL, 18, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(19, NULL, NULL, 19, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(20, NULL, NULL, 20, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(21, NULL, NULL, 21, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(22, NULL, NULL, 22, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(23, NULL, NULL, 23, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 4, NULL, 3, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0041010100013', NULL),
(24, NULL, NULL, 24, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(25, NULL, NULL, 25, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(26, NULL, NULL, 26, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(27, NULL, NULL, 27, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(28, NULL, NULL, 28, 'NEW..0000003', 'Das Bags', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(29, NULL, NULL, 29, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(30, NULL, NULL, 30, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 5, NULL, 4, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0041010100013', NULL),
(31, NULL, NULL, 31, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(32, NULL, NULL, 32, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(33, NULL, NULL, 33, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(34, NULL, NULL, 34, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(35, NULL, NULL, 35, 'NEW..0000003', 'Das Bags', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(36, NULL, NULL, 36, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(37, NULL, NULL, 37, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 6, NULL, 5, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0041010100013', NULL),
(38, NULL, NULL, 38, 'NEW..0000004', 'COOKER', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(39, NULL, NULL, 39, 'NEW..0000005', 'GK BOOKS', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(40, NULL, NULL, 40, 'NEW..0000007', 'Das Bags Green', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(41, NULL, NULL, 41, 'NEW..0000001', 'ITEM 5', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(42, NULL, NULL, 42, 'NEW..0000003', 'Das Bags', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(43, NULL, NULL, 43, 'NEW..0000002', 'TEST ITEM1', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(44, NULL, NULL, 44, 'NEW..0000006', 'SCOOTER', NULL, '0.0000', '0.0000', 7, NULL, 8, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0041010100013', NULL),
(45, NULL, NULL, 45, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '23.6364', '4.0000', 3, '2.3636', 10, '10.0000', NULL, NULL, NULL, '26.0000', '2.0000', '0000-00-00', 'received', '26.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0008010100005', NULL),
(46, NULL, NULL, 46, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '456.3636', '0.0000', 4, '45.6364', 11, '10.0000', NULL, NULL, NULL, '502.0000', '0.0000', '0000-00-00', 'received', '502.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0008010100005', NULL),
(47, NULL, NULL, 47, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '456.3636', '0.0000', 4, '45.6364', 11, '10.0000', NULL, NULL, NULL, '502.0000', '0.0000', '0000-00-00', 'received', '502.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0008010100005', NULL),
(48, NULL, NULL, 48, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '1031.8182', '2.0000', 4, '103.1818', 11, '10.0000', NULL, NULL, NULL, '1135.0000', '2.0000', '0000-00-00', 'received', '1135.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0008010100005', NULL),
(49, NULL, NULL, 49, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '1031.8182', '2.0000', 4, '103.1818', 11, '10.0000', NULL, NULL, NULL, '1135.0000', '2.0000', '0000-00-00', 'received', '1135.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0008010100005', NULL),
(50, NULL, NULL, 50, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '776.3636', '10.0000', 4, '77.6364', 11, '10.0000', NULL, NULL, NULL, '854.0000', '10.0000', '0000-00-00', 'received', '854.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0008010100005', NULL),
(51, NULL, NULL, 51, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '776.3636', '10.0000', 4, '77.6364', 11, '10.0000', NULL, NULL, NULL, '854.0000', '10.0000', '0000-00-00', 'received', '854.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0008010100005', NULL),
(52, NULL, NULL, 52, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '301.8182', '6.0000', 4, '30.1818', 11, '10.0000', NULL, NULL, NULL, '332.0000', '6.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0008010100005', NULL),
(53, NULL, NULL, 53, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '301.8182', '6.0000', 4, '30.1818', 11, '10.0000', NULL, NULL, NULL, '332.0000', '6.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0008010100005', NULL),
(54, NULL, NULL, 54, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '4.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '4.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0008010100005', NULL),
(55, NULL, NULL, 55, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '4.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '4.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0008010100005', NULL),
(56, NULL, NULL, 56, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '0.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '0.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0008010100005', NULL),
(57, NULL, NULL, 57, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '0.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '0.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0008010100005', NULL),
(58, NULL, NULL, 58, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '4.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '4.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0008010100005', NULL),
(59, NULL, NULL, 59, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '909.0909', '4.0000', 4, '90.9091', 11, '10.0000', NULL, NULL, NULL, '1000.0000', '4.0000', '0000-00-00', 'received', '1000.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0008010100005', NULL),
(60, NULL, NULL, 60, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '0.0000', '0.0000', 5, NULL, 12, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0008010100005', NULL),
(61, NULL, NULL, 61, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '0.0000', '0.0000', 6, NULL, 13, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0008010100005', NULL),
(62, NULL, NULL, 62, 'CC-RD-0000001', 'Cotton T- Shirt', NULL, '0.0000', '0.0000', 7, NULL, 16, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0008010100005', NULL),
(63, NULL, NULL, 63, 'SC-M-0000001', 'Silk Suit', NULL, '43.6364', '5.0000', 3, '4.3636', 18, '10.0000', NULL, NULL, NULL, '48.0000', '5.0000', '0000-00-00', 'received', '48.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0009010100006', NULL),
(64, NULL, NULL, 64, 'SC-M-0000001', 'Silk Suit', NULL, '1731.8182', '10.0000', 3, '173.1818', 18, '10.0000', NULL, NULL, NULL, '1905.0000', '10.0000', '0000-00-00', 'received', '1905.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0009010100006', NULL),
(65, NULL, NULL, 65, 'SC-M-0000001', 'Silk Suit', NULL, '1305.4545', '6.0000', 4, '130.5455', 19, '10.0000', NULL, NULL, NULL, '1436.0000', '6.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0009010100006', NULL),
(66, NULL, NULL, 66, 'SC-M-0000001', 'Silk Suit', NULL, '1305.4545', '6.0000', 4, '130.5455', 19, '10.0000', NULL, NULL, NULL, '1436.0000', '6.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0009010100006', NULL),
(67, NULL, NULL, 67, 'SC-M-0000001', 'Silk Suit', NULL, '0.0000', '0.0000', 5, NULL, 20, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0009010100006', NULL),
(68, NULL, NULL, 68, 'SC-M-0000001', 'Silk Suit', NULL, '0.0000', '0.0000', 6, NULL, 21, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0009010100006', NULL),
(69, NULL, NULL, 69, 'SC-M-0000001', 'Silk Suit', NULL, '0.0000', '0.0000', 7, NULL, 24, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0009010100006', NULL),
(70, NULL, NULL, 70, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 3, NULL, 26, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0102010100016', NULL),
(71, NULL, NULL, 71, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 4, NULL, 27, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0102010100016', NULL),
(72, NULL, NULL, 72, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 4, NULL, 27, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0102010100016', NULL),
(73, NULL, NULL, 73, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 5, NULL, 28, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0102010100016', NULL),
(74, NULL, NULL, 74, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 6, NULL, 29, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0102010100016', NULL),
(75, NULL, NULL, 75, 'ITM..GRN.0000001', 'TEST2', NULL, '0.0000', '0.0000', 7, NULL, 32, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0102010100016', NULL),
(76, NULL, NULL, 76, 'COMP..0000003', 'MOULD', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(77, NULL, NULL, 77, 'COMP-0000008', 'charger', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(78, NULL, NULL, 78, 'COMP..0000001', 'HP ProBook', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(79, NULL, NULL, 79, 'COMP-0000003', 'KEYBOARD', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(80, NULL, NULL, 80, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(81, NULL, NULL, 81, 'COMP-0000002', 'CD/ROM', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(82, NULL, NULL, 82, 'COMP-0000001', 'RAM 2GB', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(83, NULL, NULL, 83, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(84, NULL, NULL, 84, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(85, NULL, NULL, 85, 'COMP..0000002', 'Lenevo 201', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(86, NULL, NULL, 86, 'COMP-0000004', 'MOUSE', NULL, '0.0000', '0.0000', 3, NULL, 34, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0024010100011', NULL),
(87, NULL, NULL, 87, 'COMP-0000002', 'CD/ROM', NULL, '23.6364', '10.0000', 4, '2.3636', 35, '10.0000', NULL, NULL, NULL, '26.0000', '10.0000', '0000-00-00', 'received', '26.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(88, NULL, NULL, 88, 'COMP-0000002', 'CD/ROM', NULL, '23.6364', '10.0000', 4, '2.3636', 35, '10.0000', NULL, NULL, NULL, '26.0000', '10.0000', '0000-00-00', 'received', '26.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(89, NULL, NULL, 89, 'COMP..0000001', 'HP ProBook', NULL, '1170.9091', '0.0000', 4, '117.0909', 35, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(90, NULL, NULL, 90, 'COMP..0000001', 'HP ProBook', NULL, '1170.9091', '0.0000', 4, '117.0909', 35, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(91, NULL, NULL, 91, 'COMP-0000001', 'RAM 2GB', NULL, '181.8182', '10.0000', 4, '18.1818', 35, '10.0000', NULL, NULL, NULL, '200.0000', '10.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(92, NULL, NULL, 92, 'COMP-0000001', 'RAM 2GB', NULL, '181.8182', '10.0000', 4, '18.1818', 35, '10.0000', NULL, NULL, NULL, '200.0000', '10.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(93, NULL, NULL, 93, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '10.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '10.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(94, NULL, NULL, 94, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '10.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '10.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(95, NULL, NULL, 95, 'COMP..0000001', 'HP ProBook', NULL, '1143.6364', '3.0000', 4, '114.3636', 35, '10.0000', NULL, NULL, NULL, '1258.0000', '3.0000', '0000-00-00', 'received', '1258.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(96, NULL, NULL, 96, 'COMP..0000001', 'HP ProBook', NULL, '1143.6364', '3.0000', 4, '114.3636', 35, '10.0000', NULL, NULL, NULL, '1258.0000', '3.0000', '0000-00-00', 'received', '1258.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(97, NULL, NULL, 97, 'COMP..0000003', 'MOULD', NULL, '24401.8182', '4.0000', 4, '2440.1818', 35, '10.0000', NULL, NULL, NULL, '26842.0000', '4.0000', '0000-00-00', 'received', '26842.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(98, NULL, NULL, 98, 'COMP..0000003', 'MOULD', NULL, '24401.8182', '4.0000', 4, '2440.1818', 35, '10.0000', NULL, NULL, NULL, '26842.0000', '4.0000', '0000-00-00', 'received', '26842.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(99, NULL, NULL, 99, 'COMP-0000003', 'KEYBOARD', NULL, '227.2727', '10.0000', 4, '22.7273', 35, '10.0000', NULL, NULL, NULL, '250.0000', '10.0000', '0000-00-00', 'received', '250.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(100, NULL, NULL, 100, 'COMP-0000003', 'KEYBOARD', NULL, '227.2727', '10.0000', 4, '22.7273', 35, '10.0000', NULL, NULL, NULL, '250.0000', '10.0000', '0000-00-00', 'received', '250.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(101, NULL, NULL, 101, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '2.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '2.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(102, NULL, NULL, 102, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '2.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '2.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(103, NULL, NULL, 103, 'COMP-0000002', 'CD/ROM', NULL, '23.6364', '10.0000', 4, '2.3636', 35, '10.0000', NULL, NULL, NULL, '26.0000', '10.0000', '0000-00-00', 'received', '26.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(104, NULL, NULL, 104, 'COMP-0000002', 'CD/ROM', NULL, '23.6364', '10.0000', 4, '2.3636', 35, '10.0000', NULL, NULL, NULL, '26.0000', '10.0000', '0000-00-00', 'received', '26.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(105, NULL, NULL, 105, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '10.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '10.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(106, NULL, NULL, 106, 'COMP-0000004', 'MOUSE', NULL, '36.3636', '10.0000', 4, '3.6364', 35, '10.0000', NULL, NULL, NULL, '40.0000', '10.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(107, NULL, NULL, 107, 'COMP..0000002', 'Lenevo 201', NULL, '1143.6364', '3.0000', 4, '114.3636', 35, '10.0000', NULL, NULL, NULL, '1258.0000', '3.0000', '0000-00-00', 'received', '1258.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(108, NULL, NULL, 108, 'COMP..0000002', 'Lenevo 201', NULL, '1143.6364', '3.0000', 4, '114.3636', 35, '10.0000', NULL, NULL, NULL, '1258.0000', '3.0000', '0000-00-00', 'received', '1258.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(109, NULL, NULL, 109, 'COMP-0000001', 'RAM 2GB', NULL, '155.4545', '10.0000', 4, '15.5455', 35, '10.0000', NULL, NULL, NULL, '171.0000', '10.0000', '0000-00-00', 'received', '171.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(110, NULL, NULL, 110, 'COMP-0000001', 'RAM 2GB', NULL, '155.4545', '10.0000', 4, '15.5455', 35, '10.0000', NULL, NULL, NULL, '171.0000', '10.0000', '0000-00-00', 'received', '171.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0024010100011', NULL),
(111, NULL, NULL, 111, 'COMP-0000001', 'RAM 2GB', NULL, '181.8182', '8.0000', 4, '18.1818', 35, '10.0000', NULL, NULL, NULL, '200.0000', '8.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(112, NULL, NULL, 112, 'COMP-0000001', 'RAM 2GB', NULL, '181.8182', '8.0000', 4, '18.1818', 35, '10.0000', NULL, NULL, NULL, '200.0000', '8.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(113, NULL, NULL, 113, 'COMP-0000001', 'RAM 2GB', NULL, '95539.0909', '3.0000', 4, '9553.9091', 35, '10.0000', NULL, NULL, NULL, '105093.0000', '3.0000', '0000-00-00', 'received', '105093.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0024010100011', NULL),
(114, NULL, NULL, 114, 'COMP-0000001', 'RAM 2GB', NULL, '95539.0909', '3.0000', 4, '9553.9091', 35, '10.0000', NULL, NULL, NULL, '105093.0000', '3.0000', '0000-00-00', 'received', '105093.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0024010100011', NULL),
(115, NULL, NULL, 115, 'COMP-0000003', 'KEYBOARD', NULL, '228.1818', '10.0000', 4, '22.8182', 35, '10.0000', NULL, NULL, NULL, '251.0000', '10.0000', '0000-00-00', 'received', '251.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(116, NULL, NULL, 116, 'COMP-0000003', 'KEYBOARD', NULL, '228.1818', '10.0000', 4, '22.8182', 35, '10.0000', NULL, NULL, NULL, '251.0000', '10.0000', '0000-00-00', 'received', '251.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0024010100011', NULL),
(117, NULL, NULL, 117, 'COMP-0000008', 'charger', NULL, '2738.1818', '0.0000', 4, '273.8182', 35, '10.0000', NULL, NULL, NULL, '3012.0000', '0.0000', '0000-00-00', 'received', '3012.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(118, NULL, NULL, 118, 'COMP-0000008', 'charger', NULL, '2738.1818', '0.0000', 4, '273.8182', 35, '10.0000', NULL, NULL, NULL, '3012.0000', '0.0000', '0000-00-00', 'received', '3012.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(119, NULL, NULL, 119, 'COMP-0000003', 'KEYBOARD', NULL, '227.2727', '2.0000', 4, '22.7273', 35, '10.0000', NULL, NULL, NULL, '250.0000', '2.0000', '0000-00-00', 'received', '250.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(120, NULL, NULL, 120, 'COMP-0000003', 'KEYBOARD', NULL, '227.2727', '2.0000', 4, '22.7273', 35, '10.0000', NULL, NULL, NULL, '250.0000', '2.0000', '0000-00-00', 'received', '250.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(121, NULL, NULL, 121, 'COMP-0000004', 'MOUSE', NULL, '41.8182', '11.0000', 4, '4.1818', 35, '10.0000', NULL, NULL, NULL, '46.0000', '11.0000', '0000-00-00', 'received', '46.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(122, NULL, NULL, 122, 'COMP-0000004', 'MOUSE', NULL, '41.8182', '11.0000', 4, '4.1818', 35, '10.0000', NULL, NULL, NULL, '46.0000', '11.0000', '0000-00-00', 'received', '46.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(123, NULL, NULL, 123, 'COMP-0000001', 'RAM 2GB', NULL, '137.2727', '10.0000', 4, '13.7273', 35, '10.0000', NULL, NULL, NULL, '151.0000', '10.0000', '0000-00-00', 'received', '151.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(124, NULL, NULL, 124, 'COMP-0000001', 'RAM 2GB', NULL, '137.2727', '10.0000', 4, '13.7273', 35, '10.0000', NULL, NULL, NULL, '151.0000', '10.0000', '0000-00-00', 'received', '151.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(125, NULL, NULL, 125, 'COMP-0000003', 'KEYBOARD', NULL, '258.1818', '10.0000', 4, '25.8182', 35, '10.0000', NULL, NULL, NULL, '284.0000', '10.0000', '0000-00-00', 'received', '284.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(126, NULL, NULL, 126, 'COMP-0000003', 'KEYBOARD', NULL, '258.1818', '10.0000', 4, '25.8182', 35, '10.0000', NULL, NULL, NULL, '284.0000', '10.0000', '0000-00-00', 'received', '284.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(127, NULL, NULL, 127, 'COMP-0000002', 'CD/ROM', NULL, '27.2727', '10.0000', 4, '2.7273', 35, '10.0000', NULL, NULL, NULL, '30.0000', '10.0000', '0000-00-00', 'received', '30.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(128, NULL, NULL, 128, 'COMP-0000002', 'CD/ROM', NULL, '27.2727', '10.0000', 4, '2.7273', 35, '10.0000', NULL, NULL, NULL, '30.0000', '10.0000', '0000-00-00', 'received', '30.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0024010100011', NULL),
(129, NULL, NULL, 129, 'COMP-0000002', 'CD/ROM', NULL, '9100.0000', '3.0000', 4, '910.0000', 35, '10.0000', NULL, NULL, NULL, '10010.0000', '3.0000', '0000-00-00', 'received', '10010.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(130, NULL, NULL, 130, 'COMP-0000002', 'CD/ROM', NULL, '9100.0000', '3.0000', 4, '910.0000', 35, '10.0000', NULL, NULL, NULL, '10010.0000', '3.0000', '0000-00-00', 'received', '10010.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0024010100011', NULL),
(131, NULL, NULL, 131, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(132, NULL, NULL, 132, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(133, NULL, NULL, 133, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(134, NULL, NULL, 134, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(135, NULL, NULL, 135, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(136, NULL, NULL, 136, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 4, NULL, 35, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0024010100011', NULL),
(137, NULL, NULL, 137, 'COMP..0000003', 'MOULD', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(138, NULL, NULL, 138, 'COMP-0000008', 'charger', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(139, NULL, NULL, 139, 'COMP..0000001', 'HP ProBook', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(140, NULL, NULL, 140, 'COMP-0000003', 'KEYBOARD', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(141, NULL, NULL, 141, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(142, NULL, NULL, 142, 'COMP-0000002', 'CD/ROM', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(143, NULL, NULL, 143, 'COMP-0000001', 'RAM 2GB', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(144, NULL, NULL, 144, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(145, NULL, NULL, 145, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(146, NULL, NULL, 146, 'COMP..0000002', 'Lenevo 201', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(147, NULL, NULL, 147, 'COMP-0000004', 'MOUSE', NULL, '0.0000', '0.0000', 5, NULL, 36, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0024010100011', NULL),
(148, NULL, NULL, 148, 'COMP..0000003', 'MOULD', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(149, NULL, NULL, 149, 'COMP-0000008', 'charger', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(150, NULL, NULL, 150, 'COMP..0000001', 'HP ProBook', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(151, NULL, NULL, 151, 'COMP-0000003', 'KEYBOARD', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(152, NULL, NULL, 152, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(153, NULL, NULL, 153, 'COMP-0000002', 'CD/ROM', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(154, NULL, NULL, 154, 'COMP-0000001', 'RAM 2GB', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(155, NULL, NULL, 155, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(156, NULL, NULL, 156, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(157, NULL, NULL, 157, 'COMP..0000002', 'Lenevo 201', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(158, NULL, NULL, 158, 'COMP-0000004', 'MOUSE', NULL, '0.0000', '0.0000', 6, NULL, 37, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0024010100011', NULL),
(159, NULL, NULL, 159, 'COMP..0000003', 'MOULD', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(160, NULL, NULL, 160, 'COMP-0000008', 'charger', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(161, NULL, NULL, 161, 'COMP..0000001', 'HP ProBook', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(162, NULL, NULL, 162, 'COMP-0000003', 'KEYBOARD', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(163, NULL, NULL, 163, 'COMP-0000006', 'HP computer', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(164, NULL, NULL, 164, 'COMP-0000002', 'CD/ROM', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(165, NULL, NULL, 165, 'COMP-0000001', 'RAM 2GB', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(166, NULL, NULL, 166, 'COMP-0000007', 'wip computer', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(167, NULL, NULL, 167, 'COMP-0000005', 'Ram', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(168, NULL, NULL, 168, 'COMP..0000002', 'Lenevo 201', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(169, NULL, NULL, 169, 'COMP-0000004', 'MOUSE', NULL, '0.0000', '0.0000', 7, NULL, 40, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0024010100011', NULL),
(170, NULL, NULL, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '22.7273', '100.0000', 3, '2.2727', 42, '10.0000', NULL, NULL, NULL, '25.0000', '98.0000', '0000-00-00', 'received', '25.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0005010100002', NULL),
(171, NULL, NULL, 171, 'CIG-BLK-0000001', 'Computer', NULL, '0.0000', '0.0000', 3, NULL, 42, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', NULL),
(172, NULL, NULL, 172, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 3, NULL, 42, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', NULL),
(173, NULL, NULL, 173, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '0.0000', '0.0000', 3, NULL, 42, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0005010100002', NULL),
(174, NULL, NULL, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '301.8182', '10.0000', 4, '30.1818', 43, '10.0000', NULL, NULL, NULL, '332.0000', '-3.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(175, NULL, NULL, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '301.8182', '10.0000', 4, '30.1818', 43, '10.0000', NULL, NULL, NULL, '332.0000', '4.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(176, NULL, NULL, 176, 'CIG-BLK-0000001', 'Computer', NULL, '1818.1818', '10.0000', 4, '181.8182', 43, '10.0000', NULL, NULL, NULL, '2000.0000', '10.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(177, NULL, NULL, 177, 'CIG-BLK-0000001', 'Computer', NULL, '1818.1818', '10.0000', 4, '181.8182', 43, '10.0000', NULL, NULL, NULL, '2000.0000', '5.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(178, NULL, NULL, 178, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '12885.4545', '4.0000', 4, '1288.5455', 43, '10.0000', NULL, NULL, NULL, '14174.0000', '4.0000', '0000-00-00', 'received', '14174.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(179, NULL, NULL, 179, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '12885.4545', '4.0000', 4, '1288.5455', 43, '10.0000', NULL, NULL, NULL, '14174.0000', '4.0000', '0000-00-00', 'received', '14174.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0005010100002', NULL),
(180, NULL, NULL, 180, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '12011.8182', '3.0000', 4, '1201.1818', 43, '10.0000', NULL, NULL, NULL, '13213.0000', '3.0000', '0000-00-00', 'received', '13213.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', NULL),
(181, NULL, NULL, 181, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '12011.8182', '3.0000', 4, '1201.1818', 43, '10.0000', NULL, NULL, NULL, '13213.0000', '3.0000', '0000-00-00', 'received', '13213.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', NULL),
(182, NULL, NULL, 182, 'CIG-BLK-0000001', 'Computer', NULL, '1305.4545', '10.0000', 4, '130.5455', 43, '10.0000', NULL, NULL, NULL, '1436.0000', '10.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', NULL),
(183, NULL, NULL, 183, 'CIG-BLK-0000001', 'Computer', NULL, '1305.4545', '10.0000', 4, '130.5455', 43, '10.0000', NULL, NULL, NULL, '1436.0000', '10.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0005010100002', NULL),
(184, NULL, NULL, 184, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 4, NULL, 43, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0005010100002', NULL),
(185, NULL, NULL, 185, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 4, NULL, 43, '10.0000', NULL, NULL, NULL, '0.0000', '-1.0000', '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0005010100002', NULL),
(186, NULL, NULL, 186, 'CIG-BLK-0000001', 'Computer', NULL, '0.0000', '0.0000', 5, NULL, 44, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', NULL),
(187, NULL, NULL, 187, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 5, NULL, 44, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', NULL),
(188, NULL, NULL, 188, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '0.0000', '0.0000', 5, NULL, 44, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', NULL),
(189, NULL, NULL, 189, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '0.0000', '0.0000', 5, NULL, 44, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0005010100002', NULL),
(190, NULL, NULL, 190, 'CIG-BLK-0000001', 'Computer', NULL, '0.0000', '0.0000', 6, NULL, 45, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', NULL),
(191, NULL, NULL, 191, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 6, NULL, 45, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', NULL),
(192, NULL, NULL, 192, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '0.0000', '0.0000', 6, NULL, 45, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', NULL),
(193, NULL, NULL, 193, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '0.0000', '0.0000', 6, NULL, 45, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0005010100002', NULL),
(194, NULL, NULL, 194, 'CIG-BLK-0000001', 'Computer', NULL, '0.0000', '0.0000', 7, NULL, 48, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', NULL),
(195, NULL, NULL, 195, 'CIG-BLK-0000002', 'Motorcycle', NULL, '0.0000', '0.0000', 7, NULL, 48, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', NULL),
(196, NULL, NULL, 196, 'CIG-0000001', 'WALL PLATE 1100X3000MM', NULL, '0.0000', '0.0000', 7, NULL, 48, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', NULL),
(197, NULL, NULL, 197, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', NULL, '0.0000', '0.0000', 7, NULL, 48, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0005010100002', NULL),
(198, NULL, NULL, 198, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '682.7273', '40.0000', 3, '68.2727', 50, '10.0000', NULL, NULL, NULL, '751.0000', '40.0000', '0000-00-00', 'received', '751.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', NULL),
(199, NULL, NULL, 199, 'RM-0000005', 'Test Item Manul', NULL, '2120.9091', '10.0000', 3, '212.0909', 50, '10.0000', NULL, NULL, NULL, '2333.0000', '10.0000', '0000-00-00', 'received', '2333.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', NULL),
(200, NULL, NULL, 200, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.9091', '1.0000', 3, '1.0909', 50, '10.0000', NULL, NULL, NULL, '12.0000', '1.0000', '0000-00-00', 'received', '12.0000', NULL, 'LOT#00006', NULL, 'WH0000201', '0011010100008', NULL),
(201, NULL, NULL, 201, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '819.0909', '10.0000', 3, '81.9091', 50, '10.0000', NULL, NULL, NULL, '901.0000', '10.0000', '0000-00-00', 'received', '901.0000', NULL, 'LOT#00005', NULL, 'WH0000201', '0011010100008', NULL),
(202, NULL, NULL, 202, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '9.0909', '90.0000', 3, '0.9091', 50, '10.0000', NULL, NULL, NULL, '10.0000', '90.0000', '0000-00-00', 'received', '10.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0011010100008', NULL),
(203, NULL, NULL, 203, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '6.3636', '10.0000', 3, '0.6364', 50, '10.0000', NULL, NULL, NULL, '7.0000', '10.0000', '0000-00-00', 'received', '7.0000', NULL, 'LOT#00004', NULL, 'WH0000201', '0011010100008', NULL),
(204, NULL, NULL, 204, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '8.1818', '40.0000', 3, '0.8182', 50, '10.0000', NULL, NULL, NULL, '9.0000', '40.0000', '0000-00-00', 'received', '9.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0011010100008', NULL),
(205, NULL, NULL, 205, 'RM-A-0000001', 'AC', NULL, '1162.7273', '10.0000', 3, '116.2727', 50, '10.0000', NULL, NULL, NULL, '1279.0000', '10.0000', '0000-00-00', 'received', '1279.0000', NULL, 'AC#00001', NULL, 'WH0000201', '0011010100008', NULL),
(206, NULL, NULL, 206, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.0000', '25.0000', 3, '1.0000', 50, '10.0000', NULL, NULL, NULL, '11.0000', '25.0000', '0000-00-00', 'received', '11.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0011010100008', NULL),
(207, NULL, NULL, 207, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(208, NULL, NULL, 208, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(209, NULL, NULL, 209, 'RM-A-0000003', 'AC Moserbaer', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(210, NULL, NULL, 210, 'RM-0000004', 'Test Item QC', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(211, NULL, NULL, 211, 'RM..0000002', '28MM PILFER CAP RED', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(212, NULL, NULL, 212, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `psale`) VALUES
(213, NULL, NULL, 213, 'RM-0000011', 'STRAPING ROLLS', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(214, NULL, NULL, 214, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(215, NULL, NULL, 215, 'RM-0000009', 'Test Item Co Product', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(216, NULL, NULL, 216, 'RM-0000010', 'Test Item Scrap', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(217, NULL, NULL, 217, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(218, NULL, NULL, 218, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(219, NULL, NULL, 219, 'RM-0000003', 'Test Item Serial', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(220, NULL, NULL, 220, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(221, NULL, NULL, 221, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(222, NULL, NULL, 222, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(223, NULL, NULL, 223, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(224, NULL, NULL, 224, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(225, NULL, NULL, 225, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(226, NULL, NULL, 226, 'RM-0000002', 'Test Item Normal', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(227, NULL, NULL, 227, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(228, NULL, NULL, 228, 'RM-0000006', 'Test Item BackFlush', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(229, NULL, NULL, 229, 'RM-0000007', 'Test Item QC Serial', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(230, NULL, NULL, 230, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(231, NULL, NULL, 231, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(232, NULL, NULL, 232, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(233, NULL, NULL, 233, 'RM-0000008', 'Test Item By Product', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(234, NULL, NULL, 234, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 3, NULL, 50, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0011010100008', NULL),
(235, NULL, NULL, 235, 'RM-A-0000001', 'AC', NULL, '8734.5455', '1.0000', 4, '873.4545', 51, '10.0000', NULL, NULL, NULL, '9608.0000', '1.0000', '0000-00-00', 'received', '9608.0000', NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', NULL),
(236, NULL, NULL, 236, 'RM-A-0000001', 'AC', NULL, '8734.5455', '1.0000', 4, '873.4545', 51, '10.0000', NULL, NULL, NULL, '9608.0000', '1.0000', '0000-00-00', 'received', '9608.0000', NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', NULL),
(237, NULL, NULL, 237, 'RM-A-0000003', 'AC Moserbaer', NULL, '11109.0909', '3.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '3.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', NULL),
(238, NULL, NULL, 238, 'RM-A-0000003', 'AC Moserbaer', NULL, '11109.0909', '3.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '3.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00002', NULL, 'WH0000101', '0011010100008', NULL),
(239, NULL, NULL, 239, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '281.8182', '0.0000', 4, '28.1818', 51, '10.0000', NULL, NULL, NULL, '310.0000', '0.0000', '0000-00-00', 'received', '310.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(240, NULL, NULL, 240, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '281.8182', '0.0000', 4, '28.1818', 51, '10.0000', NULL, NULL, NULL, '310.0000', '0.0000', '0000-00-00', 'received', '310.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(241, NULL, NULL, 241, 'RM-0000005', 'Test Item Manul', NULL, '109.0909', '578.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '578.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(242, NULL, NULL, 242, 'RM-0000005', 'Test Item Manul', NULL, '109.0909', '578.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '578.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(243, NULL, NULL, 243, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '40.9091', '0.0000', 4, '4.0909', 51, '10.0000', NULL, NULL, NULL, '45.0000', '0.0000', '0000-00-00', 'received', '45.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0011010100008', NULL),
(244, NULL, NULL, 244, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '40.9091', '0.0000', 4, '4.0909', 51, '10.0000', NULL, NULL, NULL, '45.0000', '0.0000', '0000-00-00', 'received', '45.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0011010100008', NULL),
(245, NULL, NULL, 245, 'RM-A-0000001', 'AC', NULL, '1170.9091', '0.0000', 4, '117.0909', 51, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'AC#00006', NULL, 'WH0000101', '0011010100008', NULL),
(246, NULL, NULL, 246, 'RM-A-0000001', 'AC', NULL, '1170.9091', '0.0000', 4, '117.0909', 51, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'AC#00006', NULL, 'WH0000101', '0011010100008', NULL),
(247, NULL, NULL, 247, 'RM-0000004', 'Test Item QC', NULL, '109.0909', '80.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '80.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(248, NULL, NULL, 248, 'RM-0000004', 'Test Item QC', NULL, '109.0909', '80.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '80.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(249, NULL, NULL, 249, 'RM-A-0000001', 'AC', NULL, '11109.0909', '999.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '999.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00005', NULL, 'WH0000101', '0011010100008', NULL),
(250, NULL, NULL, 250, 'RM-A-0000001', 'AC', NULL, '11109.0909', '999.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '999.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00005', NULL, 'WH0000101', '0011010100008', NULL),
(251, NULL, NULL, 251, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '1.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '1.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(252, NULL, NULL, 252, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '1.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '1.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(253, NULL, NULL, 253, 'RM-0000005', 'Test Item Manul', NULL, '109.0909', '0.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '0.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(254, NULL, NULL, 254, 'RM-0000005', 'Test Item Manul', NULL, '109.0909', '0.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '0.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(255, NULL, NULL, 255, 'RM-0000009', 'Test Item Co Product', NULL, '111.8182', '100.0000', 4, '11.1818', 51, '10.0000', NULL, NULL, NULL, '123.0000', '100.0000', '0000-00-00', 'received', '123.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(256, NULL, NULL, 256, 'RM-0000009', 'Test Item Co Product', NULL, '111.8182', '100.0000', 4, '11.1818', 51, '10.0000', NULL, NULL, NULL, '123.0000', '100.0000', '0000-00-00', 'received', '123.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(257, NULL, NULL, 257, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.0000', '0.0000', 4, '1.0000', 51, '10.0000', NULL, NULL, NULL, '11.0000', '0.0000', '0000-00-00', 'received', '11.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0011010100008', NULL),
(258, NULL, NULL, 258, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.0000', '0.0000', 4, '1.0000', 51, '10.0000', NULL, NULL, NULL, '11.0000', '0.0000', '0000-00-00', 'received', '11.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0011010100008', NULL),
(259, NULL, NULL, 259, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '13.6364', '0.0000', 4, '1.3636', 51, '10.0000', NULL, NULL, NULL, '15.0000', '0.0000', '0000-00-00', 'received', '15.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0011010100008', NULL),
(260, NULL, NULL, 260, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '13.6364', '0.0000', 4, '1.3636', 51, '10.0000', NULL, NULL, NULL, '15.0000', '0.0000', '0000-00-00', 'received', '15.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0011010100008', NULL),
(261, NULL, NULL, 261, 'RM-0000006', 'Test Item BackFlush', NULL, '109.0909', '40.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '40.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(262, NULL, NULL, 262, 'RM-0000006', 'Test Item BackFlush', NULL, '109.0909', '40.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '40.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(263, NULL, NULL, 263, 'RM-A-0000001', 'AC', NULL, '11109.0909', '0.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '0.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', NULL),
(264, NULL, NULL, 264, 'RM-A-0000001', 'AC', NULL, '11109.0909', '0.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '0.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', NULL),
(265, NULL, NULL, 265, 'RM-0000008', 'Test Item By Product', NULL, '118.1818', '80.0000', 4, '11.8182', 51, '10.0000', NULL, NULL, NULL, '130.0000', '80.0000', '0000-00-00', 'received', '130.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(266, NULL, NULL, 266, 'RM-0000008', 'Test Item By Product', NULL, '118.1818', '80.0000', 4, '11.8182', 51, '10.0000', NULL, NULL, NULL, '130.0000', '80.0000', '0000-00-00', 'received', '130.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(267, NULL, NULL, 267, 'RM-0000002', 'Test Item Normal', NULL, '109.0909', '103.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '103.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(268, NULL, NULL, 268, 'RM-0000002', 'Test Item Normal', NULL, '109.0909', '103.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '103.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(269, NULL, NULL, 269, 'RM-0000006', 'Test Item BackFlush', NULL, '47.2727', '0.0000', 4, '4.7273', 51, '10.0000', NULL, NULL, NULL, '52.0000', '0.0000', '0000-00-00', 'received', '52.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(270, NULL, NULL, 270, 'RM-0000006', 'Test Item BackFlush', NULL, '47.2727', '0.0000', 4, '4.7273', 51, '10.0000', NULL, NULL, NULL, '52.0000', '0.0000', '0000-00-00', 'received', '52.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(271, NULL, NULL, 271, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '0.9091', '100.0000', 4, '0.0909', 51, '10.0000', NULL, NULL, NULL, '1.0000', '100.0000', '0000-00-00', 'received', '1.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(272, NULL, NULL, 272, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '0.9091', '100.0000', 4, '0.0909', 51, '10.0000', NULL, NULL, NULL, '1.0000', '100.0000', '0000-00-00', 'received', '1.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(273, NULL, NULL, 273, 'RM-A-0000001', 'AC', NULL, '11109.0909', '0.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '0.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', NULL),
(274, NULL, NULL, 274, 'RM-A-0000001', 'AC', NULL, '11109.0909', '0.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '0.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', NULL),
(275, NULL, NULL, 275, 'RM-0000006', 'Test Item BackFlush', NULL, '109.0909', '500.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '500.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(276, NULL, NULL, 276, 'RM-0000006', 'Test Item BackFlush', NULL, '109.0909', '500.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '500.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(277, NULL, NULL, 277, 'RM-A-0000003', 'AC Moserbaer', NULL, '11109.0909', '3.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '3.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', NULL),
(278, NULL, NULL, 278, 'RM-A-0000003', 'AC Moserbaer', NULL, '11109.0909', '3.0000', 4, '1110.9091', 51, '10.0000', NULL, NULL, NULL, '12220.0000', '3.0000', '0000-00-00', 'received', '12220.0000', NULL, 'AC#00003', NULL, 'WH0000101', '0011010100008', NULL),
(279, NULL, NULL, 279, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '20.9091', '0.0000', 4, '2.0909', 51, '10.0000', NULL, NULL, NULL, '23.0000', '0.0000', '0000-00-00', 'received', '23.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(280, NULL, NULL, 280, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '20.9091', '0.0000', 4, '2.0909', 51, '10.0000', NULL, NULL, NULL, '23.0000', '0.0000', '0000-00-00', 'received', '23.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0011010100008', NULL),
(281, NULL, NULL, 281, 'RM-A-0000001', 'AC', NULL, '301.8182', '5.0000', 4, '30.1818', 51, '10.0000', NULL, NULL, NULL, '332.0000', '5.0000', '0000-00-00', 'received', '332.0000', NULL, 'AC#00004', NULL, 'WH0000101', '0011010100008', NULL),
(282, NULL, NULL, 282, 'RM-A-0000001', 'AC', NULL, '301.8182', '5.0000', 4, '30.1818', 51, '10.0000', NULL, NULL, NULL, '332.0000', '5.0000', '0000-00-00', 'received', '332.0000', NULL, 'AC#00004', NULL, 'WH0000101', '0011010100008', NULL),
(283, NULL, NULL, 283, 'RM-0000010', 'Test Item Scrap', NULL, '1121.8182', '100.0000', 4, '112.1818', 51, '10.0000', NULL, NULL, NULL, '1234.0000', '100.0000', '0000-00-00', 'received', '1234.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(284, NULL, NULL, 284, 'RM-0000010', 'Test Item Scrap', NULL, '1121.8182', '100.0000', 4, '112.1818', 51, '10.0000', NULL, NULL, NULL, '1234.0000', '100.0000', '0000-00-00', 'received', '1234.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(285, NULL, NULL, 285, 'RM-0000007', 'Test Item QC Serial', NULL, '118.1818', '10.0000', 4, '11.8182', 51, '10.0000', NULL, NULL, NULL, '130.0000', '10.0000', '0000-00-00', 'received', '130.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(286, NULL, NULL, 286, 'RM-0000007', 'Test Item QC Serial', NULL, '118.1818', '10.0000', 4, '11.8182', 51, '10.0000', NULL, NULL, NULL, '130.0000', '10.0000', '0000-00-00', 'received', '130.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(287, NULL, NULL, 287, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '27272.7273', '49801003.0000', 4, '2727.2727', 51, '10.0000', NULL, NULL, NULL, '30000.0000', '49801003.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00009', NULL, 'WH0000101', '0011010100008', NULL),
(288, NULL, NULL, 288, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '27272.7273', '49801003.0000', 4, '2727.2727', 51, '10.0000', NULL, NULL, NULL, '30000.0000', '49801003.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00009', NULL, 'WH0000101', '0011010100008', NULL),
(289, NULL, NULL, 289, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '1.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '1.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(290, NULL, NULL, 290, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '1.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '1.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(291, NULL, NULL, 291, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.9091', '0.0000', 4, '1.0909', 51, '10.0000', NULL, NULL, NULL, '12.0000', '0.0000', '0000-00-00', 'received', '12.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(292, NULL, NULL, 292, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '10.9091', '0.0000', 4, '1.0909', 51, '10.0000', NULL, NULL, NULL, '12.0000', '0.0000', '0000-00-00', 'received', '12.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(293, NULL, NULL, 293, 'RM-0000002', 'Test Item Normal', NULL, '109.0909', '2.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '2.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(294, NULL, NULL, 294, 'RM-0000002', 'Test Item Normal', NULL, '109.0909', '2.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '2.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0011010100008', NULL),
(295, NULL, NULL, 295, 'RM-0000011', 'STRAPING ROLLS', NULL, '369.0909', '9.0000', 4, '36.9091', 51, '10.0000', NULL, NULL, NULL, '406.0000', '9.0000', '0000-00-00', 'received', '406.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(296, NULL, NULL, 296, 'RM-0000011', 'STRAPING ROLLS', NULL, '369.0909', '9.0000', 4, '36.9091', 51, '10.0000', NULL, NULL, NULL, '406.0000', '9.0000', '0000-00-00', 'received', '406.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(297, NULL, NULL, 297, 'RM-A-0000003', 'AC Moserbaer', NULL, '769.0909', '3.0000', 4, '76.9091', 51, '10.0000', NULL, NULL, NULL, '846.0000', '3.0000', '0000-00-00', 'received', '846.0000', NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', NULL),
(298, NULL, NULL, 298, 'RM-A-0000003', 'AC Moserbaer', NULL, '769.0909', '3.0000', 4, '76.9091', 51, '10.0000', NULL, NULL, NULL, '846.0000', '3.0000', '0000-00-00', 'received', '846.0000', NULL, 'AC#00001', NULL, 'WH0000101', '0011010100008', NULL),
(299, NULL, NULL, 299, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '9.0909', '0.0000', 4, '0.9091', 51, '10.0000', NULL, NULL, NULL, '10.0000', '0.0000', '0000-00-00', 'received', '10.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0011010100008', NULL),
(300, NULL, NULL, 300, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '9.0909', '0.0000', 4, '0.9091', 51, '10.0000', NULL, NULL, NULL, '10.0000', '0.0000', '0000-00-00', 'received', '10.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0011010100008', NULL),
(301, NULL, NULL, 301, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '10.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '10.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(302, NULL, NULL, 302, 'RM-0000003', 'Test Item Serial', NULL, '109.0909', '10.0000', 4, '10.9091', 51, '10.0000', NULL, NULL, NULL, '120.0000', '10.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0011010100008', NULL),
(303, NULL, NULL, 303, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '13.6364', '0.0000', 4, '1.3636', 51, '10.0000', NULL, NULL, NULL, '15.0000', '0.0000', '0000-00-00', 'received', '15.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0011010100008', NULL),
(304, NULL, NULL, 304, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '13.6364', '0.0000', 4, '1.3636', 51, '10.0000', NULL, NULL, NULL, '15.0000', '0.0000', '0000-00-00', 'received', '15.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0011010100008', NULL),
(305, NULL, NULL, 305, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(306, NULL, NULL, 306, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(307, NULL, NULL, 307, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(308, NULL, NULL, 308, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(309, NULL, NULL, 309, 'RM..0000002', '28MM PILFER CAP RED', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(310, NULL, NULL, 310, 'RM..0000002', '28MM PILFER CAP RED', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(311, NULL, NULL, 311, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(312, NULL, NULL, 312, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(313, NULL, NULL, 313, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(314, NULL, NULL, 314, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(315, NULL, NULL, 315, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(316, NULL, NULL, 316, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(317, NULL, NULL, 317, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(318, NULL, NULL, 318, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(319, NULL, NULL, 319, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(320, NULL, NULL, 320, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(321, NULL, NULL, 321, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(322, NULL, NULL, 322, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(323, NULL, NULL, 323, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(324, NULL, NULL, 324, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(325, NULL, NULL, 325, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(326, NULL, NULL, 326, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(327, NULL, NULL, 327, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(328, NULL, NULL, 328, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(329, NULL, NULL, 329, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(330, NULL, NULL, 330, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(331, NULL, NULL, 331, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(332, NULL, NULL, 332, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(333, NULL, NULL, 333, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(334, NULL, NULL, 334, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(335, NULL, NULL, 335, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(336, NULL, NULL, 336, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(337, NULL, NULL, 337, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(338, NULL, NULL, 338, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(339, NULL, NULL, 339, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(340, NULL, NULL, 340, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 4, NULL, 51, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0011010100008', NULL),
(341, NULL, NULL, 341, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '1.8182', '0.0000', 5, '0.1818', 52, '10.0000', NULL, NULL, NULL, '2.0000', '0.0000', '0000-00-00', 'received', '2.0000', NULL, 'LOT#00001', NULL, 'WH0000501', '0011010100008', NULL),
(342, NULL, NULL, 342, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '5.4545', '0.0000', 5, '0.5455', 52, '10.0000', NULL, NULL, NULL, '6.0000', '0.0000', '0000-00-00', 'received', '6.0000', NULL, 'LOT#00001', NULL, 'WH0000501', '0011010100008', NULL),
(343, NULL, NULL, 343, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(344, NULL, NULL, 344, 'RM-A-0000003', 'AC Moserbaer', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(345, NULL, NULL, 345, 'RM-0000004', 'Test Item QC', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(346, NULL, NULL, 346, 'RM..0000002', '28MM PILFER CAP RED', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(347, NULL, NULL, 347, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(348, NULL, NULL, 348, 'RM-0000011', 'STRAPING ROLLS', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(349, NULL, NULL, 349, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(350, NULL, NULL, 350, 'RM-0000009', 'Test Item Co Product', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(351, NULL, NULL, 351, 'RM-0000010', 'Test Item Scrap', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(352, NULL, NULL, 352, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(353, NULL, NULL, 353, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(354, NULL, NULL, 354, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(355, NULL, NULL, 355, 'RM-0000003', 'Test Item Serial', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(356, NULL, NULL, 356, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(357, NULL, NULL, 357, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(358, NULL, NULL, 358, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(359, NULL, NULL, 359, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(360, NULL, NULL, 360, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(361, NULL, NULL, 361, 'RM-0000005', 'Test Item Manul', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(362, NULL, NULL, 362, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(363, NULL, NULL, 363, 'RM-0000002', 'Test Item Normal', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(364, NULL, NULL, 364, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(365, NULL, NULL, 365, 'RM-0000006', 'Test Item BackFlush', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(366, NULL, NULL, 366, 'RM-0000007', 'Test Item QC Serial', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(367, NULL, NULL, 367, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(368, NULL, NULL, 368, 'RM-A-0000001', 'AC', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(369, NULL, NULL, 369, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(370, NULL, NULL, 370, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(371, NULL, NULL, 371, 'RM-0000008', 'Test Item By Product', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(372, NULL, NULL, 372, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 5, NULL, 52, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0011010100008', NULL),
(373, NULL, NULL, 373, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '5.4545', '0.0000', 6, '0.5455', 53, '10.0000', NULL, NULL, NULL, '6.0000', '0.0000', '0000-00-00', 'received', '6.0000', NULL, 'LOT#00002', NULL, 'WH0000401', '0011010100008', NULL),
(374, NULL, NULL, 374, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '5.4545', '0.0000', 6, '0.5455', 53, '10.0000', NULL, NULL, NULL, '6.0000', '0.0000', '0000-00-00', 'received', '6.0000', NULL, 'LOT#00001', NULL, 'WH0000401', '0011010100008', NULL),
(375, NULL, NULL, 375, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(376, NULL, NULL, 376, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(377, NULL, NULL, 377, 'RM-A-0000003', 'AC Moserbaer', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(378, NULL, NULL, 378, 'RM-0000004', 'Test Item QC', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(379, NULL, NULL, 379, 'RM..0000002', '28MM PILFER CAP RED', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(380, NULL, NULL, 380, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(381, NULL, NULL, 381, 'RM-0000011', 'STRAPING ROLLS', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(382, NULL, NULL, 382, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(383, NULL, NULL, 383, 'RM-0000009', 'Test Item Co Product', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(384, NULL, NULL, 384, 'RM-0000010', 'Test Item Scrap', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(385, NULL, NULL, 385, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(386, NULL, NULL, 386, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(387, NULL, NULL, 387, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(388, NULL, NULL, 388, 'RM-0000003', 'Test Item Serial', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(389, NULL, NULL, 389, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(390, NULL, NULL, 390, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(391, NULL, NULL, 391, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(392, NULL, NULL, 392, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(393, NULL, NULL, 393, 'RM-0000005', 'Test Item Manul', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(394, NULL, NULL, 394, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(395, NULL, NULL, 395, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(396, NULL, NULL, 396, 'RM-0000002', 'Test Item Normal', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(397, NULL, NULL, 397, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(398, NULL, NULL, 398, 'RM-0000006', 'Test Item BackFlush', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(399, NULL, NULL, 399, 'RM-0000007', 'Test Item QC Serial', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(400, NULL, NULL, 400, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(401, NULL, NULL, 401, 'RM-A-0000001', 'AC', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(402, NULL, NULL, 402, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(403, NULL, NULL, 403, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(404, NULL, NULL, 404, 'RM-0000008', 'Test Item By Product', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(405, NULL, NULL, 405, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 6, NULL, 53, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0011010100008', NULL),
(406, NULL, NULL, 406, 'RM..0000002', '28MM PILFER CAP RED', NULL, '1.8182', '300000.0000', 7, '0.1818', 56, '10.0000', NULL, NULL, NULL, '2.0000', '300000.0000', '0000-00-00', 'received', '2.0000', NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', NULL),
(407, NULL, NULL, 407, 'RM..0000002', '28MM PILFER CAP RED', NULL, '1.8182', '287000.0000', 7, '0.1818', 56, '10.0000', NULL, NULL, NULL, '2.0000', '287000.0000', '0000-00-00', 'received', '2.0000', NULL, 'LOT#00002', NULL, 'WH0000301', '0011010100008', NULL),
(408, NULL, NULL, 408, 'RM..0000001', '1LTR CHEM. BOTTLE WHITE OPQ', NULL, '10.0000', '287000.0000', 7, '1.0000', 56, '10.0000', NULL, NULL, NULL, '11.0000', '287000.0000', '0000-00-00', 'received', '11.0000', NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', NULL),
(409, NULL, NULL, 409, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.9091', '300000.0000', 7, '0.0909', 56, '10.0000', NULL, NULL, NULL, '1.0000', '300000.0000', '0000-00-00', 'received', '1.0000', NULL, 'LOT#00001', NULL, 'WH0000301', '0011010100008', NULL),
(410, NULL, NULL, 410, 'RM..0000003', '2LTR NATURAL PLUGS', NULL, '0.9091', '287000.0000', 7, '0.0909', 56, '10.0000', NULL, NULL, NULL, '1.0000', '287000.0000', '0000-00-00', 'received', '1.0000', NULL, 'LOT#00002', NULL, 'WH0000301', '0011010100008', NULL),
(411, NULL, NULL, 411, 'RM..0000006', '60ML WHITE BOTTLE NW', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(412, NULL, NULL, 412, 'RM-0000014', 'PP-PREMIX OUTPUT', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(413, NULL, NULL, 413, 'RM-A-0000003', 'AC Moserbaer', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(414, NULL, NULL, 414, 'RM-0000004', 'Test Item QC', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(415, NULL, NULL, 415, 'RM-A-0000002', 'AC Bajaj', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(416, NULL, NULL, 416, 'RM-0000011', 'STRAPING ROLLS', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(417, NULL, NULL, 417, 'RM-0000012', 'Raw paint one', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(418, NULL, NULL, 418, 'RM-0000009', 'Test Item Co Product', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(419, NULL, NULL, 419, 'RM-0000010', 'Test Item Scrap', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(420, NULL, NULL, 420, 'RM-0000001', 'GORILLA GLASS 5MM', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `psale`) VALUES
(421, NULL, NULL, 421, 'RM-G-0000001', 'V/MATT MAGNOLIA EMULSION 20LTRS', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(422, NULL, NULL, 422, 'RM-0000003', 'Test Item Serial', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(423, NULL, NULL, 423, 'RM-0000013', 'Raw paint two', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(424, NULL, NULL, 424, 'RM..0000005', 'PESTICIDE D. GREEN WADDED CAP', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(425, NULL, NULL, 425, 'RM-0000016', 'PP-GRINDING OUTPUT', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(426, NULL, NULL, 426, 'RM-0000018', 'PP- WEIGHT -OUTPUT', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(427, NULL, NULL, 427, 'RM-0000005', 'Test Item Manul', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(428, NULL, NULL, 428, 'RM-G-0000002', 'LOARD POLISH CLEAR WAX', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(429, NULL, NULL, 429, 'RM..0000007', 'UV 28MM BLUE PILFER PRINTED CAP', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(430, NULL, NULL, 430, 'RM-0000002', 'Test Item Normal', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(431, NULL, NULL, 431, 'RM-A-0000004', 'AC Starline', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(432, NULL, NULL, 432, 'RM-0000006', 'Test Item BackFlush', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(433, NULL, NULL, 433, 'RM-0000007', 'Test Item QC Serial', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(434, NULL, NULL, 434, 'RM-0000017', 'PP-SIEVING OUTPUT', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(435, NULL, NULL, 435, 'RM-A-0000001', 'AC', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(436, NULL, NULL, 436, 'RM..A.0000001', 'AC Moserbaer two', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(437, NULL, NULL, 437, 'RM-0000015', 'PP-EXTRUSION OUTPUT', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(438, NULL, NULL, 438, 'RM-0000008', 'Test Item By Product', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(439, NULL, NULL, 439, 'RM..0000008', 'AKS Bottel', NULL, '0.0000', '0.0000', 7, NULL, 56, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0011010100008', NULL),
(440, NULL, NULL, 440, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 3, NULL, 58, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', NULL),
(441, NULL, NULL, 441, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '0.0000', '0.0000', 3, NULL, 58, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', NULL),
(442, NULL, NULL, 442, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '0.0000', '0.0000', 3, NULL, 58, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0010010100007', NULL),
(443, NULL, NULL, 443, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '181.8182', '85.0000', 4, '18.1818', 59, '10.0000', NULL, NULL, NULL, '200.0000', '85.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', NULL),
(444, NULL, NULL, 444, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '181.8182', '85.0000', 4, '18.1818', 59, '10.0000', NULL, NULL, NULL, '200.0000', '85.0000', '0000-00-00', 'received', '200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', NULL),
(445, NULL, NULL, 445, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '109.0909', '86.0000', 4, '10.9091', 59, '10.0000', NULL, NULL, NULL, '120.0000', '86.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', NULL),
(446, NULL, NULL, 446, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '109.0909', '86.0000', 4, '10.9091', 59, '10.0000', NULL, NULL, NULL, '120.0000', '86.0000', '0000-00-00', 'received', '120.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0010010100007', NULL),
(447, NULL, NULL, 447, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 4, NULL, 59, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0010010100007', NULL),
(448, NULL, NULL, 448, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 4, NULL, 59, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0010010100007', NULL),
(449, NULL, NULL, 449, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 5, NULL, 60, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', NULL),
(450, NULL, NULL, 450, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '0.0000', '0.0000', 5, NULL, 60, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', NULL),
(451, NULL, NULL, 451, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '0.0000', '0.0000', 5, NULL, 60, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0010010100007', NULL),
(452, NULL, NULL, 452, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 6, NULL, 61, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', NULL),
(453, NULL, NULL, 453, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '0.0000', '0.0000', 6, NULL, 61, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', NULL),
(454, NULL, NULL, 454, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '0.0000', '0.0000', 6, NULL, 61, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0010010100007', NULL),
(455, NULL, NULL, 455, 'ITM-0000005', 'BWIP SOKO MAIZE FLOUR', NULL, '0.0000', '0.0000', 7, NULL, 64, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', NULL),
(456, NULL, NULL, 456, 'ITM-0000004', 'SOKO 10 KG BALER', NULL, '0.0000', '0.0000', 7, NULL, 64, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', NULL),
(457, NULL, NULL, 457, 'ITM-0000003', 'SOKO 5KG BALER', NULL, '0.0000', '0.0000', 7, NULL, 64, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0010010100007', NULL),
(458, NULL, NULL, 458, 'TRA-0000001', 'NILAY', NULL, '0.0000', '0.0000', 3, NULL, 66, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0026010100012', NULL),
(459, NULL, NULL, 459, 'TRA-0000001', 'NILAY', NULL, '1530.0000', '10.0000', 4, '153.0000', 67, '10.0000', NULL, NULL, NULL, '1683.0000', '10.0000', '0000-00-00', 'received', '1683.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0026010100012', NULL),
(460, NULL, NULL, 460, 'TRA-0000001', 'NILAY', NULL, '1530.0000', '10.0000', 4, '153.0000', 67, '10.0000', NULL, NULL, NULL, '1683.0000', '10.0000', '0000-00-00', 'received', '1683.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0026010100012', NULL),
(461, NULL, NULL, 461, 'TRA-0000001', 'NILAY', NULL, '0.0000', '0.0000', 5, NULL, 68, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0026010100012', NULL),
(462, NULL, NULL, 462, 'TRA-0000001', 'NILAY', NULL, '0.0000', '0.0000', 6, NULL, 69, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0026010100012', NULL),
(463, NULL, NULL, 463, 'TRA-0000001', 'NILAY', NULL, '0.0000', '0.0000', 7, NULL, 72, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0026010100012', NULL),
(464, NULL, NULL, 464, 'CL-0000001', 'TEST LIFO ITEM', NULL, '2938.1818', '10.0000', 3, '293.8182', 74, '10.0000', NULL, NULL, NULL, '3232.0000', '10.0000', '0000-00-00', 'received', '3232.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0007010100004', NULL),
(465, NULL, NULL, 465, 'CL-0000001', 'TEST LIFO ITEM', NULL, '31.8182', '10.0000', 3, '3.1818', 74, '10.0000', NULL, NULL, NULL, '35.0000', '10.0000', '0000-00-00', 'received', '35.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0007010100004', NULL),
(466, NULL, NULL, 466, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 3, NULL, 74, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0007010100004', NULL),
(467, NULL, NULL, 467, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '0.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '0.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0007010100004', NULL),
(468, NULL, NULL, 468, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '0.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '0.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0007010100004', NULL),
(469, NULL, NULL, 469, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '867367.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '867367.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0007010100004', NULL),
(470, NULL, NULL, 470, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '867367.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '867367.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0007010100004', NULL),
(471, NULL, NULL, 471, 'CL-0000001', 'TEST LIFO ITEM', NULL, '10.9091', '0.0000', 4, '1.0909', 75, '10.0000', NULL, NULL, NULL, '12.0000', '0.0000', '0000-00-00', 'received', '12.0000', NULL, 'LOT_1', NULL, 'WH0000101', '0007010100004', NULL),
(472, NULL, NULL, 472, 'CL-0000001', 'TEST LIFO ITEM', NULL, '10.9091', '0.0000', 4, '1.0909', 75, '10.0000', NULL, NULL, NULL, '12.0000', '0.0000', '0000-00-00', 'received', '12.0000', NULL, 'LOT_1', NULL, 'WH0000101', '0007010100004', NULL),
(473, NULL, NULL, 473, 'CL-0000001', 'TEST LIFO ITEM', NULL, '26.3636', '0.0000', 4, '2.6364', 75, '10.0000', NULL, NULL, NULL, '29.0000', '0.0000', '0000-00-00', 'received', '29.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0007010100004', NULL),
(474, NULL, NULL, 474, 'CL-0000001', 'TEST LIFO ITEM', NULL, '26.3636', '0.0000', 4, '2.6364', 75, '10.0000', NULL, NULL, NULL, '29.0000', '0.0000', '0000-00-00', 'received', '29.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0007010100004', NULL),
(475, NULL, NULL, 475, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '100.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '100.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0007010100004', NULL),
(476, NULL, NULL, 476, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '100.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '100.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0007010100004', NULL),
(477, NULL, NULL, 477, 'CL-0000001', 'TEST LIFO ITEM', NULL, '45.4545', '118.0000', 4, '4.5455', 75, '10.0000', NULL, NULL, NULL, '50.0000', '118.0000', '0000-00-00', 'received', '50.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0007010100004', NULL),
(478, NULL, NULL, 478, 'CL-0000001', 'TEST LIFO ITEM', NULL, '45.4545', '118.0000', 4, '4.5455', 75, '10.0000', NULL, NULL, NULL, '50.0000', '118.0000', '0000-00-00', 'received', '50.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0007010100004', NULL),
(479, NULL, NULL, 479, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '500.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '500.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0007010100004', NULL),
(480, NULL, NULL, 480, 'CL-0000001', 'TEST LIFO ITEM', NULL, '36.3636', '500.0000', 4, '3.6364', 75, '10.0000', NULL, NULL, NULL, '40.0000', '500.0000', '0000-00-00', 'received', '40.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0007010100004', NULL),
(481, NULL, NULL, 481, 'CL-0000001', 'TEST LIFO ITEM', NULL, '566.3636', '0.0000', 4, '56.6364', 75, '10.0000', NULL, NULL, NULL, '623.0000', '0.0000', '0000-00-00', 'received', '623.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0007010100004', NULL),
(482, NULL, NULL, 482, 'CL-0000001', 'TEST LIFO ITEM', NULL, '566.3636', '0.0000', 4, '56.6364', 75, '10.0000', NULL, NULL, NULL, '623.0000', '0.0000', '0000-00-00', 'received', '623.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0007010100004', NULL),
(483, NULL, NULL, 483, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 4, NULL, 75, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0007010100004', NULL),
(484, NULL, NULL, 484, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 4, NULL, 75, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0007010100004', NULL),
(485, NULL, NULL, 485, 'CL-0000001', 'TEST LIFO ITEM', NULL, '0.0000', '0.0000', 5, NULL, 76, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0007010100004', NULL),
(486, NULL, NULL, 486, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 5, NULL, 76, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0007010100004', NULL),
(487, NULL, NULL, 487, 'CL-0000001', 'TEST LIFO ITEM', NULL, '0.0000', '0.0000', 6, NULL, 77, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0007010100004', NULL),
(488, NULL, NULL, 488, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 6, NULL, 77, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0007010100004', NULL),
(489, NULL, NULL, 489, 'CL-0000001', 'TEST LIFO ITEM', NULL, '0.0000', '0.0000', 7, NULL, 80, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0007010100004', NULL),
(490, NULL, NULL, 490, 'CL-BL-0000001', 'TEST LIFO ITEM NEW', NULL, '0.0000', '0.0000', 7, NULL, 80, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0007010100004', NULL),
(491, NULL, NULL, 491, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '231.8182', '2.0000', 3, '23.1818', 82, '10.0000', NULL, NULL, NULL, '255.0000', '2.0000', '0000-00-00', 'received', '255.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0006010100003', NULL),
(492, NULL, NULL, 492, 'BL-0000001', 'Swing machin', NULL, '68.1818', '4.0000', 3, '6.8182', 82, '10.0000', NULL, NULL, NULL, '75.0000', '4.0000', '0000-00-00', 'received', '75.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', NULL),
(493, NULL, NULL, 493, 'ITM-0000002', 'bike tool kit', NULL, '34.5455', '5.0000', 3, '3.4545', 82, '10.0000', NULL, NULL, NULL, '38.0000', '5.0000', '0000-00-00', 'received', '38.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0006010100003', NULL),
(494, NULL, NULL, 494, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '1090.9091', '1.0000', 3, '109.0909', 82, '10.0000', NULL, NULL, NULL, '1200.0000', '1.0000', '0000-00-00', 'received', '1200.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', NULL),
(495, NULL, NULL, 495, 'ITM-0000002', 'bike tool kit', NULL, '2470.9091', '10.0000', 3, '247.0909', 82, '10.0000', NULL, NULL, NULL, '2718.0000', '10.0000', '0000-00-00', 'received', '2718.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0006010100003', NULL),
(496, NULL, NULL, 496, 'ITM-0000002', 'bike tool kit', NULL, '2112.7273', '17.0000', 3, '211.2727', 82, '10.0000', NULL, NULL, NULL, '2324.0000', '17.0000', '0000-00-00', 'received', '2324.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', NULL),
(497, NULL, NULL, 497, 'ITM-0000001', 'grinder mixer', NULL, '350.9091', '4.0000', 3, '35.0909', 82, '10.0000', NULL, NULL, NULL, '386.0000', '4.0000', '0000-00-00', 'received', '386.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0006010100003', NULL),
(498, NULL, NULL, 498, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(499, NULL, NULL, 499, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(500, NULL, NULL, 500, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(501, NULL, NULL, 501, 'ITM-0000008', 'Laptop', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(502, NULL, NULL, 502, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(503, NULL, NULL, 503, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 3, NULL, 82, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0006010100003', NULL),
(504, NULL, NULL, 504, 'BL-0000001', 'Swing machin', NULL, '301.8182', '10.0000', 4, '30.1818', 83, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(505, NULL, NULL, 505, 'BL-0000001', 'Swing machin', NULL, '301.8182', '10.0000', 4, '30.1818', 83, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(506, NULL, NULL, 506, 'ITM-0000002', 'bike tool kit', NULL, '1618.1818', '10.0000', 4, '161.8182', 83, '10.0000', NULL, NULL, NULL, '1780.0000', '10.0000', '0000-00-00', 'received', '1780.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0006010100003', NULL),
(507, NULL, NULL, 507, 'ITM-0000002', 'bike tool kit', NULL, '1618.1818', '10.0000', 4, '161.8182', 83, '10.0000', NULL, NULL, NULL, '1780.0000', '10.0000', '0000-00-00', 'received', '1780.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0006010100003', NULL),
(508, NULL, NULL, 508, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '301.8182', '10.0000', 4, '30.1818', 83, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(509, NULL, NULL, 509, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '301.8182', '10.0000', 4, '30.1818', 83, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(510, NULL, NULL, 510, 'ITM-0000008', 'Laptop', NULL, '2090.9091', '3.0000', 4, '209.0909', 83, '10.0000', NULL, NULL, NULL, '2300.0000', '3.0000', '0000-00-00', 'received', '2300.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(511, NULL, NULL, 511, 'ITM-0000008', 'Laptop', NULL, '2090.9091', '3.0000', 4, '209.0909', 83, '10.0000', NULL, NULL, NULL, '2300.0000', '3.0000', '0000-00-00', 'received', '2300.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(512, NULL, NULL, 512, 'ITM-0000001', 'grinder mixer', NULL, '1170.9091', '0.0000', 4, '117.0909', 83, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(513, NULL, NULL, 513, 'ITM-0000001', 'grinder mixer', NULL, '1170.9091', '0.0000', 4, '117.0909', 83, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(514, NULL, NULL, 514, 'ITM-0000002', 'bike tool kit', NULL, '918.1818', '7.0000', 4, '91.8182', 83, '10.0000', NULL, NULL, NULL, '1010.0000', '7.0000', '0000-00-00', 'received', '1010.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(515, NULL, NULL, 515, 'ITM-0000002', 'bike tool kit', NULL, '918.1818', '7.0000', 4, '91.8182', 83, '10.0000', NULL, NULL, NULL, '1010.0000', '7.0000', '0000-00-00', 'received', '1010.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0006010100003', NULL),
(516, NULL, NULL, 516, 'ITM-0000002', 'bike tool kit', NULL, '1668.1818', '9.0000', 4, '166.8182', 83, '10.0000', NULL, NULL, NULL, '1835.0000', '9.0000', '0000-00-00', 'received', '1835.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0006010100003', NULL),
(517, NULL, NULL, 517, 'ITM-0000002', 'bike tool kit', NULL, '1668.1818', '9.0000', 4, '166.8182', 83, '10.0000', NULL, NULL, NULL, '1835.0000', '9.0000', '0000-00-00', 'received', '1835.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0006010100003', NULL),
(518, NULL, NULL, 518, 'ITM-0000002', 'bike tool kit', NULL, '2072.7273', '2000000.0000', 4, '207.2727', 83, '10.0000', NULL, NULL, NULL, '2280.0000', '2000000.0000', '0000-00-00', 'received', '2280.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0006010100003', NULL),
(519, NULL, NULL, 519, 'ITM-0000002', 'bike tool kit', NULL, '2072.7273', '2000000.0000', 4, '207.2727', 83, '10.0000', NULL, NULL, NULL, '2280.0000', '2000000.0000', '0000-00-00', 'received', '2280.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0006010100003', NULL),
(520, NULL, NULL, 520, 'ITM-0000002', 'bike tool kit', NULL, '1818.1818', '10.0000', 4, '181.8182', 83, '10.0000', NULL, NULL, NULL, '2000.0000', '10.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0006010100003', NULL),
(521, NULL, NULL, 521, 'ITM-0000002', 'bike tool kit', NULL, '1818.1818', '10.0000', 4, '181.8182', 83, '10.0000', NULL, NULL, NULL, '2000.0000', '10.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0006010100003', NULL),
(522, NULL, NULL, 522, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '971.8182', '9.0000', 4, '97.1818', 83, '10.0000', NULL, NULL, NULL, '1069.0000', '9.0000', '0000-00-00', 'received', '1069.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(523, NULL, NULL, 523, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '971.8182', '9.0000', 4, '97.1818', 83, '10.0000', NULL, NULL, NULL, '1069.0000', '9.0000', '0000-00-00', 'received', '1069.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(524, NULL, NULL, 524, 'ITM-0000002', 'bike tool kit', NULL, '1710.9091', '50000.0000', 4, '171.0909', 83, '10.0000', NULL, NULL, NULL, '1882.0000', '50000.0000', '0000-00-00', 'received', '1882.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0006010100003', NULL),
(525, NULL, NULL, 525, 'ITM-0000002', 'bike tool kit', NULL, '1710.9091', '50000.0000', 4, '171.0909', 83, '10.0000', NULL, NULL, NULL, '1882.0000', '50000.0000', '0000-00-00', 'received', '1882.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0006010100003', NULL),
(526, NULL, NULL, 526, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '690.0000', '10.0000', 4, '69.0000', 83, '10.0000', NULL, NULL, NULL, '759.0000', '10.0000', '0000-00-00', 'received', '759.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(527, NULL, NULL, 527, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '690.0000', '10.0000', 4, '69.0000', 83, '10.0000', NULL, NULL, NULL, '759.0000', '10.0000', '0000-00-00', 'received', '759.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(528, NULL, NULL, 528, 'ITM-0000002', 'bike tool kit', NULL, '4090.9091', '0.0000', 4, '409.0909', 83, '10.0000', NULL, NULL, NULL, '4500.0000', '0.0000', '0000-00-00', 'received', '4500.0000', NULL, 'LOT_1', NULL, 'WH0000101', '0006010100003', NULL),
(529, NULL, NULL, 529, 'ITM-0000002', 'bike tool kit', NULL, '4090.9091', '0.0000', 4, '409.0909', 83, '10.0000', NULL, NULL, NULL, '4500.0000', '0.0000', '0000-00-00', 'received', '4500.0000', NULL, 'LOT_1', NULL, 'WH0000101', '0006010100003', NULL),
(530, NULL, NULL, 530, 'ITM-0000002', 'bike tool kit', NULL, '1714.5455', '10.0000', 4, '171.4545', 83, '10.0000', NULL, NULL, NULL, '1886.0000', '10.0000', '0000-00-00', 'received', '1886.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0006010100003', NULL),
(531, NULL, NULL, 531, 'ITM-0000002', 'bike tool kit', NULL, '1714.5455', '10.0000', 4, '171.4545', 83, '10.0000', NULL, NULL, NULL, '1886.0000', '10.0000', '0000-00-00', 'received', '1886.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0006010100003', NULL),
(532, NULL, NULL, 532, 'ITM-0000001', 'grinder mixer', NULL, '369.0909', '5.0000', 4, '36.9091', 83, '10.0000', NULL, NULL, NULL, '406.0000', '5.0000', '0000-00-00', 'received', '406.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(533, NULL, NULL, 533, 'ITM-0000001', 'grinder mixer', NULL, '369.0909', '5.0000', 4, '36.9091', 83, '10.0000', NULL, NULL, NULL, '406.0000', '5.0000', '0000-00-00', 'received', '406.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0006010100003', NULL),
(534, NULL, NULL, 534, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(535, NULL, NULL, 535, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(536, NULL, NULL, 536, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(537, NULL, NULL, 537, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(538, NULL, NULL, 538, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(539, NULL, NULL, 539, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(540, NULL, NULL, 540, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(541, NULL, NULL, 541, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 4, NULL, 83, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0006010100003', NULL),
(542, NULL, NULL, 542, 'ITM-0000002', 'bike tool kit', NULL, '920.0000', '10.0000', 5, '92.0000', 84, '10.0000', NULL, NULL, NULL, '1012.0000', '10.0000', '0000-00-00', 'received', '1012.0000', NULL, 'LOT#00001', NULL, 'WH0000501', '0006010100003', NULL),
(543, NULL, NULL, 543, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(544, NULL, NULL, 544, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(545, NULL, NULL, 545, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(546, NULL, NULL, 546, 'BL-0000001', 'Swing machin', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(547, NULL, NULL, 547, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(548, NULL, NULL, 548, 'ITM-0000001', 'grinder mixer', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(549, NULL, NULL, 549, 'ITM-0000008', 'Laptop', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(550, NULL, NULL, 550, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(551, NULL, NULL, 551, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 5, NULL, 84, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0006010100003', NULL),
(552, NULL, NULL, 552, 'ITM-0000002', 'bike tool kit', NULL, '920.0000', '10.0000', 6, '92.0000', 85, '10.0000', NULL, NULL, NULL, '1012.0000', '10.0000', '0000-00-00', 'received', '1012.0000', NULL, 'LOT#00001', NULL, 'WH0000401', '0006010100003', NULL),
(553, NULL, NULL, 553, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(554, NULL, NULL, 554, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(555, NULL, NULL, 555, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(556, NULL, NULL, 556, 'BL-0000001', 'Swing machin', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(557, NULL, NULL, 557, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(558, NULL, NULL, 558, 'ITM-0000001', 'grinder mixer', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(559, NULL, NULL, 559, 'ITM-0000008', 'Laptop', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(560, NULL, NULL, 560, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(561, NULL, NULL, 561, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 6, NULL, 85, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0006010100003', NULL),
(562, NULL, NULL, 562, 'ITM-0000002', 'bike tool kit', NULL, '920.0000', '10.0000', 7, '92.0000', 88, '10.0000', NULL, NULL, NULL, '1012.0000', '10.0000', '0000-00-00', 'received', '1012.0000', NULL, 'LOT#00001', NULL, 'WH0000301', '0006010100003', NULL),
(563, NULL, NULL, 563, 'ITM-0000002', 'bike tool kit', NULL, '925.4545', '10.0000', 7, '92.5455', 88, '10.0000', NULL, NULL, NULL, '1018.0000', '10.0000', '0000-00-00', 'received', '1018.0000', NULL, 'LOT#00002', NULL, 'WH0000301', '0006010100003', NULL),
(564, NULL, NULL, 564, 'G-0000001', 'Machine Condenser', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(565, NULL, NULL, 565, 'BLK-0000001', 'BENDING MACHINE MOTOR REWINDING', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(566, NULL, NULL, 566, 'M-0000001', 'SPOTLIGHT SCREW TYPE 250W', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(567, NULL, NULL, 567, 'BL-0000001', 'Swing machin', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(568, NULL, NULL, 568, 'ITM-0000006', 'Printer', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(569, NULL, NULL, 569, 'ITM-0000001', 'grinder mixer', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(570, NULL, NULL, 570, 'ITM-0000008', 'Laptop', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(571, NULL, NULL, 571, 'CIG..0000001', 'TEST ITN', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(572, NULL, NULL, 572, 'ITM-0000007', 'Dryer', NULL, '0.0000', '0.0000', 7, NULL, 88, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0006010100003', NULL),
(573, NULL, NULL, 573, 'CV-BLK-0000001', 'Car', NULL, '163.6364', '5.0000', 3, '16.3636', 90, '10.0000', NULL, NULL, NULL, '180.0000', '5.0000', '0000-00-00', 'received', '180.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0012010100009', NULL),
(574, NULL, NULL, 574, 'CV-BLK-0000001', 'Car', NULL, '18181.8182', '2.0000', 3, '1818.1818', 90, '10.0000', NULL, NULL, NULL, '20000.0000', '2.0000', '0000-00-00', 'received', '20000.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', NULL),
(575, NULL, NULL, 575, 'FW-0000002', 'Mini Truck', NULL, '69129.0909', '8.0000', 3, '6912.9091', 90, '10.0000', NULL, NULL, NULL, '76042.0000', '8.0000', '0000-00-00', 'received', '76042.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', NULL),
(576, NULL, NULL, 576, 'FW-0000005', 'MNF Input1', NULL, '1110.0000', '100.0000', 3, '111.0000', 90, '10.0000', NULL, NULL, NULL, '1221.0000', '100.0000', '0000-00-00', 'received', '1221.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0012010100009', NULL),
(577, NULL, NULL, 577, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(578, NULL, NULL, 578, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(579, NULL, NULL, 579, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(580, NULL, NULL, 580, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(581, NULL, NULL, 581, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(582, NULL, NULL, 582, 'CV.FW..0000002', 'Fortuner', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(583, NULL, NULL, 583, 'FV-0000001', 'AULTO 800', NULL, '0.0000', '0.0000', 3, NULL, 90, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0012010100009', NULL),
(584, NULL, NULL, 584, 'CV-BLK-0000001', 'Car', NULL, '18181.8182', '1.0000', 4, '1818.1818', 91, '10.0000', NULL, NULL, NULL, '20000.0000', '1.0000', '0000-00-00', 'received', '20000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(585, NULL, NULL, 585, 'CV-BLK-0000001', 'Car', NULL, '18181.8182', '1.0000', 4, '1818.1818', 91, '10.0000', NULL, NULL, NULL, '20000.0000', '1.0000', '0000-00-00', 'received', '20000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(586, NULL, NULL, 586, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '1963.6364', '10.0000', 4, '196.3636', 91, '10.0000', NULL, NULL, NULL, '2160.0000', '10.0000', '0000-00-00', 'received', '2160.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(587, NULL, NULL, 587, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '1963.6364', '10.0000', 4, '196.3636', 91, '10.0000', NULL, NULL, NULL, '2160.0000', '10.0000', '0000-00-00', 'received', '2160.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(588, NULL, NULL, 588, 'FW-0000005', 'MNF Input1', NULL, '667.2727', '97.0000', 4, '66.7273', 91, '10.0000', NULL, NULL, NULL, '734.0000', '97.0000', '0000-00-00', 'received', '734.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(589, NULL, NULL, 589, 'FW-0000005', 'MNF Input1', NULL, '667.2727', '97.0000', 4, '66.7273', 91, '10.0000', NULL, NULL, NULL, '734.0000', '97.0000', '0000-00-00', 'received', '734.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(590, NULL, NULL, 590, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '292.7273', '257005.0000', 4, '29.2727', 91, '10.0000', NULL, NULL, NULL, '322.0000', '257005.0000', '0000-00-00', 'received', '322.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(591, NULL, NULL, 591, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '292.7273', '257005.0000', 4, '29.2727', 91, '10.0000', NULL, NULL, NULL, '322.0000', '257005.0000', '0000-00-00', 'received', '322.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(592, NULL, NULL, 592, 'FW-0000002', 'Mini Truck', NULL, '195.4545', '300000.0000', 4, '19.5455', 91, '10.0000', NULL, NULL, NULL, '215.0000', '300000.0000', '0000-00-00', 'received', '215.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0012010100009', NULL),
(593, NULL, NULL, 593, 'FW-0000002', 'Mini Truck', NULL, '195.4545', '300000.0000', 4, '19.5455', 91, '10.0000', NULL, NULL, NULL, '215.0000', '300000.0000', '0000-00-00', 'received', '215.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0012010100009', NULL),
(594, NULL, NULL, 594, 'FV-0000001', 'AULTO 800', NULL, '22727.2727', '1.0000', 4, '2272.7273', 91, '10.0000', NULL, NULL, NULL, '25000.0000', '1.0000', '0000-00-00', 'received', '25000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(595, NULL, NULL, 595, 'FV-0000001', 'AULTO 800', NULL, '22727.2727', '1.0000', 4, '2272.7273', 91, '10.0000', NULL, NULL, NULL, '25000.0000', '1.0000', '0000-00-00', 'received', '25000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(596, NULL, NULL, 596, 'FW-0000002', 'Mini Truck', NULL, '1170.9091', '0.0000', 4, '117.0909', 91, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(597, NULL, NULL, 597, 'FW-0000002', 'Mini Truck', NULL, '1170.9091', '0.0000', 4, '117.0909', 91, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(598, NULL, NULL, 598, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '1170.9091', '0.0000', 4, '117.0909', 91, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(599, NULL, NULL, 599, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '1170.9091', '0.0000', 4, '117.0909', 91, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(600, NULL, NULL, 600, 'FW-0000005', 'MNF Input1', NULL, '1510.0000', '0.0000', 4, '151.0000', 91, '10.0000', NULL, NULL, NULL, '1661.0000', '0.0000', '0000-00-00', 'received', '1661.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(601, NULL, NULL, 601, 'FW-0000005', 'MNF Input1', NULL, '1510.0000', '0.0000', 4, '151.0000', 91, '10.0000', NULL, NULL, NULL, '1661.0000', '0.0000', '0000-00-00', 'received', '1661.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0012010100009', NULL),
(602, NULL, NULL, 602, 'CV.FW..0000002', 'Fortuner', NULL, '10957.2727', '4.0000', 4, '1095.7273', 91, '10.0000', NULL, NULL, NULL, '12053.0000', '4.0000', '0000-00-00', 'received', '12053.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(603, NULL, NULL, 603, 'CV.FW..0000002', 'Fortuner', NULL, '10957.2727', '4.0000', 4, '1095.7273', 91, '10.0000', NULL, NULL, NULL, '12053.0000', '4.0000', '0000-00-00', 'received', '12053.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(604, NULL, NULL, 604, 'FW-0000002', 'Mini Truck', NULL, '4319.0909', '10.0000', 4, '431.9091', 91, '10.0000', NULL, NULL, NULL, '4751.0000', '10.0000', '0000-00-00', 'received', '4751.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(605, NULL, NULL, 605, 'FW-0000002', 'Mini Truck', NULL, '4319.0909', '10.0000', 4, '431.9091', 91, '10.0000', NULL, NULL, NULL, '4751.0000', '10.0000', '0000-00-00', 'received', '4751.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0012010100009', NULL),
(606, NULL, NULL, 606, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(607, NULL, NULL, 607, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(608, NULL, NULL, 608, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(609, NULL, NULL, 609, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(610, NULL, NULL, 610, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(611, NULL, NULL, 611, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 4, NULL, 91, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0012010100009', NULL),
(612, NULL, NULL, 612, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(613, NULL, NULL, 613, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(614, NULL, NULL, 614, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(615, NULL, NULL, 615, 'FW-0000002', 'Mini Truck', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(616, NULL, NULL, 616, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(617, NULL, NULL, 617, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(618, NULL, NULL, 618, 'CV.FW..0000002', 'Fortuner', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(619, NULL, NULL, 619, 'FW-0000005', 'MNF Input1', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(620, NULL, NULL, 620, 'FV-0000001', 'AULTO 800', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(621, NULL, NULL, 621, 'CV-BLK-0000001', 'Car', NULL, '0.0000', '0.0000', 5, NULL, 92, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0012010100009', NULL),
(622, NULL, NULL, 622, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(623, NULL, NULL, 623, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(624, NULL, NULL, 624, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(625, NULL, NULL, 625, 'FW-0000002', 'Mini Truck', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(626, NULL, NULL, 626, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(627, NULL, NULL, 627, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(628, NULL, NULL, 628, 'CV.FW..0000002', 'Fortuner', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `psale`) VALUES
(629, NULL, NULL, 629, 'FW-0000005', 'MNF Input1', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(630, NULL, NULL, 630, 'FV-0000001', 'AULTO 800', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(631, NULL, NULL, 631, 'CV-BLK-0000001', 'Car', NULL, '0.0000', '0.0000', 6, NULL, 93, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0012010100009', NULL),
(632, NULL, NULL, 632, 'CV.FW..0000001', 'Ashok Leyland Trucks', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(633, NULL, NULL, 633, 'FW-0000003', 'AULTO LC 800', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(634, NULL, NULL, 634, 'FW-RD-0000001', 'Hero Honda Splendor', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(635, NULL, NULL, 635, 'FW-0000002', 'Mini Truck', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(636, NULL, NULL, 636, 'FW-0000001', 'Tata ACE 407', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(637, NULL, NULL, 637, 'FW-0000004', 'Marauti Baleno', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(638, NULL, NULL, 638, 'CV.FW..0000002', 'Fortuner', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(639, NULL, NULL, 639, 'FW-0000005', 'MNF Input1', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(640, NULL, NULL, 640, 'FV-0000001', 'AULTO 800', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(641, NULL, NULL, 641, 'CV-BLK-0000001', 'Car', NULL, '0.0000', '0.0000', 7, NULL, 96, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0012010100009', NULL),
(642, NULL, NULL, 642, 'CV-0000001', 'Tata Indica', NULL, '2959.0909', '4.0000', 3, '295.9091', 98, '10.0000', NULL, NULL, NULL, '3255.0000', '4.0000', '0000-00-00', 'received', '3255.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0004010100001', NULL),
(643, NULL, NULL, 643, 'CV-0000005', 'Paint', NULL, '0.0000', '0.0000', 3, NULL, 98, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', NULL),
(644, NULL, NULL, 644, 'CV-0000002', 'TATA 247', NULL, '0.0000', '0.0000', 3, NULL, 98, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', NULL),
(645, NULL, NULL, 645, 'CV-0000004', 'INNOVA', NULL, '0.0000', '0.0000', 3, NULL, 98, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', NULL),
(646, NULL, NULL, 646, 'CV-0000003', 'Marauti Van Cab', NULL, '0.0000', '0.0000', 3, NULL, 98, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0004010100001', NULL),
(647, NULL, NULL, 647, 'CV-0000004', 'INNOVA', NULL, '1170.9091', '0.0000', 4, '117.0909', 99, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', NULL),
(648, NULL, NULL, 648, 'CV-0000004', 'INNOVA', NULL, '1170.9091', '0.0000', 4, '117.0909', 99, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', NULL),
(649, NULL, NULL, 649, 'CV-0000005', 'Paint', NULL, '6023.6364', '1.0000', 4, '602.3636', 99, '10.0000', NULL, NULL, NULL, '6626.0000', '1.0000', '0000-00-00', 'received', '6626.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(650, NULL, NULL, 650, 'CV-0000005', 'Paint', NULL, '6023.6364', '1.0000', 4, '602.3636', 99, '10.0000', NULL, NULL, NULL, '6626.0000', '1.0000', '0000-00-00', 'received', '6626.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(651, NULL, NULL, 651, 'CV-0000003', 'Marauti Van Cab', NULL, '1090.9091', '3.0000', 4, '109.0909', 99, '10.0000', NULL, NULL, NULL, '1200.0000', '3.0000', '0000-00-00', 'received', '1200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(652, NULL, NULL, 652, 'CV-0000003', 'Marauti Van Cab', NULL, '1090.9091', '3.0000', 4, '109.0909', 99, '10.0000', NULL, NULL, NULL, '1200.0000', '3.0000', '0000-00-00', 'received', '1200.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(653, NULL, NULL, 653, 'CV-0000002', 'TATA 247', NULL, '1036363.6364', '1.0000', 4, '103636.3636', 99, '10.0000', NULL, NULL, NULL, '1140000.0000', '1.0000', '0000-00-00', 'received', '1140000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(654, NULL, NULL, 654, 'CV-0000002', 'TATA 247', NULL, '1036363.6364', '1.0000', 4, '103636.3636', 99, '10.0000', NULL, NULL, NULL, '1140000.0000', '1.0000', '0000-00-00', 'received', '1140000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(655, NULL, NULL, 655, 'CV-0000004', 'INNOVA', NULL, '195.4545', '250005.0000', 4, '19.5455', 99, '10.0000', NULL, NULL, NULL, '215.0000', '250005.0000', '0000-00-00', 'received', '215.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0004010100001', NULL),
(656, NULL, NULL, 656, 'CV-0000004', 'INNOVA', NULL, '195.4545', '250005.0000', 4, '19.5455', 99, '10.0000', NULL, NULL, NULL, '215.0000', '250005.0000', '0000-00-00', 'received', '215.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0004010100001', NULL),
(657, NULL, NULL, 657, 'CV-0000003', 'Marauti Van Cab', NULL, '1109.0909', '3.0000', 4, '110.9091', 99, '10.0000', NULL, NULL, NULL, '1220.0000', '3.0000', '0000-00-00', 'received', '1220.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', NULL),
(658, NULL, NULL, 658, 'CV-0000003', 'Marauti Van Cab', NULL, '1109.0909', '3.0000', 4, '110.9091', 99, '10.0000', NULL, NULL, NULL, '1220.0000', '3.0000', '0000-00-00', 'received', '1220.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0004010100001', NULL),
(659, NULL, NULL, 659, 'CV-0000004', 'INNOVA', NULL, '483291.8182', '2.0000', 4, '48329.1818', 99, '10.0000', NULL, NULL, NULL, '531621.0000', '2.0000', '0000-00-00', 'received', '531621.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(660, NULL, NULL, 660, 'CV-0000004', 'INNOVA', NULL, '483291.8182', '2.0000', 4, '48329.1818', 99, '10.0000', NULL, NULL, NULL, '531621.0000', '2.0000', '0000-00-00', 'received', '531621.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0004010100001', NULL),
(661, NULL, NULL, 661, 'CV-0000001', 'Tata Indica', NULL, '0.0000', '0.0000', 4, NULL, 99, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0004010100001', NULL),
(662, NULL, NULL, 662, 'CV-0000001', 'Tata Indica', NULL, '0.0000', '0.0000', 4, NULL, 99, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0004010100001', NULL),
(663, NULL, NULL, 663, 'CV-0000005', 'Paint', NULL, '0.0000', '0.0000', 5, NULL, 100, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', NULL),
(664, NULL, NULL, 664, 'CV-0000002', 'TATA 247', NULL, '0.0000', '0.0000', 5, NULL, 100, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', NULL),
(665, NULL, NULL, 665, 'CV-0000004', 'INNOVA', NULL, '0.0000', '0.0000', 5, NULL, 100, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', NULL),
(666, NULL, NULL, 666, 'CV-0000001', 'Tata Indica', NULL, '0.0000', '0.0000', 5, NULL, 100, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', NULL),
(667, NULL, NULL, 667, 'CV-0000003', 'Marauti Van Cab', NULL, '0.0000', '0.0000', 5, NULL, 100, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0004010100001', NULL),
(668, NULL, NULL, 668, 'CV-0000005', 'Paint', NULL, '0.0000', '0.0000', 6, NULL, 101, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', NULL),
(669, NULL, NULL, 669, 'CV-0000002', 'TATA 247', NULL, '0.0000', '0.0000', 6, NULL, 101, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', NULL),
(670, NULL, NULL, 670, 'CV-0000004', 'INNOVA', NULL, '0.0000', '0.0000', 6, NULL, 101, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', NULL),
(671, NULL, NULL, 671, 'CV-0000001', 'Tata Indica', NULL, '0.0000', '0.0000', 6, NULL, 101, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', NULL),
(672, NULL, NULL, 672, 'CV-0000003', 'Marauti Van Cab', NULL, '0.0000', '0.0000', 6, NULL, 101, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0004010100001', NULL),
(673, NULL, NULL, 673, 'CV-0000005', 'Paint', NULL, '0.0000', '0.0000', 7, NULL, 104, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', NULL),
(674, NULL, NULL, 674, 'CV-0000002', 'TATA 247', NULL, '0.0000', '0.0000', 7, NULL, 104, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', NULL),
(675, NULL, NULL, 675, 'CV-0000004', 'INNOVA', NULL, '0.0000', '0.0000', 7, NULL, 104, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', NULL),
(676, NULL, NULL, 676, 'CV-0000001', 'Tata Indica', NULL, '0.0000', '0.0000', 7, NULL, 104, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', NULL),
(677, NULL, NULL, 677, 'CV-0000003', 'Marauti Van Cab', NULL, '0.0000', '0.0000', 7, NULL, 104, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0004010100001', NULL),
(678, NULL, NULL, 678, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '360.9091', '190.0000', 3, '36.0909', 122, '10.0000', NULL, NULL, NULL, '397.0000', '190.0000', '0000-00-00', 'received', '397.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', NULL),
(679, NULL, NULL, 679, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '3268.1818', '10.0000', 3, '326.8182', 122, '10.0000', NULL, NULL, NULL, '3595.0000', '10.0000', '0000-00-00', 'received', '3595.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', NULL),
(680, NULL, NULL, 680, 'CV-RD-0000001', 'Bike', NULL, '125.4545', '5.0000', 3, '12.5455', 122, '10.0000', NULL, NULL, NULL, '138.0000', '5.0000', '0000-00-00', 'received', '138.0000', NULL, 'LOT#00005', NULL, 'WH0000201', '0016010100010', NULL),
(681, NULL, NULL, 681, 'CV-RD-0000001', 'Bike', NULL, '1223.6364', '10.0000', 3, '122.3636', 122, '10.0000', NULL, NULL, NULL, '1346.0000', '10.0000', '0000-00-00', 'received', '1346.0000', NULL, 'LOT#00004', NULL, 'WH0000201', '0016010100010', NULL),
(682, NULL, NULL, 682, 'CV-RD-0000001', 'Bike', NULL, '2676.3636', '4.0000', 3, '267.6364', 122, '10.0000', NULL, NULL, NULL, '2944.0000', '4.0000', '0000-00-00', 'received', '2944.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', NULL),
(683, NULL, NULL, 683, 'TW-0000002', 'Bike accessories', NULL, '1836.3636', '16.0000', 3, '183.6364', 122, '10.0000', NULL, NULL, NULL, '2020.0000', '16.0000', '0000-00-00', 'received', '2020.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', NULL),
(684, NULL, NULL, 684, 'TW-0000002', 'Bike accessories', NULL, '51.8182', '5.0000', 3, '5.1818', 122, '10.0000', NULL, NULL, NULL, '57.0000', '5.0000', '0000-00-00', 'received', '57.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', NULL),
(685, NULL, NULL, 685, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '1818.1818', '1.0000', 3, '181.8182', 122, '10.0000', NULL, NULL, NULL, '2000.0000', '1.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', NULL),
(686, NULL, NULL, 686, 'TW-0000002', 'Bike accessories', NULL, '3131.8182', '10.0000', 3, '313.1818', 122, '10.0000', NULL, NULL, NULL, '3445.0000', '10.0000', '0000-00-00', 'received', '3445.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', NULL),
(687, NULL, NULL, 687, 'CV-RD-0000001', 'Bike', NULL, '13636.3636', '0.0000', 3, '1363.6364', 122, '10.0000', NULL, NULL, NULL, '15000.0000', '0.0000', '0000-00-00', 'received', '15000.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', NULL),
(688, NULL, NULL, 688, 'TW-0000002', 'Bike accessories', NULL, '5177.2727', '97.0000', 3, '517.7273', 122, '10.0000', NULL, NULL, NULL, '5695.0000', '97.0000', '0000-00-00', 'received', '5695.0000', NULL, 'LOT#00004', NULL, 'WH0000201', '0016010100010', NULL),
(689, NULL, NULL, 689, 'CV-RD-0000001', 'Bike', NULL, '13636.3636', '5.0000', 3, '1363.6364', 122, '10.0000', NULL, NULL, NULL, '15000.0000', '5.0000', '0000-00-00', 'received', '15000.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', NULL),
(690, NULL, NULL, 690, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '328.1818', '10.0000', 3, '32.8182', 122, '10.0000', NULL, NULL, NULL, '361.0000', '10.0000', '0000-00-00', 'received', '361.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', NULL),
(691, NULL, NULL, 691, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '2065.4545', '80.0000', 3, '206.5455', 122, '10.0000', NULL, NULL, NULL, '2272.0000', '80.0000', '0000-00-00', 'received', '2272.0000', NULL, 'LOT#00003', NULL, 'WH0000201', '0016010100010', NULL),
(692, NULL, NULL, 692, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '1036.3636', '10.0000', 3, '103.6364', 122, '10.0000', NULL, NULL, NULL, '1140.0000', '10.0000', '0000-00-00', 'received', '1140.0000', NULL, 'LOT#00001', NULL, 'WH0000201', '0016010100010', NULL),
(693, NULL, NULL, 693, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '21.8182', '10.0000', 3, '2.1818', 122, '10.0000', NULL, NULL, NULL, '24.0000', '10.0000', '0000-00-00', 'received', '24.0000', NULL, 'LOT#00002', NULL, 'WH0000201', '0016010100010', NULL),
(694, NULL, NULL, 694, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(695, NULL, NULL, 695, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(696, NULL, NULL, 696, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(697, NULL, NULL, 697, 'TW-0000009', 'MNF tool', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(698, NULL, NULL, 698, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(699, NULL, NULL, 699, 'TW-0000006', 'MNF Input 2', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(700, NULL, NULL, 700, 'TW-RD-0000002', 'CBZ', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(701, NULL, NULL, 701, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(702, NULL, NULL, 702, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(703, NULL, NULL, 703, 'TW-0000001', 'WIP Bike', NULL, '0.0000', '0.0000', 3, NULL, 122, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000201', '0016010100010', NULL),
(704, NULL, NULL, 704, 'TW-0000009', 'MNF tool', NULL, '70.9091', '100.0000', 4, '7.0909', 123, '10.0000', NULL, NULL, NULL, '78.0000', '100.0000', '0000-00-00', 'received', '78.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(705, NULL, NULL, 705, 'TW-0000009', 'MNF tool', NULL, '70.9091', '100.0000', 4, '7.0909', 123, '10.0000', NULL, NULL, NULL, '78.0000', '100.0000', '0000-00-00', 'received', '78.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(706, NULL, NULL, 706, 'TW-RD-0000002', 'CBZ', NULL, '669.0909', '10.0000', 4, '66.9091', 123, '10.0000', NULL, NULL, NULL, '736.0000', '10.0000', '0000-00-00', 'received', '736.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(707, NULL, NULL, 707, 'TW-RD-0000002', 'CBZ', NULL, '669.0909', '10.0000', 4, '66.9091', 123, '10.0000', NULL, NULL, NULL, '736.0000', '10.0000', '0000-00-00', 'received', '736.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(708, NULL, NULL, 708, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '1305.4545', '3.0000', 4, '130.5455', 123, '10.0000', NULL, NULL, NULL, '1436.0000', '3.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(709, NULL, NULL, 709, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '1305.4545', '3.0000', 4, '130.5455', 123, '10.0000', NULL, NULL, NULL, '1436.0000', '3.0000', '0000-00-00', 'received', '1436.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(710, NULL, NULL, 710, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '100.0000', '0.0000', 4, '10.0000', 123, '10.0000', NULL, NULL, NULL, '110.0000', '0.0000', '0000-00-00', 'received', '110.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(711, NULL, NULL, 711, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '100.0000', '0.0000', 4, '10.0000', 123, '10.0000', NULL, NULL, NULL, '110.0000', '0.0000', '0000-00-00', 'received', '110.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(712, NULL, NULL, 712, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '327.2727', '0.0000', 4, '32.7273', 123, '10.0000', NULL, NULL, NULL, '360.0000', '0.0000', '0000-00-00', 'received', '360.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(713, NULL, NULL, 713, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '327.2727', '0.0000', 4, '32.7273', 123, '10.0000', NULL, NULL, NULL, '360.0000', '0.0000', '0000-00-00', 'received', '360.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(714, NULL, NULL, 714, 'TW-0000002', 'Bike accessories', NULL, '1877.2727', '10.0000', 4, '187.7273', 123, '10.0000', NULL, NULL, NULL, '2065.0000', '10.0000', '0000-00-00', 'received', '2065.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(715, NULL, NULL, 715, 'TW-0000002', 'Bike accessories', NULL, '1877.2727', '10.0000', 4, '187.7273', 123, '10.0000', NULL, NULL, NULL, '2065.0000', '10.0000', '0000-00-00', 'received', '2065.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(716, NULL, NULL, 716, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '1170.9091', '0.0000', 4, '117.0909', 123, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(717, NULL, NULL, 717, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '1170.9091', '0.0000', 4, '117.0909', 123, '10.0000', NULL, NULL, NULL, '1288.0000', '0.0000', '0000-00-00', 'received', '1288.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(718, NULL, NULL, 718, 'TW-0000002', 'Bike accessories', NULL, '1937.2727', '0.0000', 4, '193.7273', 123, '10.0000', NULL, NULL, NULL, '2131.0000', '0.0000', '0000-00-00', 'received', '2131.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(719, NULL, NULL, 719, 'TW-0000002', 'Bike accessories', NULL, '1937.2727', '0.0000', 4, '193.7273', 123, '10.0000', NULL, NULL, NULL, '2131.0000', '0.0000', '0000-00-00', 'received', '2131.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(720, NULL, NULL, 720, 'TW-0000002', 'Bike accessories', NULL, '4429.0909', '10.0000', 4, '442.9091', 123, '10.0000', NULL, NULL, NULL, '4872.0000', '10.0000', '0000-00-00', 'received', '4872.0000', NULL, 'LOT#00010', NULL, 'WH0000101', '0016010100010', NULL),
(721, NULL, NULL, 721, 'TW-0000002', 'Bike accessories', NULL, '4429.0909', '10.0000', 4, '442.9091', 123, '10.0000', NULL, NULL, NULL, '4872.0000', '10.0000', '0000-00-00', 'received', '4872.0000', NULL, 'LOT#00010', NULL, 'WH0000101', '0016010100010', NULL),
(722, NULL, NULL, 722, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '545.4545', '0.0000', 4, '54.5455', 123, '10.0000', NULL, NULL, NULL, '600.0000', '0.0000', '0000-00-00', 'received', '600.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(723, NULL, NULL, 723, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '545.4545', '0.0000', 4, '54.5455', 123, '10.0000', NULL, NULL, NULL, '600.0000', '0.0000', '0000-00-00', 'received', '600.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(724, NULL, NULL, 724, 'TW-0000002', 'Bike accessories', NULL, '4807.2727', '10.0000', 4, '480.7273', 123, '10.0000', NULL, NULL, NULL, '5288.0000', '10.0000', '0000-00-00', 'received', '5288.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(725, NULL, NULL, 725, 'TW-0000002', 'Bike accessories', NULL, '4807.2727', '10.0000', 4, '480.7273', 123, '10.0000', NULL, NULL, NULL, '5288.0000', '10.0000', '0000-00-00', 'received', '5288.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(726, NULL, NULL, 726, 'TW-0000002', 'Bike accessories', NULL, '3202.7273', '10.0000', 4, '320.2727', 123, '10.0000', NULL, NULL, NULL, '3523.0000', '10.0000', '0000-00-00', 'received', '3523.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(727, NULL, NULL, 727, 'TW-0000002', 'Bike accessories', NULL, '3202.7273', '10.0000', 4, '320.2727', 123, '10.0000', NULL, NULL, NULL, '3523.0000', '10.0000', '0000-00-00', 'received', '3523.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(728, NULL, NULL, 728, 'CV-RD-0000001', 'Bike', NULL, '13636.3636', '3.0000', 4, '1363.6364', 123, '10.0000', NULL, NULL, NULL, '15000.0000', '3.0000', '0000-00-00', 'received', '15000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(729, NULL, NULL, 729, 'CV-RD-0000001', 'Bike', NULL, '13636.3636', '3.0000', 4, '1363.6364', 123, '10.0000', NULL, NULL, NULL, '15000.0000', '3.0000', '0000-00-00', 'received', '15000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(730, NULL, NULL, 730, 'TW-0000002', 'Bike accessories', NULL, '2293.6364', '10.0000', 4, '229.3636', 123, '10.0000', NULL, NULL, NULL, '2523.0000', '10.0000', '0000-00-00', 'received', '2523.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', NULL),
(731, NULL, NULL, 731, 'TW-0000002', 'Bike accessories', NULL, '2293.6364', '10.0000', 4, '229.3636', 123, '10.0000', NULL, NULL, NULL, '2523.0000', '10.0000', '0000-00-00', 'received', '2523.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', NULL),
(732, NULL, NULL, 732, 'TW-0000006', 'MNF Input 2', NULL, '330.9091', '97.0000', 4, '33.0909', 123, '10.0000', NULL, NULL, NULL, '364.0000', '97.0000', '0000-00-00', 'received', '364.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(733, NULL, NULL, 733, 'TW-0000006', 'MNF Input 2', NULL, '330.9091', '97.0000', 4, '33.0909', 123, '10.0000', NULL, NULL, NULL, '364.0000', '97.0000', '0000-00-00', 'received', '364.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(734, NULL, NULL, 734, 'TW-0000002', 'Bike accessories', NULL, '4090.9091', '2.0000', 4, '409.0909', 123, '10.0000', NULL, NULL, NULL, '4500.0000', '2.0000', '0000-00-00', 'received', '4500.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(735, NULL, NULL, 735, 'TW-0000002', 'Bike accessories', NULL, '4090.9091', '2.0000', 4, '409.0909', 123, '10.0000', NULL, NULL, NULL, '4500.0000', '2.0000', '0000-00-00', 'received', '4500.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(736, NULL, NULL, 736, 'CV-RD-0000001', 'Bike', NULL, '2223.6364', '1.0000', 4, '222.3636', 123, '10.0000', NULL, NULL, NULL, '2446.0000', '1.0000', '0000-00-00', 'received', '2446.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', NULL),
(737, NULL, NULL, 737, 'CV-RD-0000001', 'Bike', NULL, '2223.6364', '1.0000', 4, '222.3636', 123, '10.0000', NULL, NULL, NULL, '2446.0000', '1.0000', '0000-00-00', 'received', '2446.0000', NULL, 'LOT#00008', NULL, 'WH0000101', '0016010100010', NULL),
(738, NULL, NULL, 738, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '219.0909', '0.0000', 4, '21.9091', 123, '10.0000', NULL, NULL, NULL, '241.0000', '0.0000', '0000-00-00', 'received', '241.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(739, NULL, NULL, 739, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '219.0909', '0.0000', 4, '21.9091', 123, '10.0000', NULL, NULL, NULL, '241.0000', '0.0000', '0000-00-00', 'received', '241.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(740, NULL, NULL, 740, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '183.6364', '0.0000', 4, '18.3636', 123, '10.0000', NULL, NULL, NULL, '202.0000', '0.0000', '0000-00-00', 'received', '202.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(741, NULL, NULL, 741, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '183.6364', '0.0000', 4, '18.3636', 123, '10.0000', NULL, NULL, NULL, '202.0000', '0.0000', '0000-00-00', 'received', '202.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(742, NULL, NULL, 742, 'TW-0000009', 'MNF tool', NULL, '430.0000', '10.0000', 4, '43.0000', 123, '10.0000', NULL, NULL, NULL, '473.0000', '10.0000', '0000-00-00', 'received', '473.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(743, NULL, NULL, 743, 'TW-0000009', 'MNF tool', NULL, '430.0000', '10.0000', 4, '43.0000', 123, '10.0000', NULL, NULL, NULL, '473.0000', '10.0000', '0000-00-00', 'received', '473.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(744, NULL, NULL, 744, 'CV-RD-0000001', 'Bike', NULL, '1817.2727', '1.0000', 4, '181.7273', 123, '10.0000', NULL, NULL, NULL, '1999.0000', '1.0000', '0000-00-00', 'received', '1999.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(745, NULL, NULL, 745, 'CV-RD-0000001', 'Bike', NULL, '1817.2727', '1.0000', 4, '181.7273', 123, '10.0000', NULL, NULL, NULL, '1999.0000', '1.0000', '0000-00-00', 'received', '1999.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(746, NULL, NULL, 746, 'CV-RD-0000001', 'Bike', NULL, '2973.6364', '1.0000', 4, '297.3636', 123, '10.0000', NULL, NULL, NULL, '3271.0000', '1.0000', '0000-00-00', 'received', '3271.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(747, NULL, NULL, 747, 'CV-RD-0000001', 'Bike', NULL, '2973.6364', '1.0000', 4, '297.3636', 123, '10.0000', NULL, NULL, NULL, '3271.0000', '1.0000', '0000-00-00', 'received', '3271.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(748, NULL, NULL, 748, 'CV-RD-0000001', 'Bike', NULL, '5396.3636', '1.0000', 4, '539.6364', 123, '10.0000', NULL, NULL, NULL, '5936.0000', '1.0000', '0000-00-00', 'received', '5936.0000', NULL, '1', NULL, 'WH0000101', '0016010100010', NULL),
(749, NULL, NULL, 749, 'CV-RD-0000001', 'Bike', NULL, '5396.3636', '1.0000', 4, '539.6364', 123, '10.0000', NULL, NULL, NULL, '5936.0000', '1.0000', '0000-00-00', 'received', '5936.0000', NULL, '1', NULL, 'WH0000101', '0016010100010', NULL),
(750, NULL, NULL, 750, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '120.9091', '0.0000', 4, '12.0909', 123, '10.0000', NULL, NULL, NULL, '133.0000', '0.0000', '0000-00-00', 'received', '133.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(751, NULL, NULL, 751, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '120.9091', '0.0000', 4, '12.0909', 123, '10.0000', NULL, NULL, NULL, '133.0000', '0.0000', '0000-00-00', 'received', '133.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(752, NULL, NULL, 752, 'CV-RD-0000001', 'Bike', NULL, '636.3636', '10.0000', 4, '63.6364', 123, '10.0000', NULL, NULL, NULL, '700.0000', '10.0000', '0000-00-00', 'received', '700.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(753, NULL, NULL, 753, 'CV-RD-0000001', 'Bike', NULL, '636.3636', '10.0000', 4, '63.6364', 123, '10.0000', NULL, NULL, NULL, '700.0000', '10.0000', '0000-00-00', 'received', '700.0000', NULL, 'LOT#00003', NULL, 'WH0000101', '0016010100010', NULL),
(754, NULL, NULL, 754, 'TW-0000006', 'MNF Input 2', NULL, '2640.0000', '0.0000', 4, '264.0000', 123, '10.0000', NULL, NULL, NULL, '2904.0000', '0.0000', '0000-00-00', 'received', '2904.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(755, NULL, NULL, 755, 'TW-0000006', 'MNF Input 2', NULL, '2640.0000', '0.0000', 4, '264.0000', 123, '10.0000', NULL, NULL, NULL, '2904.0000', '0.0000', '0000-00-00', 'received', '2904.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(756, NULL, NULL, 756, 'CV-RD-0000001', 'Bike', NULL, '7057.2727', '2.0000', 4, '705.7273', 123, '10.0000', NULL, NULL, NULL, '7763.0000', '2.0000', '0000-00-00', 'received', '7763.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(757, NULL, NULL, 757, 'CV-RD-0000001', 'Bike', NULL, '7057.2727', '2.0000', 4, '705.7273', 123, '10.0000', NULL, NULL, NULL, '7763.0000', '2.0000', '0000-00-00', 'received', '7763.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(758, NULL, NULL, 758, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '27272.7273', '160046.0000', 4, '2727.2727', 123, '10.0000', NULL, NULL, NULL, '30000.0000', '160046.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(759, NULL, NULL, 759, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '27272.7273', '160046.0000', 4, '2727.2727', 123, '10.0000', NULL, NULL, NULL, '30000.0000', '160046.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00007', NULL, 'WH0000101', '0016010100010', NULL),
(760, NULL, NULL, 760, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '163.6364', '0.0000', 4, '16.3636', 123, '10.0000', NULL, NULL, NULL, '180.0000', '0.0000', '0000-00-00', 'received', '180.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(761, NULL, NULL, 761, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '163.6364', '0.0000', 4, '16.3636', 123, '10.0000', NULL, NULL, NULL, '180.0000', '0.0000', '0000-00-00', 'received', '180.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(762, NULL, NULL, 762, 'CV-RD-0000001', 'Bike', NULL, '90.0000', '1.0000', 4, '9.0000', 123, '10.0000', NULL, NULL, NULL, '99.0000', '1.0000', '0000-00-00', 'received', '99.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(763, NULL, NULL, 763, 'CV-RD-0000001', 'Bike', NULL, '90.0000', '1.0000', 4, '9.0000', 123, '10.0000', NULL, NULL, NULL, '99.0000', '1.0000', '0000-00-00', 'received', '99.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(764, NULL, NULL, 764, 'TW-0000002', 'Bike accessories', NULL, '301.8182', '10.0000', 4, '30.1818', 123, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(765, NULL, NULL, 765, 'TW-0000002', 'Bike accessories', NULL, '301.8182', '10.0000', 4, '30.1818', 123, '10.0000', NULL, NULL, NULL, '332.0000', '10.0000', '0000-00-00', 'received', '332.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(766, NULL, NULL, 766, 'TW-0000002', 'Bike accessories', NULL, '6819.0909', '0.0000', 4, '681.9091', 123, '10.0000', NULL, NULL, NULL, '7501.0000', '0.0000', '0000-00-00', 'received', '7501.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(767, NULL, NULL, 767, 'TW-0000002', 'Bike accessories', NULL, '6819.0909', '0.0000', 4, '681.9091', 123, '10.0000', NULL, NULL, NULL, '7501.0000', '0.0000', '0000-00-00', 'received', '7501.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(768, NULL, NULL, 768, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '1818.1818', '2.0000', 4, '181.8182', 123, '10.0000', NULL, NULL, NULL, '2000.0000', '2.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(769, NULL, NULL, 769, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '1818.1818', '2.0000', 4, '181.8182', 123, '10.0000', NULL, NULL, NULL, '2000.0000', '2.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(770, NULL, NULL, 770, 'TW-0000002', 'Bike accessories', NULL, '4610.9091', '10.0000', 4, '461.0909', 123, '10.0000', NULL, NULL, NULL, '5072.0000', '10.0000', '0000-00-00', 'received', '5072.0000', NULL, 'LOT#00009', NULL, 'WH0000101', '0016010100010', NULL),
(771, NULL, NULL, 771, 'TW-0000002', 'Bike accessories', NULL, '4610.9091', '10.0000', 4, '461.0909', 123, '10.0000', NULL, NULL, NULL, '5072.0000', '10.0000', '0000-00-00', 'received', '5072.0000', NULL, 'LOT#00009', NULL, 'WH0000101', '0016010100010', NULL),
(772, NULL, NULL, 772, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '27272.7273', '30042.0000', 4, '2727.2727', 123, '10.0000', NULL, NULL, NULL, '30000.0000', '30042.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(773, NULL, NULL, 773, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '27272.7273', '30042.0000', 4, '2727.2727', 123, '10.0000', NULL, NULL, NULL, '30000.0000', '30042.0000', '0000-00-00', 'received', '30000.0000', NULL, 'LOT#00006', NULL, 'WH0000101', '0016010100010', NULL),
(774, NULL, NULL, 774, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '233.6364', '0.0000', 4, '23.3636', 123, '10.0000', NULL, NULL, NULL, '257.0000', '0.0000', '0000-00-00', 'received', '257.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(775, NULL, NULL, 775, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '233.6364', '0.0000', 4, '23.3636', 123, '10.0000', NULL, NULL, NULL, '257.0000', '0.0000', '0000-00-00', 'received', '257.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(776, NULL, NULL, 776, 'CV-RD-0000001', 'Bike', NULL, '1817.2727', '1.0000', 4, '181.7273', 123, '10.0000', NULL, NULL, NULL, '1999.0000', '1.0000', '0000-00-00', 'received', '1999.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(777, NULL, NULL, 777, 'CV-RD-0000001', 'Bike', NULL, '1817.2727', '1.0000', 4, '181.7273', 123, '10.0000', NULL, NULL, NULL, '1999.0000', '1.0000', '0000-00-00', 'received', '1999.0000', NULL, 'LOT#00004', NULL, 'WH0000101', '0016010100010', NULL),
(778, NULL, NULL, 778, 'TW-0000001', 'WIP Bike', NULL, '14545.4545', '1.0000', 4, '1454.5455', 123, '10.0000', NULL, NULL, NULL, '16000.0000', '1.0000', '0000-00-00', 'received', '16000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(779, NULL, NULL, 779, 'TW-0000001', 'WIP Bike', NULL, '14545.4545', '1.0000', 4, '1454.5455', 123, '10.0000', NULL, NULL, NULL, '16000.0000', '1.0000', '0000-00-00', 'received', '16000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(780, NULL, NULL, 780, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '374.5455', '0.0000', 4, '37.4545', 123, '10.0000', NULL, NULL, NULL, '412.0000', '0.0000', '0000-00-00', 'received', '412.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(781, NULL, NULL, 781, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '374.5455', '0.0000', 4, '37.4545', 123, '10.0000', NULL, NULL, NULL, '412.0000', '0.0000', '0000-00-00', 'received', '412.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(782, NULL, NULL, 782, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '583.6364', '0.0000', 4, '58.3636', 123, '10.0000', NULL, NULL, NULL, '642.0000', '0.0000', '0000-00-00', 'received', '642.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(783, NULL, NULL, 783, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '583.6364', '0.0000', 4, '58.3636', 123, '10.0000', NULL, NULL, NULL, '642.0000', '0.0000', '0000-00-00', 'received', '642.0000', NULL, 'LOT#00005', NULL, 'WH0000101', '0016010100010', NULL),
(784, NULL, NULL, 784, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '1549.0909', '0.0000', 4, '154.9091', 123, '10.0000', NULL, NULL, NULL, '1704.0000', '0.0000', '0000-00-00', 'received', '1704.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(785, NULL, NULL, 785, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '1549.0909', '0.0000', 4, '154.9091', 123, '10.0000', NULL, NULL, NULL, '1704.0000', '0.0000', '0000-00-00', 'received', '1704.0000', NULL, 'LOT#00002', NULL, 'WH0000101', '0016010100010', NULL),
(786, NULL, NULL, 786, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '1818.1818', '0.0000', 4, '181.8182', 123, '10.0000', NULL, NULL, NULL, '2000.0000', '0.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(787, NULL, NULL, 787, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '1818.1818', '0.0000', 4, '181.8182', 123, '10.0000', NULL, NULL, NULL, '2000.0000', '0.0000', '0000-00-00', 'received', '2000.0000', NULL, 'LOT#00001', NULL, 'WH0000101', '0016010100010', NULL),
(788, NULL, NULL, 788, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(789, NULL, NULL, 789, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(790, NULL, NULL, 790, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(791, NULL, NULL, 791, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(792, NULL, NULL, 792, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(793, NULL, NULL, 793, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(794, NULL, NULL, 794, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(795, NULL, NULL, 795, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(796, NULL, NULL, 796, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(797, NULL, NULL, 797, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 4, NULL, 123, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000101', '0016010100010', NULL),
(798, NULL, NULL, 798, 'TW-0000002', 'Bike accessories', NULL, '2298.1818', '10.0000', 5, '229.8182', 124, '10.0000', NULL, NULL, NULL, '2528.0000', '10.0000', '0000-00-00', 'received', '2528.0000', NULL, 'LOT#00001', NULL, 'WH0000501', '0016010100010', NULL),
(799, NULL, NULL, 799, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(800, NULL, NULL, 800, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(801, NULL, NULL, 801, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(802, NULL, NULL, 802, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(803, NULL, NULL, 803, 'TW-0000009', 'MNF tool', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(804, NULL, NULL, 804, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(805, NULL, NULL, 805, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(806, NULL, NULL, 806, 'TW-0000006', 'MNF Input 2', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(807, NULL, NULL, 807, 'TW-RD-0000002', 'CBZ', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(808, NULL, NULL, 808, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(809, NULL, NULL, 809, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(810, NULL, NULL, 810, 'TW-0000001', 'WIP Bike', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(811, NULL, NULL, 811, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(812, NULL, NULL, 812, 'CV-RD-0000001', 'Bike', NULL, '0.0000', '0.0000', 5, NULL, 124, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000501', '0016010100010', NULL),
(813, NULL, NULL, 813, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '25.4545', '0.0000', 6, '2.5455', 125, '10.0000', NULL, NULL, NULL, '28.0000', '0.0000', '0000-00-00', 'received', '28.0000', NULL, 'LOT#00002', NULL, 'WH0000401', '0016010100010', NULL),
(814, NULL, NULL, 814, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '25.4545', '0.0000', 6, '2.5455', 125, '10.0000', NULL, NULL, NULL, '28.0000', '0.0000', '0000-00-00', 'received', '28.0000', NULL, 'LOT#00001', NULL, 'WH0000401', '0016010100010', NULL),
(815, NULL, NULL, 815, 'TW-0000002', 'Bike accessories', NULL, '2298.1818', '10.0000', 6, '229.8182', 125, '10.0000', NULL, NULL, NULL, '2528.0000', '10.0000', '0000-00-00', 'received', '2528.0000', NULL, 'LOT#00001', NULL, 'WH0000401', '0016010100010', NULL),
(816, NULL, NULL, 816, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(817, NULL, NULL, 817, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(818, NULL, NULL, 818, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(819, NULL, NULL, 819, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(820, NULL, NULL, 820, 'TW-0000009', 'MNF tool', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(821, NULL, NULL, 821, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(822, NULL, NULL, 822, 'TW-0000006', 'MNF Input 2', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(823, NULL, NULL, 823, 'TW-RD-0000002', 'CBZ', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(824, NULL, NULL, 824, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(825, NULL, NULL, 825, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(826, NULL, NULL, 826, 'TW-0000001', 'WIP Bike', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(827, NULL, NULL, 827, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(828, NULL, NULL, 828, 'CV-RD-0000001', 'Bike', NULL, '0.0000', '0.0000', 6, NULL, 125, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000401', '0016010100010', NULL),
(829, NULL, NULL, 829, 'TW-0000002', 'Bike accessories', NULL, '2298.1818', '10.0000', 7, '229.8182', 128, '10.0000', NULL, NULL, NULL, '2528.0000', '10.0000', '0000-00-00', 'received', '2528.0000', NULL, 'LOT#00001', NULL, 'WH0000301', '0016010100010', NULL),
(830, NULL, NULL, 830, 'TW-0000002', 'Bike accessories', NULL, '2310.0000', '10.0000', 7, '231.0000', 128, '10.0000', NULL, NULL, NULL, '2541.0000', '10.0000', '0000-00-00', 'received', '2541.0000', NULL, 'LOT#00002', NULL, 'WH0000301', '0016010100010', NULL),
(831, NULL, NULL, 831, 'TW-RD-0000001', 'Bajaj Chetak', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(832, NULL, NULL, 832, 'TW-0000008', 'MNF Output', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL);
INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `grp_id`, `psale`) VALUES
(833, NULL, NULL, 833, 'CV.TW..0000002', 'Bike new', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(834, NULL, NULL, 834, 'CV.TW..0000001', 'Bajaj Pulser 220 cc', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(835, NULL, NULL, 835, 'TW-0000009', 'MNF tool', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(836, NULL, NULL, 836, 'TW-0000005', 'Super Splendor Front Brake Nut', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(837, NULL, NULL, 837, 'RM..0000004', '1LTR PESTCIDE BOTTLE WHITE', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(838, NULL, NULL, 838, 'TW-0000006', 'MNF Input 2', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(839, NULL, NULL, 839, 'TW-RD-0000002', 'CBZ', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(840, NULL, NULL, 840, 'TW-0000007', 'MNF WIP', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(841, NULL, NULL, 841, 'TW-0000004', 'Rubber Indicator Stays', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(842, NULL, NULL, 842, 'TW-0000001', 'WIP Bike', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(843, NULL, NULL, 843, 'TW-0000003', 'Honda Activa Bend Pipe', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(844, NULL, NULL, 844, 'CV-RD-0000001', 'Bike', NULL, '0.0000', '0.0000', 7, NULL, 128, '10.0000', NULL, NULL, NULL, '0.0000', NULL, '0000-00-00', 'received', NULL, NULL, NULL, NULL, 'WH0000301', '0016010100010', NULL),
(1024, NULL, NULL, 504, 'BL-0000001', 'Swing machin', NULL, '0.0000', '0.0000', 1, '0.0000', 0, '', NULL, NULL, NULL, '0.0000', '49.0000', '2016-07-14', 'received', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_return_items`
--

INSERT INTO `sma_return_items` (`id`, `sale_id`, `return_id`, `sale_item_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`) VALUES
(1, 0, 1, 9, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '-15.4500', '1.0000', 3, '0.4500', 42, '10.0000%', '20', '20.0000', '-15.0000', '', '25.0000'),
(2, 0, 2, 10, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '13.1800', '1.0000', 3, '1.8200', 42, '10.0000%', '5', '5.0000', '15.0000', '', '25.0000'),
(3, 0, 3, 11, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '13.1800', '1.0000', 3, '1.8200', 42, '10.0000%', '5', '5.0000', '15.0000', '', '25.0000'),
(4, 0, 4, 12, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '22.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '0', '0.0000', '25.0000', '', '25.0000'),
(5, 0, 5, 13, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '22.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '0', '0.0000', '25.0000', '', '25.0000'),
(6, 0, 6, 14, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '9.3600', '1.0000', 3, '1.6400', 42, '10.0000%', '7', '7.0000', '11.0000', '', '25.0000'),
(7, 0, 7, 15, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '3.6400', '1.0000', 3, '1.3600', 42, '10.0000%', '10', '10.0000', '5.0000', '', '25.0000'),
(8, 0, 8, 16, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '17.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '5', '5.0000', '20.0000', '', '25.0000'),
(9, 0, 9, 17, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '7.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '15', '15.0000', '10.0000', '', '25.0000'),
(10, 0, 10, 18, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '17.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '5', '5.0000', '20.0000', '', '25.0000'),
(11, 0, 11, 19, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '15.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '7', '7.0000', '18.0000', '', '25.0000'),
(12, 0, 12, 20, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '12.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '10', '10.0000', '15.0000', '', '25.0000'),
(13, 0, 13, 21, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '17.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '5', '5.0000', '20.0000', '', '25.0000'),
(14, 0, 14, 22, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '15.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '7', '7.0000', '18.0000', '', '25.0000'),
(15, 0, 15, 25, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '12.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '10', '10.0000', '15.0000', '', '25.0000'),
(16, 0, 15, 26, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '15.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '7', '7.0000', '18.0000', '', '25.0000'),
(17, 0, 15, 27, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '17.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '5', '5.0000', '20.0000', '', '25.0000'),
(18, 0, 16, 28, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '15.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '7', '7.0000', '18.0000', '', '25.0000'),
(19, 0, 16, 29, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '16.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '6', '6.0000', '19.0000', '', '25.0000'),
(20, 0, 16, 30, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '17.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '5', '5.0000', '20.0000', '', '25.0000'),
(21, 0, 17, 31, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '22.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '0', '0.0000', '25.0000', '', '25.0000'),
(22, 0, 17, 32, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '22.7300', '1.0000', 3, '2.2700', 42, '10.0000%', '0', '0.0000', '25.0000', '', '25.0000'),
(23, 0, 18, 49, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000'),
(24, 0, 19, 48, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_sales`
--

CREATE TABLE `sma_return_sales` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_return_sales`
--

INSERT INTO `sma_return_sales` (`id`, `sale_id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `surcharge`, `grand_total`, `created_by`, `updated_by`, `updated_at`, `attachment`) VALUES
(1, 8, '2016-07-15 11:08:11', 'RETURNSL/2016/07/0001', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '-15.4500', '20.0000', NULL, '20.0000', '0.0000', '0.4500', 1, '-1.5000', '-1.0500', '0.0000', '-16.5000', 18, NULL, NULL, NULL),
(2, 9, '2016-07-15 11:39:26', 'RETURNSL/2016/07/0002', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '&lt;p&gt;hello dear&lt;&sol;p&gt;', '13.1800', '5.0000', NULL, '5.0000', '0.0000', '1.8200', 1, '1.5000', '3.3200', '0.0000', '16.5000', 18, NULL, NULL, NULL),
(3, 10, '2016-07-15 11:54:12', 'RETURNSL/2016/07/0003', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '13.1800', '5.0000', NULL, '5.0000', '0.0000', '1.8200', 1, '1.5000', '3.3200', '0.0000', '16.5000', 18, NULL, NULL, NULL),
(4, 11, '2016-07-15 12:06:23', 'RETURNSL/2016/07/0004', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '22.7300', '0.0000', NULL, '0.0000', '0.0000', '2.2700', 1, '2.5000', '4.7700', '0.0000', '27.5000', 18, NULL, NULL, NULL),
(5, 12, '2016-07-15 12:22:26', 'RETURNSL/2016/07/0005', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '22.7300', '0.0000', NULL, '0.0000', '0.0000', '2.2700', 1, '2.5000', '4.7700', '0.0000', '27.5000', 18, NULL, NULL, NULL),
(6, 13, '2016-07-15 12:40:10', 'RETURNSL/2016/07/0006', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '9.3600', '7.0000', NULL, '7.0000', '0.0000', '1.6400', 1, '1.1000', '2.7400', '0.0000', '12.1000', 18, NULL, NULL, NULL),
(7, 14, '2016-07-15 12:57:49', 'RETURNSL/2016/07/0007', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '3.6400', '10.0000', NULL, '10.0000', '0.0000', '1.3600', 63, '0.5000', '1.8600', '0.0000', '5.5000', 18, NULL, NULL, NULL),
(8, 15, '2016-07-15 13:06:31', 'RETURNSL/2016/07/0008', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '17.7300', '5.0000', NULL, '5.0000', '0.0000', '2.2700', 1, '2.0000', '4.2700', '0.0000', '22.0000', 18, NULL, NULL, NULL),
(9, 16, '2016-07-15 13:09:15', 'RETURNSL/2016/07/0009', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '7.7300', '15.0000', NULL, '15.0000', '0.0000', '2.2700', 1, '1.0000', '3.2700', '0.0000', '11.0000', 18, NULL, NULL, NULL),
(10, 17, '2016-07-15 13:16:42', 'RETURNSL/2016/07/0010', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '17.7300', '5.0000', NULL, '5.0000', '0.0000', '2.2700', 1, '2.0000', '4.2700', '0.0000', '22.0000', 18, NULL, NULL, NULL),
(11, 18, '2016-07-15 13:22:13', 'RETURNSL/2016/07/0011', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '15.7300', '7.0000', NULL, '7.0000', '0.0000', '2.2700', 1, '1.8000', '4.0700', '0.0000', '19.8000', 18, NULL, NULL, NULL),
(12, 19, '2016-07-15 13:30:33', 'RETURNSL/2016/07/0012', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '12.7300', '10.0000', NULL, '10.0000', '0.0000', '2.2700', NULL, '0.0000', '2.2700', '0.0000', '15.0000', 18, NULL, NULL, NULL),
(13, 20, '2016-07-15 13:36:41', 'RETURNSL/2016/07/0013', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '17.7300', '5.0000', NULL, '5.0000', '0.0000', '2.2700', 129, '0.0000', '2.2700', '0.0000', '20.0000', 18, NULL, NULL, NULL),
(14, 21, '2016-07-15 14:03:16', 'RETURNSL/2016/07/0014', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '15.7300', '7.0000', NULL, '7.0000', '0.0000', '2.2700', 3, '1.8000', '4.0700', '0.0000', '19.8000', 18, NULL, NULL, NULL),
(15, 23, '2016-07-16 05:20:00', 'RETURNSL/2016/07/0015', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '46.1900', '22.0000', NULL, '22.0000', '0.0000', '6.8100', 3, '5.3000', '12.1100', '0.0000', '58.3000', 1, NULL, NULL, NULL),
(16, 24, '2016-07-16 05:24:00', 'RETURNSL/2016/07/0016', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '50.1900', '18.0000', NULL, '18.0000', '0.0000', '6.8100', 3, '5.7000', '12.5100', '0.0000', '62.7000', 1, NULL, NULL, NULL),
(17, 25, '2016-07-16 05:28:00', 'RETURNSL/2016/07/0017', 20, 'ESS Group', 6, ' ACME TELECOM', 3, '', '45.4600', '0.0000', NULL, '0.0000', '0.0000', '4.5400', 3, '5.0000', '9.5400', '0.0000', '55.0000', 1, NULL, NULL, NULL),
(18, 41, '2016-07-20 05:21:51', 'RETURNSL/2016/07/0018', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 23, NULL, NULL, NULL),
(19, 40, '2016-07-20 09:54:16', 'RETURNSL/2016/07/0019', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '&lt;p&gt;hello world&lt;&sol;p&gt;', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 23, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` tinyint(4) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sales`
--

INSERT INTO `sma_sales` (`id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `staff_note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `sale_status`, `payment_status`, `payment_term`, `due_date`, `created_by`, `updated_by`, `updated_at`, `total_items`, `pos`, `paid`, `return_id`, `surcharge`, `attachment`, `sync_flg`) VALUES
(1, '2016-01-06 05:41:41', 'SALE/POS/2016/01/0001', 1, 'Walk-in Customer', 3, 'Test Biller', 1, 'sdcd', 'test', '909.0900', '0.0000', '15', '15.0000', '15.0000', '90.9100', 2, '98.5000', '189.4100', '0.0000', '1083.5000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1083.5000', NULL, '0.0000', NULL, NULL),
(2, '2016-01-28 08:08:11', 'SALE/POS/2016/01/0002', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'partial', 0, NULL, 1, NULL, NULL, 1, 1, '100.0000', NULL, '0.0000', NULL, NULL),
(3, '2016-07-13 13:07:55', 'SALE/POS/2016/07/0003', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '77272.7300', '0.0000', '10', '10.0000', '10.0000', '7727.2700', 1, '0.0000', '7727.2700', '0.0000', '84990.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '84990.0000', NULL, '0.0000', NULL, NULL),
(4, '2016-07-13 13:20:49', 'SALE/POS/2016/07/0004', 4, 'ess', 3, 'Test Biller', 1, '', '', '45454.5500', '0.0000', NULL, '0.0000', '0.0000', '4545.4500', 1, '0.0000', '4545.4500', '0.0000', '50000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '50000.0000', NULL, '0.0000', NULL, NULL),
(5, '2016-07-13 13:30:45', 'SALE/POS/2016/07/0005', 5, 'ess', 3, 'Test Biller', 1, '', '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, NULL),
(6, '2016-07-14 16:18:23', 'SALE/POS/2016/07/0006', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 1, '', '', '332.0000', '0.0000', '10', '10.0000', '10.0000', '0.0000', 7, '0.0000', '0.0000', '0.0000', '322.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '322.0000', NULL, '0.0000', NULL, NULL),
(7, '2016-07-15 11:51:42', 'SALE/POS/2016/07/0007', 15, 'ESS Group', 6, ' ACME TELECOM', 3, '', '', '25.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '25.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '27.5000', NULL, '0.0000', NULL, NULL),
(22, '2016-07-15 14:09:38', 'SALE/POS/2016/07/0022', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '', '32.7300', '16.0000', NULL, '16.0000', '0.0000', '3.2700', 3, '3.6000', '6.8700', '0.0000', '39.6000', 'completed', 'paid', 0, NULL, 18, NULL, NULL, 2, 1, '39.6000', NULL, '0.0000', NULL, NULL),
(26, '2016-07-16 09:12:45', 'SALE/POS/2016/07/0026', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 3, '', '', '36.3700', '10.0000', NULL, '10.0000', '0.0000', '3.6300', 3, '4.0000', '7.6300', '0.0000', '44.0000', 'completed', 'paid', 0, NULL, 18, NULL, NULL, 2, 1, '44.0000', NULL, '0.0000', NULL, NULL),
(27, '2016-07-19 10:18:18', 'SALE/POS/2016/07/0027', 47, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(28, '2016-07-19 10:58:30', 'SALE/POS/2016/07/0028', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(29, '2016-07-19 11:44:56', 'SALE/POS/2016/07/0029', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(30, '2016-07-19 11:51:55', 'SALE/POS/2016/07/0030', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(31, '2016-07-19 12:01:35', 'SALE/POS/2016/07/0031', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(32, '2016-07-19 12:02:56', 'SALE/POS/2016/07/0032', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(33, '2016-07-19 12:27:32', 'SALE/POS/2016/07/0033', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(34, '2016-07-19 12:29:55', 'SALE/POS/2016/07/0034', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(35, '2016-07-19 12:36:25', 'SALE/POS/2016/07/0035', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(36, '2016-07-19 12:40:42', 'SALE/POS/2016/07/0036', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'due', 0, NULL, 23, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, NULL),
(37, '2016-07-19 12:41:53', 'SALE/POS/2016/07/0037', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'due', 0, NULL, 23, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, NULL),
(38, '2016-07-19 12:42:42', 'SALE/POS/2016/07/0038', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'due', 0, NULL, 23, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, NULL),
(39, '2016-07-19 12:43:05', 'SALE/POS/2016/07/0039', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'due', 0, NULL, 23, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, NULL),
(42, '2016-07-20 10:05:26', 'SALE/POS/2016/07/0042', 17, 'ESS Group', 6, ' ACME TELECOM', 4, '', '', '301.8200', '0.0000', NULL, '0.0000', '0.0000', '30.1800', 3, '33.2000', '63.3800', '0.0000', '365.2000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 1, 1, '365.2000', NULL, '0.0000', NULL, NULL),
(43, '2016-07-20 10:30:48', 'SALE/POS/2016/07/0043', 1, 'Walk-in Customer', 6, ' ACME TELECOM', 4, '', '', '10600.0000', '0.0000', NULL, '0.0000', '0.0000', '1060.0000', 3, '1166.0000', '2226.0000', '0.0000', '12826.0000', 'completed', 'paid', 0, NULL, 23, NULL, NULL, 14, 1, '12826.0000', NULL, '0.0000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sale_items`
--

INSERT INTO `sma_sale_items` (`id`, `sale_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`, `sync_flg`) VALUES
(1, 1, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', NULL),
(2, 2, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', NULL),
(3, 3, 3, '1223', 'iphone 5', 'standard', 0, '31818.1800', '35000.0000', '1.0000', 1, '3181.8200', 2, '10.0000%', '0', '0.0000', '35000.0000', '', '35000.0000', NULL),
(4, 3, 2, '1213', 'iphone 6s', 'standard', 0, '45454.5500', '50000.0000', '1.0000', 1, '4545.4500', 2, '10.0000%', '0', '0.0000', '50000.0000', '', '50000.0000', NULL),
(5, 4, 2, '1213', 'iphone 6s', 'standard', 0, '45454.5500', '50000.0000', '1.0000', 1, '4545.4500', 2, '10.0000%', '0', '0.0000', '50000.0000', '', '50000.0000', NULL),
(6, 5, 2, '1213', 'iphone 6s', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', NULL),
(7, 6, 504, 'BL-0000001', 'Swing machin', 'standard', NULL, '332.0000', '332.0000', '1.0000', 1, '0.0000', 83, '', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(8, 7, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '25.0000', '25.0000', '1.0000', 3, '0.0000', 42, '', '0', '0.0000', '25.0000', '', '25.0000', NULL),
(23, 22, 45, 'CC-RD-0000001', 'Cotton T- Shirt', 'standard', 0, '14.5500', '16.0000', '1.0000', 3, '1.4500', 10, '10.0000%', '10', '10.0000', '16.0000', '', '26.0000', NULL),
(24, 22, 45, 'CC-RD-0000001', 'Cotton T- Shirt', 'standard', 0, '18.1800', '20.0000', '1.0000', 3, '1.8200', 10, '10.0000%', '6', '6.0000', '20.0000', '', '26.0000', NULL),
(33, 26, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '13.6400', '15.0000', '1.0000', 3, '1.3600', 42, '10.0000%', '10', '10.0000', '15.0000', '', '25.0000', NULL),
(34, 26, 170, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '22.7300', '25.0000', '1.0000', 3, '2.2700', 42, '10.0000%', '0', '0.0000', '25.0000', '', '25.0000', NULL),
(35, 27, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(36, 28, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(37, 29, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(38, 30, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(39, 31, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(40, 32, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(41, 33, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(42, 34, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(43, 35, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(44, 36, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(45, 37, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(46, 38, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(47, 39, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(50, 42, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(51, 43, 177, 'CIG-BLK-0000001', 'Computer', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 4, '181.8200', 43, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', NULL),
(52, 43, 177, 'CIG-BLK-0000001', 'Computer', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 4, '181.8200', 43, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', NULL),
(53, 43, 177, 'CIG-BLK-0000001', 'Computer', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 4, '181.8200', 43, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', NULL),
(54, 43, 177, 'CIG-BLK-0000001', 'Computer', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 4, '181.8200', 43, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', NULL),
(55, 43, 177, 'CIG-BLK-0000001', 'Computer', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 4, '181.8200', 43, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', NULL),
(56, 43, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(57, 43, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(58, 43, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(59, 43, 174, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(60, 43, 175, 'CIG-L-0000001', 'CONTRACT PANIC BOLT C/W VERTICAL RODS', 'standard', 0, '301.8200', '332.0000', '1.0000', 4, '30.1800', 43, '10.0000%', '0', '0.0000', '332.0000', '', '332.0000', NULL),
(61, 43, 185, 'CIG-BLK-0000002', 'Motorcycle', 'standard', 0, '0.0000', '0.0000', '1.0000', 4, '0.0000', 43, '10.0000%', '0', '0.0000', '0.0000', '', '0.0000', NULL),
(62, 43, 185, 'CIG-BLK-0000002', 'Motorcycle', 'standard', 0, '0.0000', '0.0000', '1.0000', 4, '0.0000', 43, '10.0000%', '0', '0.0000', '0.0000', '', '0.0000', NULL),
(63, 43, 185, 'CIG-BLK-0000002', 'Motorcycle', 'standard', 0, '0.0000', '0.0000', '1.0000', 4, '0.0000', 43, '10.0000%', '0', '0.0000', '0.0000', '', '0.0000', NULL),
(64, 43, 185, 'CIG-BLK-0000002', 'Motorcycle', 'standard', 0, '0.0000', '0.0000', '1.0000', 4, '0.0000', 43, '10.0000%', '0', '0.0000', '0.0000', '', '0.0000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sessions`
--

INSERT INTO `sma_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('03ec8e70c374efa023be0309c01dca71d0a2b18c', '::1', 1469004731, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030343632313b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030343733313b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('2824eaa02e7c4f474cc266f55f6e285464d19920', '::1', 1469004524, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030343234333b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436383939393732393b),
('2c86f45b7932622f9cda3c4872f099d701b41602', '::1', 1469011898, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031313839383b7265717565737465645f706167657c733a333a22706f73223b),
('49630c0e511372e9c7edc3e0a44300f85e60fb68', '::1', 1469005892, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030353539303b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('530965286ade7fb624f3c197f4a331a04bce0bad', '::1', 1469006295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030363030393b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('53a7c509e216f4c1c0093d685bc2c7bf43dada22', '::1', 1469010867, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031303539373b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393938323137223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393031303836373b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('691df2351f407db6a91cd107e325ea103a7800cf', '::1', 1469009118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030383736333b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030393130333b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('706258e699f25e45df894416e2fcb45c9a4b3838', '::1', 1469007898, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030373736333b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('73f37cb18f70411baed783ec37eda502b7941c08', '::1', 1469008420, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030383134363b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('88a73c09a42bb8e7f7711efee7fe496e33846cf0', '::1', 1469006888, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030363639363b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('8ac5550b83b56d679c1a9c01536c22d958952f6d', '::1', 1469010461, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031303136313b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393031303436313b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('b2c108f0fe75a295302e7ac7f046fcd70f19a0e5', '::1', 1469006568, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030363332353b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353735303b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('baf350eb94759df7c2a3ecbb28dc341ed270d878', '::1', 1469011547, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031313533343b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393938323137223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393031313534373b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('be9b02c6d38615c1056c5b4fdf10411d94d62485', '::1', 1469009362, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030393132363b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030393336323b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('c194ca00b43805e42ad25b2d809ff30b75ce6de7', '::1', 1469008741, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030383435363b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030383732383b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('c39ad27d41e7440842f78fb1f621ce1fb38bb06f', '::1', 1469005160, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393030353036333b7265717565737465645f706167657c733a32303a2273616c65732f72657475726e5f73616c652f3430223b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393931343335223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393030353038323b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('c7d58793378cca0b885e6510f52d8575a58720b2', '::1', 1469011223, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031303932393b6964656e746974797c733a31303a2253555045525649534f52223b757365726e616d657c733a31303a2253555045525649534f52223b656d61696c7c733a32353a2273757065727669736f7240657373696e6469612e636f2e696e223b757365725f69647c733a323a223233223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343638393938323137223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2234223b62696c6c65725f69647c733a313a2236223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313436393031313232333b72656769737465725f69647c733a313a2236223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d31392031303a32393a3237223b),
('eb06cd97b11eefdc4f75b60a5ad853fe1f26897f', '::1', 1469011899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031313839393b),
('ee73696fa006f82cc2013ee8c448ea2f183e5e47', '::1', 1469012027, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436393031323032363b);

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_settings`
--

INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`) VALUES
(1, 'logo2.png', 'images.png', 'ESS INDIA', 'english', 3, 2, 'USD', 1, 10, '1.0', 3, 5, 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'RETURNSL', '', 0, 'default', 1, 1, 1, 1, 1, 1, 1, 1, 0, 'Asia/Kolkata', 800, 800, 60, 60, 0, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@tecdiary.com', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', NULL, NULL, 1, 'contact@tecdiary.com', 0, 4, 1, 0, 2, 1, 1, 0, 2, 2, '.', ',', 0, 8, 'luhartripathi', 'f8a1a55d-0d35-475d-a88c-a61dac988052', 0, NULL, NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_skrill`
--

INSERT INTO `sma_skrill` (`id`, `active`, `account_email`, `secret_word`, `skrill_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_subcategories`
--

CREATE TABLE `sma_subcategories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_subcategories`
--

INSERT INTO `sma_subcategories` (`id`, `category_id`, `code`, `name`, `image`) VALUES
(1, 1, 'mob', 'Mobiles', '9ea31650ccbf76f3356f772c68d3f755.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_tax_rates`
--

INSERT INTO `sma_tax_rates` (`id`, `name`, `code`, `rate`, `type`, `org_id`, `grp_id`) VALUES
(1, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0041010100013'),
(2, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0041010100013'),
(3, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0041010100013'),
(4, '10% TAX', 'tx234', '10.0000', '1', 'WH0000501', '0041010100013'),
(5, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0041010100013'),
(6, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0041010100013'),
(7, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0041010100013'),
(8, '10% TAX', 'tx123', '10.0000', '1', 'WH0000301', '0041010100013'),
(9, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0008010100005'),
(10, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0008010100005'),
(11, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0008010100005'),
(12, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0008010100005'),
(13, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0008010100005'),
(14, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0008010100005'),
(15, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0008010100005'),
(16, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0008010100005'),
(17, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0009010100006'),
(18, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0009010100006'),
(19, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0009010100006'),
(20, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0009010100006'),
(21, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0009010100006'),
(22, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0009010100006'),
(23, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0009010100006'),
(24, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0009010100006'),
(25, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0102010100016'),
(26, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0102010100016'),
(27, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0102010100016'),
(28, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0102010100016'),
(29, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0102010100016'),
(30, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0102010100016'),
(31, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0102010100016'),
(32, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0102010100016'),
(33, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0024010100011'),
(34, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0024010100011'),
(35, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0024010100011'),
(36, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0024010100011'),
(37, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0024010100011'),
(38, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0024010100011'),
(39, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0024010100011'),
(40, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0024010100011'),
(41, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0005010100002'),
(42, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0005010100002'),
(43, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0005010100002'),
(44, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0005010100002'),
(45, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0005010100002'),
(46, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0005010100002'),
(47, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0005010100002'),
(48, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0005010100002'),
(49, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0011010100008'),
(50, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0011010100008'),
(51, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0011010100008'),
(52, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0011010100008'),
(53, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0011010100008'),
(54, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0011010100008'),
(55, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0011010100008'),
(56, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0011010100008'),
(57, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0010010100007'),
(58, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0010010100007'),
(59, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0010010100007'),
(60, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0010010100007'),
(61, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0010010100007'),
(62, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0010010100007'),
(63, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0010010100007'),
(64, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0010010100007'),
(65, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0026010100012'),
(66, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0026010100012'),
(67, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0026010100012'),
(68, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0026010100012'),
(69, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0026010100012'),
(70, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0026010100012'),
(71, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0026010100012'),
(72, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0026010100012'),
(73, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0007010100004'),
(74, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0007010100004'),
(75, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0007010100004'),
(76, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0007010100004'),
(77, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0007010100004'),
(78, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0007010100004'),
(79, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0007010100004'),
(80, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0007010100004'),
(81, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0006010100003'),
(82, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0006010100003'),
(83, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0006010100003'),
(84, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0006010100003'),
(85, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0006010100003'),
(86, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0006010100003'),
(87, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0006010100003'),
(88, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0006010100003'),
(89, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0012010100009'),
(90, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0012010100009'),
(91, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0012010100009'),
(92, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0012010100009'),
(93, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0012010100009'),
(94, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0012010100009'),
(95, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0012010100009'),
(96, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0012010100009'),
(97, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0004010100001'),
(98, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0004010100001'),
(99, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0004010100001'),
(100, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0004010100001'),
(101, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0004010100001'),
(102, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0004010100001'),
(103, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0004010100001'),
(104, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0004010100001'),
(105, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0101010100015'),
(106, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0101010100015'),
(107, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0101010100015'),
(108, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0101010100015'),
(109, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0101010100015'),
(110, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0101010100015'),
(111, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0101010100015'),
(112, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0101010100015'),
(113, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0061010100014'),
(114, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0061010100014'),
(115, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0061010100014'),
(116, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0061010100014'),
(117, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0061010100014'),
(118, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0061010100014'),
(119, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0061010100014'),
(120, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0061010100014'),
(121, '10% TAX', NULL, '10.0000', '1', 'WH0000103', '0016010100010'),
(122, '10% TAX', NULL, '10.0000', '1', 'WH0000201', '0016010100010'),
(123, '10% TAX', NULL, '10.0000', '1', 'WH0000101', '0016010100010'),
(124, '10% TAX', NULL, '10.0000', '1', 'WH0000501', '0016010100010'),
(125, '10% TAX', NULL, '10.0000', '1', 'WH0000401', '0016010100010'),
(126, '10% TAX', NULL, '10.0000', '1', 'WH0000202', '0016010100010'),
(127, '10% TAX', NULL, '10.0000', '1', 'WH0000102', '0016010100010'),
(128, '10% TAX', NULL, '10.0000', '1', 'WH0000301', '0016010100010'),
(129, '0% TAX', 'ZERT', '0.0000', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_till`
--

CREATE TABLE `sma_till` (
  `id` int(5) NOT NULL,
  `till_name` varchar(50) NOT NULL,
  `till_ip` varchar(50) NOT NULL,
  `store_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_till`
--

INSERT INTO `sma_till` (`id`, `till_name`, `till_ip`, `store_id`, `user_id`) VALUES
(7, 'till1', '132.132.20.120', 4, 20),
(8, 'till2', '132.132.5.59', 4, 19),
(10, 'till3', '132.132.5.120', 4, 21);

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `show_discount` tinyint(1) DEFAULT NULL,
  `upd_flg` tinyint(1) DEFAULT NULL,
  `usr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_users`
--

INSERT INTO `sma_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `show_discount`, `upd_flg`, `usr_id`) VALUES
(1, 0x3a3a31, 0x0000, 'owner', '7c222fb2927d828af22f592134e8932480637c0d', NULL, 'owner@essindia.com', NULL, NULL, NULL, NULL, 1351661704, 1469010535, 1, 'Owner', 'Owner', 'Stock Manager', '012345678', '9f4c84ab5a2b7ea782defada30dffe0d.jpg', 'male', 1, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL),
(15, 0x3a3a31, '', 'ANKIT', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, 1468668233, 1, 'ANKIT', 'ANKIT', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 8),
(16, 0x3a3a31, '', 'RAJAN', '4c8f53c8d8e48e4acea3a0aea18f10e35833ec42', 'Null', 'rajan@essindia.com', NULL, NULL, NULL, NULL, 1468497949, 1468498547, 1, 'RAJAN', 'RAJAN', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 10),
(17, NULL, '', 'ITADMIN', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, NULL, 1, 'ITADMIN', 'ITADMIN', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 2),
(18, 0x3a3a31, '', 'VIVEK', '7c222fb2927d828af22f592134e8932480637c0d', 'Null', 'vivek@essindia.com', NULL, NULL, NULL, NULL, 1468497949, 1468822736, 1, 'VIVEK', 'VIVEK', 'Default Group', '999999999', NULL, 'M', 6, 3, 6, NULL, 1, 1, 0, 0, NULL, 3),
(19, NULL, '', 'MANMOHAN', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, NULL, 1, 'MANMOHAN', 'MANMOHAN', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 6),
(20, 0x3133322e3133322e32302e313230, '', 'SHARAD', '7c222fb2927d828af22f592134e8932480637c0d', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, 1468905933, 1, 'SHARAD', 'SHARAD', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 7),
(21, NULL, '', 'ANAND', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, NULL, 1, 'ANAND', 'ANAND', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 9),
(22, NULL, '', 'NEERAJ', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, NULL, 1, 'NEERAJ', 'NEERAJ', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 4),
(23, 0x3a3a31, '', 'SUPERVISOR', '7c222fb2927d828af22f592134e8932480637c0d', 'Null', 'supervisor@essindia.co.in', NULL, NULL, NULL, NULL, 1468497949, 1469010609, 1, 'SUPERVISOR', 'SUPERVISOR', 'Default Group', '999999999', NULL, 'male', 6, 4, 6, NULL, 0, 0, 0, 0, NULL, 1),
(24, NULL, '', 'PRIYANKA', '39a2f03a3d6eed96d56f0672d2a19927589ef9f2', 'Null', 'Null', NULL, NULL, NULL, NULL, 1468497949, NULL, 1, 'PRIYANKA', 'PRIYANKA', 'Default Group', '999999999', NULL, 'M', 5, 4, 6, NULL, 0, 0, 0, 0, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_user_logins`
--

INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(1, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-06 09:16:19'),
(2, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-07 07:40:52'),
(3, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-01-14 12:13:52'),
(4, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-28 12:28:05'),
(5, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-02-02 05:49:35'),
(6, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-02-12 05:07:12'),
(7, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-02-12 06:18:44'),
(8, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 10:11:45'),
(9, 2, NULL, 0x3132372e302e302e31, 'ayush@essindia.com', '2016-07-13 10:50:13'),
(10, 3, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-07-13 10:51:53'),
(11, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 10:54:46'),
(12, 4, NULL, 0x3132372e302e302e31, 'ravi@essindia.com', '2016-07-13 10:57:48'),
(13, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 10:58:13'),
(14, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 11:44:24'),
(15, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 11:57:08'),
(16, 1, NULL, 0x3132372e302e302e31, 'owner@essindia.com', '2016-07-13 12:01:22'),
(17, 1, NULL, 0x3a3a31, 'owner@essindia.com', '2016-07-14 11:23:28'),
(18, 1, NULL, 0x3a3a31, 'owner@essindia.com', '2016-07-14 12:12:36'),
(19, 18, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-07-14 12:14:07'),
(20, 16, NULL, 0x3a3a31, 'rajan@essindia.com', '2016-07-14 12:15:47'),
(21, 1, NULL, 0x3a3a31, 'owner@essindia.com', '2016-07-14 12:17:11'),
(22, 1, NULL, 0x3a3a31, 'owner@essindia.com', '2016-07-14 12:57:49'),
(23, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 13:07:25'),
(24, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 14:00:06'),
(25, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 14:01:59'),
(26, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 14:13:56'),
(27, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 14:17:57'),
(28, 18, NULL, 0x3a3a31, 'vivek', '2016-07-14 14:20:13'),
(29, 1, NULL, 0x3a3a31, 'owner', '2016-07-14 14:39:22'),
(30, 18, NULL, 0x3a3a31, 'vivek', '2016-07-14 14:54:25'),
(31, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 04:50:54'),
(32, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 05:08:38'),
(33, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 05:47:23'),
(34, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 10:05:44'),
(35, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 10:34:54'),
(36, 18, NULL, 0x3a3a31, 'vivek', '2016-07-15 10:37:11'),
(37, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 13:27:52'),
(38, 18, NULL, 0x3a3a31, 'vivek', '2016-07-15 13:28:57'),
(39, 1, NULL, 0x3a3a31, 'owner', '2016-07-15 13:33:06'),
(40, 18, NULL, 0x3a3a31, 'vivek', '2016-07-15 13:34:52'),
(41, 18, NULL, 0x3a3a31, 'vivek', '2016-07-15 14:07:30'),
(42, 1, NULL, 0x3a3a31, 'owner', '2016-07-16 04:51:06'),
(43, 18, NULL, 0x3a3a31, 'vivek', '2016-07-16 05:30:29'),
(44, 1, NULL, 0x3a3a31, 'owner', '2016-07-16 05:38:37'),
(45, 18, NULL, 0x3a3a31, 'vivek', '2016-07-16 09:11:15'),
(46, 1, NULL, 0x3a3a31, 'owner', '2016-07-16 09:11:36'),
(47, 18, NULL, 0x3a3a31, 'vivek', '2016-07-16 09:12:15'),
(48, 15, NULL, 0x3a3a31, 'ankit', '2016-07-16 11:04:29'),
(49, 18, NULL, 0x3a3a31, 'vivek', '2016-07-16 11:06:11'),
(50, 15, NULL, 0x3a3a31, 'ankit', '2016-07-16 11:06:55'),
(51, 15, NULL, 0x3a3a31, 'ankit', '2016-07-16 11:09:12'),
(52, 15, NULL, 0x3a3a31, 'ankit', '2016-07-16 11:09:53'),
(53, 18, NULL, 0x3a3a31, 'vivek', '2016-07-16 11:22:29'),
(54, 15, NULL, 0x3a3a31, 'Ankit', '2016-07-16 11:23:53'),
(55, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 04:55:59'),
(56, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 05:55:53'),
(57, 18, NULL, 0x3a3a31, 'vivek', '2016-07-18 06:18:56'),
(58, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 06:40:49'),
(59, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 06:46:57'),
(60, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 06:57:55'),
(61, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 07:10:37'),
(62, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 07:11:24'),
(63, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 07:17:03'),
(64, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 07:26:21'),
(65, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 07:27:08'),
(66, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 07:28:48'),
(67, 1, NULL, 0x3a3a31, 'owner', '2016-07-18 08:01:33'),
(68, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 08:08:48'),
(69, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 08:11:02'),
(70, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 08:49:42'),
(71, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 09:26:29'),
(72, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 10:14:34'),
(73, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 10:22:26'),
(74, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-18 10:23:14'),
(75, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 10:24:32'),
(76, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 10:33:07'),
(77, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 10:35:56'),
(78, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-18 12:03:28'),
(79, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 12:05:25'),
(80, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 12:08:49'),
(81, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-18 12:20:33'),
(82, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 12:21:55'),
(83, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 12:26:17'),
(84, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 12:26:46'),
(85, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-18 12:27:58'),
(86, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-18 12:28:15'),
(87, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 12:39:18'),
(88, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-18 12:42:34'),
(89, 1, NULL, 0x3a3a31, 'owner', '2016-07-19 04:58:52'),
(90, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-19 04:59:18'),
(91, 20, NULL, 0x3a3a31, 'SHARAD', '2016-07-19 05:01:46'),
(92, 1, NULL, 0x3a3a31, 'owner', '2016-07-19 05:04:44'),
(93, 1, NULL, 0x3a3a31, 'owner', '2016-07-19 05:05:02'),
(94, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-19 05:05:13'),
(95, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-19 05:06:49'),
(96, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-19 05:11:28'),
(97, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-19 05:12:54'),
(98, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-19 05:24:18'),
(99, 20, NULL, 0x3133322e3133322e32302e313230, 'SHARAD', '2016-07-19 05:25:33'),
(100, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-19 05:25:53'),
(101, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-19 05:26:21'),
(102, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-19 07:41:05'),
(103, 1, NULL, 0x3133322e3133322e32302e313230, 'owner', '2016-07-19 09:58:06'),
(104, 23, NULL, 0x3133322e3133322e32302e313230, 'Supervisor', '2016-07-19 10:03:04'),
(105, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-20 05:10:35'),
(106, 1, NULL, 0x3a3a31, 'owner', '2016-07-20 05:27:16'),
(107, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-20 07:03:37'),
(108, 1, NULL, 0x3a3a31, 'owner', '2016-07-20 10:28:55'),
(109, 23, NULL, 0x3a3a31, 'Supervisor', '2016-07-20 10:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `org_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses`
--

INSERT INTO `sma_warehouses` (`id`, `code`, `name`, `address`, `map`, `phone`, `email`, `org_id`) VALUES
(1, 'WH0000202', 'WH0000202', 'Ghana', NULL, NULL, NULL, 'WH0000202'),
(2, 'WH0000102', 'WH0000102', 'Ghana', NULL, NULL, NULL, 'WH0000102'),
(3, 'WH0000201', 'WH0000201', 'INDIA', NULL, NULL, NULL, 'WH0000201'),
(4, 'WH0000101', 'WH0000101', 'INDIA', NULL, NULL, NULL, 'WH0000101'),
(5, 'WH0000501', 'WH0000501', 'INDIA', NULL, NULL, NULL, 'WH0000501'),
(6, 'WH0000401', 'WH0000401', 'INDIA', NULL, NULL, NULL, 'WH0000401'),
(7, 'WH0000301', 'WH0000301', 'INDIA', NULL, NULL, NULL, 'WH0000301'),
(8, 'WH0000103', 'WH0000103', 'INDIA', NULL, NULL, NULL, 'WH0000103');

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `rack` varchar(55) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses_products`
--

INSERT INTO `sma_warehouses_products` (`id`, `product_id`, `warehouse_id`, `lot_no`, `quantity`, `org_id`, `grp_id`, `rack`, `psale`) VALUES
(1, 1, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(2, 2, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(3, 3, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(4, 4, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(5, 5, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(6, 6, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(7, 7, 3, NULL, '0.0000', 'WH0000201', '0041010100013', NULL, NULL),
(8, 8, 4, 'LOT#00002', '257000.0000', 'WH0000101', '0041010100013', NULL, NULL),
(9, 9, 4, 'LOT#00002', '257000.0000', 'WH0000101', '0041010100013', NULL, NULL),
(10, 10, 4, 'LOT#00001', '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(11, 11, 4, 'LOT#00001', '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(12, 12, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(13, 13, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(14, 14, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(15, 15, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(16, 16, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(17, 17, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(18, 18, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(19, 19, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(20, 20, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(21, 21, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(22, 22, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(23, 23, 4, NULL, '0.0000', 'WH0000101', '0041010100013', NULL, NULL),
(24, 24, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(25, 25, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(26, 26, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(27, 27, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(28, 28, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(29, 29, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(30, 30, 5, NULL, '0.0000', 'WH0000501', '0041010100013', NULL, NULL),
(31, 31, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(32, 32, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(33, 33, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(34, 34, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(35, 35, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(36, 36, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(37, 37, 6, NULL, '0.0000', 'WH0000401', '0041010100013', NULL, NULL),
(38, 38, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(39, 39, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(40, 40, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(41, 41, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(42, 42, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(43, 43, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(44, 44, 7, NULL, '0.0000', 'WH0000301', '0041010100013', NULL, NULL),
(45, 45, 3, 'LOT#00001', '2.0000', 'WH0000201', '0008010100005', NULL, NULL),
(46, 46, 4, 'LOT#00002', '0.0000', 'WH0000101', '0008010100005', NULL, NULL),
(47, 47, 4, 'LOT#00002', '0.0000', 'WH0000101', '0008010100005', NULL, NULL),
(48, 48, 4, 'LOT#00001', '2.0000', 'WH0000101', '0008010100005', NULL, NULL),
(49, 49, 4, 'LOT#00001', '2.0000', 'WH0000101', '0008010100005', NULL, NULL),
(50, 50, 4, 'LOT#00005', '10.0000', 'WH0000101', '0008010100005', NULL, NULL),
(51, 51, 4, 'LOT#00005', '10.0000', 'WH0000101', '0008010100005', NULL, NULL),
(52, 52, 4, 'LOT#00004', '6.0000', 'WH0000101', '0008010100005', NULL, NULL),
(53, 53, 4, 'LOT#00004', '6.0000', 'WH0000101', '0008010100005', NULL, NULL),
(54, 54, 4, 'LOT#00006', '4.0000', 'WH0000101', '0008010100005', NULL, NULL),
(55, 55, 4, 'LOT#00006', '4.0000', 'WH0000101', '0008010100005', NULL, NULL),
(56, 56, 4, 'LOT#00003', '0.0000', 'WH0000101', '0008010100005', NULL, NULL),
(57, 57, 4, 'LOT#00003', '0.0000', 'WH0000101', '0008010100005', NULL, NULL),
(58, 58, 4, 'LOT#00007', '4.0000', 'WH0000101', '0008010100005', NULL, NULL),
(59, 59, 4, 'LOT#00007', '4.0000', 'WH0000101', '0008010100005', NULL, NULL),
(60, 60, 5, NULL, '0.0000', 'WH0000501', '0008010100005', NULL, NULL),
(61, 61, 6, NULL, '0.0000', 'WH0000401', '0008010100005', NULL, NULL),
(62, 62, 7, NULL, '0.0000', 'WH0000301', '0008010100005', NULL, NULL),
(63, 63, 3, 'LOT#00002', '5.0000', 'WH0000201', '0009010100006', NULL, NULL),
(64, 64, 3, 'LOT#00001', '10.0000', 'WH0000201', '0009010100006', NULL, NULL),
(65, 65, 4, 'LOT#00001', '6.0000', 'WH0000101', '0009010100006', NULL, NULL),
(66, 66, 4, 'LOT#00001', '6.0000', 'WH0000101', '0009010100006', NULL, NULL),
(67, 67, 5, NULL, '0.0000', 'WH0000501', '0009010100006', NULL, NULL),
(68, 68, 6, NULL, '0.0000', 'WH0000401', '0009010100006', NULL, NULL),
(69, 69, 7, NULL, '0.0000', 'WH0000301', '0009010100006', NULL, NULL),
(70, 70, 3, NULL, '0.0000', 'WH0000201', '0102010100016', NULL, NULL),
(71, 71, 4, NULL, '0.0000', 'WH0000101', '0102010100016', NULL, NULL),
(72, 72, 4, NULL, '0.0000', 'WH0000101', '0102010100016', NULL, NULL),
(73, 73, 5, NULL, '0.0000', 'WH0000501', '0102010100016', NULL, NULL),
(74, 74, 6, NULL, '0.0000', 'WH0000401', '0102010100016', NULL, NULL),
(75, 75, 7, NULL, '0.0000', 'WH0000301', '0102010100016', NULL, NULL),
(76, 76, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(77, 77, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(78, 78, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(79, 79, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(80, 80, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(81, 81, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(82, 82, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(83, 83, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(84, 84, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(85, 85, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(86, 86, 3, NULL, '0.0000', 'WH0000201', '0024010100011', NULL, NULL),
(87, 87, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(88, 88, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(89, 89, 4, 'LOT#00002', '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(90, 90, 4, 'LOT#00002', '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(91, 91, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(92, 92, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(93, 93, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(94, 94, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(95, 95, 4, 'LOT#00001', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(96, 96, 4, 'LOT#00001', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(97, 97, 4, 'LOT#00001', '4.0000', 'WH0000101', '0024010100011', NULL, NULL),
(98, 98, 4, 'LOT#00001', '4.0000', 'WH0000101', '0024010100011', NULL, NULL),
(99, 99, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(100, 100, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(101, 101, 4, 'LOT#00004', '2.0000', 'WH0000101', '0024010100011', NULL, NULL),
(102, 102, 4, 'LOT#00004', '2.0000', 'WH0000101', '0024010100011', NULL, NULL),
(103, 103, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(104, 104, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(105, 105, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(106, 106, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(107, 107, 4, 'LOT#00001', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(108, 108, 4, 'LOT#00001', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(109, 109, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(110, 110, 4, 'LOT#00002', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(111, 111, 4, 'LOT#00001', '8.0000', 'WH0000101', '0024010100011', NULL, NULL),
(112, 112, 4, 'LOT#00001', '8.0000', 'WH0000101', '0024010100011', NULL, NULL),
(113, 113, 4, 'LOT#00005', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(114, 114, 4, 'LOT#00005', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(115, 115, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(116, 116, 4, 'LOT#00003', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(117, 117, 4, 'LOT#00001', '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(118, 118, 4, 'LOT#00001', '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(119, 119, 4, 'LOT#00004', '2.0000', 'WH0000101', '0024010100011', NULL, NULL),
(120, 120, 4, 'LOT#00004', '2.0000', 'WH0000101', '0024010100011', NULL, NULL),
(121, 121, 4, 'LOT#00001', '11.0000', 'WH0000101', '0024010100011', NULL, NULL),
(122, 122, 4, 'LOT#00001', '11.0000', 'WH0000101', '0024010100011', NULL, NULL),
(123, 123, 4, 'LOT#00004', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(124, 124, 4, 'LOT#00004', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(125, 125, 4, 'LOT#00001', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(126, 126, 4, 'LOT#00001', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(127, 127, 4, 'LOT#00001', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(128, 128, 4, 'LOT#00001', '10.0000', 'WH0000101', '0024010100011', NULL, NULL),
(129, 129, 4, 'LOT#00004', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(130, 130, 4, 'LOT#00004', '3.0000', 'WH0000101', '0024010100011', NULL, NULL),
(131, 131, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(132, 132, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(133, 133, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(134, 134, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(135, 135, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(136, 136, 4, NULL, '0.0000', 'WH0000101', '0024010100011', NULL, NULL),
(137, 137, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(138, 138, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(139, 139, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(140, 140, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(141, 141, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(142, 142, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(143, 143, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(144, 144, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(145, 145, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(146, 146, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(147, 147, 5, NULL, '0.0000', 'WH0000501', '0024010100011', NULL, NULL),
(148, 148, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(149, 149, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(150, 150, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(151, 151, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(152, 152, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(153, 153, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(154, 154, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(155, 155, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(156, 156, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(157, 157, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(158, 158, 6, NULL, '0.0000', 'WH0000401', '0024010100011', NULL, NULL),
(159, 159, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(160, 160, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(161, 161, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(162, 162, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(163, 163, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(164, 164, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(165, 165, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(166, 166, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(167, 167, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(168, 168, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(169, 169, 7, NULL, '0.0000', 'WH0000301', '0024010100011', NULL, NULL),
(170, 170, 3, 'LOT#00001', '98.0000', 'WH0000201', '0005010100002', NULL, NULL),
(171, 171, 3, NULL, '0.0000', 'WH0000201', '0005010100002', NULL, NULL),
(172, 172, 3, NULL, '0.0000', 'WH0000201', '0005010100002', NULL, NULL),
(173, 173, 3, NULL, '0.0000', 'WH0000201', '0005010100002', NULL, NULL),
(174, 174, 4, 'LOT#00001', '-3.0000', 'WH0000101', '0005010100002', NULL, NULL),
(175, 175, 4, 'LOT#00001', '4.0000', 'WH0000101', '0005010100002', NULL, NULL),
(176, 176, 4, 'LOT#00001', '10.0000', 'WH0000101', '0005010100002', NULL, NULL),
(177, 177, 4, 'LOT#00001', '5.0000', 'WH0000101', '0005010100002', NULL, NULL),
(178, 178, 4, 'LOT#00001', '4.0000', 'WH0000101', '0005010100002', NULL, NULL),
(179, 179, 4, 'LOT#00001', '4.0000', 'WH0000101', '0005010100002', NULL, NULL),
(180, 180, 4, 'LOT#00002', '3.0000', 'WH0000101', '0005010100002', NULL, NULL),
(181, 181, 4, 'LOT#00002', '3.0000', 'WH0000101', '0005010100002', NULL, NULL),
(182, 182, 4, 'LOT#00002', '10.0000', 'WH0000101', '0005010100002', NULL, NULL),
(183, 183, 4, 'LOT#00002', '10.0000', 'WH0000101', '0005010100002', NULL, NULL),
(184, 184, 4, NULL, '0.0000', 'WH0000101', '0005010100002', NULL, NULL),
(185, 185, 4, NULL, '-1.0000', 'WH0000101', '0005010100002', NULL, NULL),
(186, 186, 5, NULL, '0.0000', 'WH0000501', '0005010100002', NULL, NULL),
(187, 187, 5, NULL, '0.0000', 'WH0000501', '0005010100002', NULL, NULL),
(188, 188, 5, NULL, '0.0000', 'WH0000501', '0005010100002', NULL, NULL),
(189, 189, 5, NULL, '0.0000', 'WH0000501', '0005010100002', NULL, NULL),
(190, 190, 6, NULL, '0.0000', 'WH0000401', '0005010100002', NULL, NULL),
(191, 191, 6, NULL, '0.0000', 'WH0000401', '0005010100002', NULL, NULL),
(192, 192, 6, NULL, '0.0000', 'WH0000401', '0005010100002', NULL, NULL),
(193, 193, 6, NULL, '0.0000', 'WH0000401', '0005010100002', NULL, NULL),
(194, 194, 7, NULL, '0.0000', 'WH0000301', '0005010100002', NULL, NULL),
(195, 195, 7, NULL, '0.0000', 'WH0000301', '0005010100002', NULL, NULL),
(196, 196, 7, NULL, '0.0000', 'WH0000301', '0005010100002', NULL, NULL),
(197, 197, 7, NULL, '0.0000', 'WH0000301', '0005010100002', NULL, NULL),
(198, 198, 3, 'LOT#00001', '40.0000', 'WH0000201', '0011010100008', NULL, NULL),
(199, 199, 3, 'LOT#00001', '10.0000', 'WH0000201', '0011010100008', NULL, NULL),
(200, 200, 3, 'LOT#00006', '1.0000', 'WH0000201', '0011010100008', NULL, NULL),
(201, 201, 3, 'LOT#00005', '10.0000', 'WH0000201', '0011010100008', NULL, NULL),
(202, 202, 3, 'LOT#00003', '90.0000', 'WH0000201', '0011010100008', NULL, NULL),
(203, 203, 3, 'LOT#00004', '10.0000', 'WH0000201', '0011010100008', NULL, NULL),
(204, 204, 3, 'LOT#00002', '40.0000', 'WH0000201', '0011010100008', NULL, NULL),
(205, 205, 3, 'AC#00001', '10.0000', 'WH0000201', '0011010100008', NULL, NULL),
(206, 206, 3, 'LOT#00001', '25.0000', 'WH0000201', '0011010100008', NULL, NULL),
(207, 207, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(208, 208, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(209, 209, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(210, 210, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(211, 211, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(212, 212, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(213, 213, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(214, 214, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(215, 215, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(216, 216, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(217, 217, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(218, 218, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(219, 219, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(220, 220, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(221, 221, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(222, 222, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(223, 223, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(224, 224, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(225, 225, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(226, 226, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(227, 227, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(228, 228, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(229, 229, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(230, 230, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(231, 231, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(232, 232, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(233, 233, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(234, 234, 3, NULL, '0.0000', 'WH0000201', '0011010100008', NULL, NULL),
(235, 235, 4, 'AC#00002', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(236, 236, 4, 'AC#00002', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(237, 237, 4, 'AC#00002', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(238, 238, 4, 'AC#00002', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(239, 239, 4, 'LOT#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(240, 240, 4, 'LOT#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(241, 241, 4, 'LOT#00002', '578.0000', 'WH0000101', '0011010100008', NULL, NULL),
(242, 242, 4, 'LOT#00002', '578.0000', 'WH0000101', '0011010100008', NULL, NULL),
(243, 243, 4, 'LOT#00006', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(244, 244, 4, 'LOT#00006', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(245, 245, 4, 'AC#00006', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(246, 246, 4, 'AC#00006', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(247, 247, 4, 'LOT#00001', '80.0000', 'WH0000101', '0011010100008', NULL, NULL),
(248, 248, 4, 'LOT#00001', '80.0000', 'WH0000101', '0011010100008', NULL, NULL),
(249, 249, 4, 'AC#00005', '999.0000', 'WH0000101', '0011010100008', NULL, NULL),
(250, 250, 4, 'AC#00005', '999.0000', 'WH0000101', '0011010100008', NULL, NULL),
(251, 251, 4, 'LOT#00003', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(252, 252, 4, 'LOT#00003', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(253, 253, 4, 'LOT#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(254, 254, 4, 'LOT#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(255, 255, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(256, 256, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(257, 257, 4, 'LOT#00005', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(258, 258, 4, 'LOT#00005', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(259, 259, 4, 'LOT#00007', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(260, 260, 4, 'LOT#00007', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(261, 261, 4, 'LOT#00001', '40.0000', 'WH0000101', '0011010100008', NULL, NULL),
(262, 262, 4, 'LOT#00001', '40.0000', 'WH0000101', '0011010100008', NULL, NULL),
(263, 263, 4, 'AC#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(264, 264, 4, 'AC#00001', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(265, 265, 4, 'LOT#00001', '80.0000', 'WH0000101', '0011010100008', NULL, NULL),
(266, 266, 4, 'LOT#00001', '80.0000', 'WH0000101', '0011010100008', NULL, NULL),
(267, 267, 4, 'LOT#00001', '103.0000', 'WH0000101', '0011010100008', NULL, NULL),
(268, 268, 4, 'LOT#00001', '103.0000', 'WH0000101', '0011010100008', NULL, NULL),
(269, 269, 4, 'LOT#00002', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(270, 270, 4, 'LOT#00002', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(271, 271, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(272, 272, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(273, 273, 4, 'AC#00003', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(274, 274, 4, 'AC#00003', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(275, 275, 4, 'LOT#00003', '500.0000', 'WH0000101', '0011010100008', NULL, NULL),
(276, 276, 4, 'LOT#00003', '500.0000', 'WH0000101', '0011010100008', NULL, NULL),
(277, 277, 4, 'AC#00003', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(278, 278, 4, 'AC#00003', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(279, 279, 4, 'LOT#00003', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(280, 280, 4, 'LOT#00003', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(281, 281, 4, 'AC#00004', '5.0000', 'WH0000101', '0011010100008', NULL, NULL),
(282, 282, 4, 'AC#00004', '5.0000', 'WH0000101', '0011010100008', NULL, NULL),
(283, 283, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(284, 284, 4, 'LOT#00001', '100.0000', 'WH0000101', '0011010100008', NULL, NULL),
(285, 285, 4, 'LOT#00001', '10.0000', 'WH0000101', '0011010100008', NULL, NULL),
(286, 286, 4, 'LOT#00001', '10.0000', 'WH0000101', '0011010100008', NULL, NULL),
(287, 287, 4, 'LOT#00009', '49801003.0000', 'WH0000101', '0011010100008', NULL, NULL),
(288, 288, 4, 'LOT#00009', '49801003.0000', 'WH0000101', '0011010100008', NULL, NULL),
(289, 289, 4, 'LOT#00002', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(290, 290, 4, 'LOT#00002', '1.0000', 'WH0000101', '0011010100008', NULL, NULL),
(291, 291, 4, 'LOT#00002', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(292, 292, 4, 'LOT#00002', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(293, 293, 4, 'LOT#00002', '2.0000', 'WH0000101', '0011010100008', NULL, NULL),
(294, 294, 4, 'LOT#00002', '2.0000', 'WH0000101', '0011010100008', NULL, NULL),
(295, 295, 4, 'LOT#00001', '9.0000', 'WH0000101', '0011010100008', NULL, NULL),
(296, 296, 4, 'LOT#00001', '9.0000', 'WH0000101', '0011010100008', NULL, NULL),
(297, 297, 4, 'AC#00001', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(298, 298, 4, 'AC#00001', '3.0000', 'WH0000101', '0011010100008', NULL, NULL),
(299, 299, 4, 'LOT#00004', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(300, 300, 4, 'LOT#00004', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(301, 301, 4, 'LOT#00001', '10.0000', 'WH0000101', '0011010100008', NULL, NULL),
(302, 302, 4, 'LOT#00001', '10.0000', 'WH0000101', '0011010100008', NULL, NULL),
(303, 303, 4, 'LOT#00008', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(304, 304, 4, 'LOT#00008', '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(305, 305, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(306, 306, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(307, 307, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(308, 308, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(309, 309, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(310, 310, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(311, 311, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(312, 312, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(313, 313, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(314, 314, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(315, 315, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(316, 316, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(317, 317, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(318, 318, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(319, 319, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(320, 320, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(321, 321, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(322, 322, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(323, 323, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(324, 324, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(325, 325, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(326, 326, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(327, 327, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(328, 328, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(329, 329, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(330, 330, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(331, 331, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(332, 332, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(333, 333, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(334, 334, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(335, 335, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(336, 336, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(337, 337, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(338, 338, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(339, 339, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(340, 340, 4, NULL, '0.0000', 'WH0000101', '0011010100008', NULL, NULL),
(341, 341, 5, 'LOT#00001', '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(342, 342, 5, 'LOT#00001', '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(343, 343, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(344, 344, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(345, 345, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(346, 346, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(347, 347, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(348, 348, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(349, 349, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(350, 350, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(351, 351, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(352, 352, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(353, 353, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(354, 354, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(355, 355, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(356, 356, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(357, 357, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(358, 358, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(359, 359, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(360, 360, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(361, 361, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(362, 362, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(363, 363, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(364, 364, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(365, 365, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(366, 366, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(367, 367, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(368, 368, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(369, 369, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(370, 370, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(371, 371, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(372, 372, 5, NULL, '0.0000', 'WH0000501', '0011010100008', NULL, NULL),
(373, 373, 6, 'LOT#00002', '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(374, 374, 6, 'LOT#00001', '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(375, 375, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(376, 376, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(377, 377, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(378, 378, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(379, 379, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(380, 380, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(381, 381, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(382, 382, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(383, 383, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(384, 384, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(385, 385, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(386, 386, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(387, 387, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(388, 388, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(389, 389, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(390, 390, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(391, 391, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(392, 392, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(393, 393, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(394, 394, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(395, 395, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(396, 396, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(397, 397, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(398, 398, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(399, 399, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(400, 400, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(401, 401, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(402, 402, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(403, 403, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(404, 404, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(405, 405, 6, NULL, '0.0000', 'WH0000401', '0011010100008', NULL, NULL),
(406, 406, 7, 'LOT#00001', '300000.0000', 'WH0000301', '0011010100008', NULL, NULL),
(407, 407, 7, 'LOT#00002', '287000.0000', 'WH0000301', '0011010100008', NULL, NULL),
(408, 408, 7, 'LOT#00001', '287000.0000', 'WH0000301', '0011010100008', NULL, NULL),
(409, 409, 7, 'LOT#00001', '300000.0000', 'WH0000301', '0011010100008', NULL, NULL),
(410, 410, 7, 'LOT#00002', '287000.0000', 'WH0000301', '0011010100008', NULL, NULL),
(411, 411, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(412, 412, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(413, 413, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(414, 414, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(415, 415, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(416, 416, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(417, 417, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(418, 418, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(419, 419, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(420, 420, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(421, 421, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(422, 422, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(423, 423, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(424, 424, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(425, 425, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(426, 426, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(427, 427, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(428, 428, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(429, 429, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(430, 430, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(431, 431, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(432, 432, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(433, 433, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(434, 434, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(435, 435, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(436, 436, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(437, 437, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(438, 438, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(439, 439, 7, NULL, '0.0000', 'WH0000301', '0011010100008', NULL, NULL),
(440, 440, 3, NULL, '0.0000', 'WH0000201', '0010010100007', NULL, NULL),
(441, 441, 3, NULL, '0.0000', 'WH0000201', '0010010100007', NULL, NULL),
(442, 442, 3, NULL, '0.0000', 'WH0000201', '0010010100007', NULL, NULL),
(443, 443, 4, 'LOT#00001', '85.0000', 'WH0000101', '0010010100007', NULL, NULL),
(444, 444, 4, 'LOT#00001', '85.0000', 'WH0000101', '0010010100007', NULL, NULL),
(445, 445, 4, 'LOT#00001', '86.0000', 'WH0000101', '0010010100007', NULL, NULL),
(446, 446, 4, 'LOT#00001', '86.0000', 'WH0000101', '0010010100007', NULL, NULL),
(447, 447, 4, NULL, '0.0000', 'WH0000101', '0010010100007', NULL, NULL),
(448, 448, 4, NULL, '0.0000', 'WH0000101', '0010010100007', NULL, NULL),
(449, 449, 5, NULL, '0.0000', 'WH0000501', '0010010100007', NULL, NULL),
(450, 450, 5, NULL, '0.0000', 'WH0000501', '0010010100007', NULL, NULL),
(451, 451, 5, NULL, '0.0000', 'WH0000501', '0010010100007', NULL, NULL),
(452, 452, 6, NULL, '0.0000', 'WH0000401', '0010010100007', NULL, NULL),
(453, 453, 6, NULL, '0.0000', 'WH0000401', '0010010100007', NULL, NULL),
(454, 454, 6, NULL, '0.0000', 'WH0000401', '0010010100007', NULL, NULL),
(455, 455, 7, NULL, '0.0000', 'WH0000301', '0010010100007', NULL, NULL),
(456, 456, 7, NULL, '0.0000', 'WH0000301', '0010010100007', NULL, NULL),
(457, 457, 7, NULL, '0.0000', 'WH0000301', '0010010100007', NULL, NULL),
(458, 458, 3, NULL, '0.0000', 'WH0000201', '0026010100012', NULL, NULL),
(459, 459, 4, 'LOT#00001', '10.0000', 'WH0000101', '0026010100012', NULL, NULL),
(460, 460, 4, 'LOT#00001', '10.0000', 'WH0000101', '0026010100012', NULL, NULL),
(461, 461, 5, NULL, '0.0000', 'WH0000501', '0026010100012', NULL, NULL),
(462, 462, 6, NULL, '0.0000', 'WH0000401', '0026010100012', NULL, NULL),
(463, 463, 7, NULL, '0.0000', 'WH0000301', '0026010100012', NULL, NULL),
(464, 464, 3, 'LOT#00001', '10.0000', 'WH0000201', '0007010100004', NULL, NULL),
(465, 465, 3, 'LOT#00002', '10.0000', 'WH0000201', '0007010100004', NULL, NULL),
(466, 466, 3, NULL, '0.0000', 'WH0000201', '0007010100004', NULL, NULL),
(467, 467, 4, 'LOT#00002', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(468, 468, 4, 'LOT#00002', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(469, 469, 4, 'LOT#00006', '867367.0000', 'WH0000101', '0007010100004', NULL, NULL),
(470, 470, 4, 'LOT#00006', '867367.0000', 'WH0000101', '0007010100004', NULL, NULL),
(471, 471, 4, 'LOT_1', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(472, 472, 4, 'LOT_1', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(473, 473, 4, 'LOT#00003', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(474, 474, 4, 'LOT#00003', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(475, 475, 4, 'LOT#00007', '100.0000', 'WH0000101', '0007010100004', NULL, NULL),
(476, 476, 4, 'LOT#00007', '100.0000', 'WH0000101', '0007010100004', NULL, NULL),
(477, 477, 4, 'LOT#00001', '118.0000', 'WH0000101', '0007010100004', NULL, NULL),
(478, 478, 4, 'LOT#00001', '118.0000', 'WH0000101', '0007010100004', NULL, NULL),
(479, 479, 4, 'LOT#00005', '500.0000', 'WH0000101', '0007010100004', NULL, NULL),
(480, 480, 4, 'LOT#00005', '500.0000', 'WH0000101', '0007010100004', NULL, NULL),
(481, 481, 4, 'LOT#00004', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(482, 482, 4, 'LOT#00004', '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(483, 483, 4, NULL, '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(484, 484, 4, NULL, '0.0000', 'WH0000101', '0007010100004', NULL, NULL),
(485, 485, 5, NULL, '0.0000', 'WH0000501', '0007010100004', NULL, NULL),
(486, 486, 5, NULL, '0.0000', 'WH0000501', '0007010100004', NULL, NULL),
(487, 487, 6, NULL, '0.0000', 'WH0000401', '0007010100004', NULL, NULL),
(488, 488, 6, NULL, '0.0000', 'WH0000401', '0007010100004', NULL, NULL),
(489, 489, 7, NULL, '0.0000', 'WH0000301', '0007010100004', NULL, NULL),
(490, 490, 7, NULL, '0.0000', 'WH0000301', '0007010100004', NULL, NULL),
(491, 491, 3, 'LOT#00002', '2.0000', 'WH0000201', '0006010100003', NULL, NULL),
(492, 492, 3, 'LOT#00001', '4.0000', 'WH0000201', '0006010100003', NULL, NULL),
(493, 493, 3, 'LOT#00003', '5.0000', 'WH0000201', '0006010100003', NULL, NULL),
(494, 494, 3, 'LOT#00001', '1.0000', 'WH0000201', '0006010100003', NULL, NULL),
(495, 495, 3, 'LOT#00002', '10.0000', 'WH0000201', '0006010100003', NULL, NULL),
(496, 496, 3, 'LOT#00001', '17.0000', 'WH0000201', '0006010100003', NULL, NULL),
(497, 497, 3, 'LOT#00001', '4.0000', 'WH0000201', '0006010100003', NULL, NULL),
(498, 498, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(499, 499, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(500, 500, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(501, 501, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(502, 502, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(503, 503, 3, NULL, '0.0000', 'WH0000201', '0006010100003', NULL, NULL),
(504, 504, 4, 'LOT#00001', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(505, 505, 4, 'LOT#00001', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(506, 506, 4, 'LOT#00006', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(507, 507, 4, 'LOT#00006', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(508, 508, 4, 'LOT#00002', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(509, 509, 4, 'LOT#00002', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(510, 510, 4, 'LOT#00001', '3.0000', 'WH0000101', '0006010100003', NULL, NULL),
(511, 511, 4, 'LOT#00001', '3.0000', 'WH0000101', '0006010100003', NULL, NULL),
(512, 512, 4, 'LOT#00002', '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(513, 513, 4, 'LOT#00002', '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(514, 514, 4, 'LOT#00002', '7.0000', 'WH0000101', '0006010100003', NULL, NULL),
(515, 515, 4, 'LOT#00002', '7.0000', 'WH0000101', '0006010100003', NULL, NULL),
(516, 516, 4, 'LOT#00005', '9.0000', 'WH0000101', '0006010100003', NULL, NULL),
(517, 517, 4, 'LOT#00005', '9.0000', 'WH0000101', '0006010100003', NULL, NULL),
(518, 518, 4, 'LOT#00008', '2000000.0000', 'WH0000101', '0006010100003', NULL, NULL),
(519, 519, 4, 'LOT#00008', '2000000.0000', 'WH0000101', '0006010100003', NULL, NULL),
(520, 520, 4, 'LOT#00004', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(521, 521, 4, 'LOT#00004', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(522, 522, 4, 'LOT#00001', '9.0000', 'WH0000101', '0006010100003', NULL, NULL),
(523, 523, 4, 'LOT#00001', '9.0000', 'WH0000101', '0006010100003', NULL, NULL),
(524, 524, 4, 'LOT#00003', '50000.0000', 'WH0000101', '0006010100003', NULL, NULL),
(525, 525, 4, 'LOT#00003', '50000.0000', 'WH0000101', '0006010100003', NULL, NULL),
(526, 526, 4, 'LOT#00001', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(527, 527, 4, 'LOT#00001', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(528, 528, 4, 'LOT_1', '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(529, 529, 4, 'LOT_1', '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(530, 530, 4, 'LOT#00007', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(531, 531, 4, 'LOT#00007', '10.0000', 'WH0000101', '0006010100003', NULL, NULL),
(532, 532, 4, 'LOT#00001', '5.0000', 'WH0000101', '0006010100003', NULL, NULL),
(533, 533, 4, 'LOT#00001', '5.0000', 'WH0000101', '0006010100003', NULL, NULL),
(534, 534, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(535, 535, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(536, 536, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(537, 537, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(538, 538, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(539, 539, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(540, 540, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(541, 541, 4, NULL, '0.0000', 'WH0000101', '0006010100003', NULL, NULL),
(542, 542, 5, 'LOT#00001', '10.0000', 'WH0000501', '0006010100003', NULL, NULL),
(543, 543, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(544, 544, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(545, 545, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(546, 546, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(547, 547, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(548, 548, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(549, 549, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(550, 550, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(551, 551, 5, NULL, '0.0000', 'WH0000501', '0006010100003', NULL, NULL),
(552, 552, 6, 'LOT#00001', '10.0000', 'WH0000401', '0006010100003', NULL, NULL),
(553, 553, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(554, 554, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(555, 555, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(556, 556, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(557, 557, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(558, 558, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(559, 559, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(560, 560, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(561, 561, 6, NULL, '0.0000', 'WH0000401', '0006010100003', NULL, NULL),
(562, 562, 7, 'LOT#00001', '10.0000', 'WH0000301', '0006010100003', NULL, NULL),
(563, 563, 7, 'LOT#00002', '10.0000', 'WH0000301', '0006010100003', NULL, NULL),
(564, 564, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(565, 565, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(566, 566, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(567, 567, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(568, 568, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(569, 569, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(570, 570, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(571, 571, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(572, 572, 7, NULL, '0.0000', 'WH0000301', '0006010100003', NULL, NULL),
(573, 573, 3, 'LOT#00002', '5.0000', 'WH0000201', '0012010100009', NULL, NULL),
(574, 574, 3, 'LOT#00001', '2.0000', 'WH0000201', '0012010100009', NULL, NULL),
(575, 575, 3, 'LOT#00001', '8.0000', 'WH0000201', '0012010100009', NULL, NULL),
(576, 576, 3, 'LOT#00001', '100.0000', 'WH0000201', '0012010100009', NULL, NULL),
(577, 577, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(578, 578, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(579, 579, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(580, 580, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(581, 581, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(582, 582, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(583, 583, 3, NULL, '0.0000', 'WH0000201', '0012010100009', NULL, NULL),
(584, 584, 4, 'LOT#00001', '1.0000', 'WH0000101', '0012010100009', NULL, NULL),
(585, 585, 4, 'LOT#00001', '1.0000', 'WH0000101', '0012010100009', NULL, NULL),
(586, 586, 4, 'LOT#00001', '10.0000', 'WH0000101', '0012010100009', NULL, NULL),
(587, 587, 4, 'LOT#00001', '10.0000', 'WH0000101', '0012010100009', NULL, NULL),
(588, 588, 4, 'LOT#00001', '97.0000', 'WH0000101', '0012010100009', NULL, NULL),
(589, 589, 4, 'LOT#00001', '97.0000', 'WH0000101', '0012010100009', NULL, NULL),
(590, 590, 4, 'LOT#00002', '257005.0000', 'WH0000101', '0012010100009', NULL, NULL),
(591, 591, 4, 'LOT#00002', '257005.0000', 'WH0000101', '0012010100009', NULL, NULL),
(592, 592, 4, 'LOT#00003', '300000.0000', 'WH0000101', '0012010100009', NULL, NULL),
(593, 593, 4, 'LOT#00003', '300000.0000', 'WH0000101', '0012010100009', NULL, NULL),
(594, 594, 4, 'LOT#00001', '1.0000', 'WH0000101', '0012010100009', NULL, NULL),
(595, 595, 4, 'LOT#00001', '1.0000', 'WH0000101', '0012010100009', NULL, NULL),
(596, 596, 4, 'LOT#00002', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(597, 597, 4, 'LOT#00002', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(598, 598, 4, 'LOT#00001', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(599, 599, 4, 'LOT#00001', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(600, 600, 4, 'LOT#00002', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(601, 601, 4, 'LOT#00002', '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(602, 602, 4, 'LOT#00001', '4.0000', 'WH0000101', '0012010100009', NULL, NULL),
(603, 603, 4, 'LOT#00001', '4.0000', 'WH0000101', '0012010100009', NULL, NULL),
(604, 604, 4, 'LOT#00001', '10.0000', 'WH0000101', '0012010100009', NULL, NULL),
(605, 605, 4, 'LOT#00001', '10.0000', 'WH0000101', '0012010100009', NULL, NULL),
(606, 606, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(607, 607, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(608, 608, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(609, 609, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(610, 610, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(611, 611, 4, NULL, '0.0000', 'WH0000101', '0012010100009', NULL, NULL),
(612, 612, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(613, 613, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(614, 614, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(615, 615, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(616, 616, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(617, 617, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(618, 618, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(619, 619, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(620, 620, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(621, 621, 5, NULL, '0.0000', 'WH0000501', '0012010100009', NULL, NULL),
(622, 622, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(623, 623, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(624, 624, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(625, 625, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(626, 626, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(627, 627, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(628, 628, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(629, 629, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(630, 630, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(631, 631, 6, NULL, '0.0000', 'WH0000401', '0012010100009', NULL, NULL),
(632, 632, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(633, 633, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(634, 634, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(635, 635, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(636, 636, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(637, 637, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(638, 638, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(639, 639, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(640, 640, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(641, 641, 7, NULL, '0.0000', 'WH0000301', '0012010100009', NULL, NULL),
(642, 642, 3, 'LOT#00001', '4.0000', 'WH0000201', '0004010100001', NULL, NULL),
(643, 643, 3, NULL, '0.0000', 'WH0000201', '0004010100001', NULL, NULL),
(644, 644, 3, NULL, '0.0000', 'WH0000201', '0004010100001', NULL, NULL),
(645, 645, 3, NULL, '0.0000', 'WH0000201', '0004010100001', NULL, NULL),
(646, 646, 3, NULL, '0.0000', 'WH0000201', '0004010100001', NULL, NULL),
(647, 647, 4, 'LOT#00002', '0.0000', 'WH0000101', '0004010100001', NULL, NULL),
(648, 648, 4, 'LOT#00002', '0.0000', 'WH0000101', '0004010100001', NULL, NULL),
(649, 649, 4, 'LOT#00001', '1.0000', 'WH0000101', '0004010100001', NULL, NULL),
(650, 650, 4, 'LOT#00001', '1.0000', 'WH0000101', '0004010100001', NULL, NULL),
(651, 651, 4, 'LOT#00001', '3.0000', 'WH0000101', '0004010100001', NULL, NULL),
(652, 652, 4, 'LOT#00001', '3.0000', 'WH0000101', '0004010100001', NULL, NULL),
(653, 653, 4, 'LOT#00001', '1.0000', 'WH0000101', '0004010100001', NULL, NULL),
(654, 654, 4, 'LOT#00001', '1.0000', 'WH0000101', '0004010100001', NULL, NULL),
(655, 655, 4, 'LOT#00003', '250005.0000', 'WH0000101', '0004010100001', NULL, NULL),
(656, 656, 4, 'LOT#00003', '250005.0000', 'WH0000101', '0004010100001', NULL, NULL),
(657, 657, 4, 'LOT#00002', '3.0000', 'WH0000101', '0004010100001', NULL, NULL),
(658, 658, 4, 'LOT#00002', '3.0000', 'WH0000101', '0004010100001', NULL, NULL),
(659, 659, 4, 'LOT#00001', '2.0000', 'WH0000101', '0004010100001', NULL, NULL),
(660, 660, 4, 'LOT#00001', '2.0000', 'WH0000101', '0004010100001', NULL, NULL),
(661, 661, 4, NULL, '0.0000', 'WH0000101', '0004010100001', NULL, NULL),
(662, 662, 4, NULL, '0.0000', 'WH0000101', '0004010100001', NULL, NULL),
(663, 663, 5, NULL, '0.0000', 'WH0000501', '0004010100001', NULL, NULL),
(664, 664, 5, NULL, '0.0000', 'WH0000501', '0004010100001', NULL, NULL),
(665, 665, 5, NULL, '0.0000', 'WH0000501', '0004010100001', NULL, NULL),
(666, 666, 5, NULL, '0.0000', 'WH0000501', '0004010100001', NULL, NULL),
(667, 667, 5, NULL, '0.0000', 'WH0000501', '0004010100001', NULL, NULL),
(668, 668, 6, NULL, '0.0000', 'WH0000401', '0004010100001', NULL, NULL),
(669, 669, 6, NULL, '0.0000', 'WH0000401', '0004010100001', NULL, NULL),
(670, 670, 6, NULL, '0.0000', 'WH0000401', '0004010100001', NULL, NULL),
(671, 671, 6, NULL, '0.0000', 'WH0000401', '0004010100001', NULL, NULL),
(672, 672, 6, NULL, '0.0000', 'WH0000401', '0004010100001', NULL, NULL),
(673, 673, 7, NULL, '0.0000', 'WH0000301', '0004010100001', NULL, NULL),
(674, 674, 7, NULL, '0.0000', 'WH0000301', '0004010100001', NULL, NULL),
(675, 675, 7, NULL, '0.0000', 'WH0000301', '0004010100001', NULL, NULL);
INSERT INTO `sma_warehouses_products` (`id`, `product_id`, `warehouse_id`, `lot_no`, `quantity`, `org_id`, `grp_id`, `rack`, `psale`) VALUES
(676, 676, 7, NULL, '0.0000', 'WH0000301', '0004010100001', NULL, NULL),
(677, 677, 7, NULL, '0.0000', 'WH0000301', '0004010100001', NULL, NULL),
(678, 678, 3, 'LOT#00003', '190.0000', 'WH0000201', '0016010100010', NULL, NULL),
(679, 679, 3, 'LOT#00001', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(680, 680, 3, 'LOT#00005', '5.0000', 'WH0000201', '0016010100010', NULL, NULL),
(681, 681, 3, 'LOT#00004', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(682, 682, 3, 'LOT#00002', '4.0000', 'WH0000201', '0016010100010', NULL, NULL),
(683, 683, 3, 'LOT#00001', '16.0000', 'WH0000201', '0016010100010', NULL, NULL),
(684, 684, 3, 'LOT#00003', '5.0000', 'WH0000201', '0016010100010', NULL, NULL),
(685, 685, 3, 'LOT#00001', '1.0000', 'WH0000201', '0016010100010', NULL, NULL),
(686, 686, 3, 'LOT#00002', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(687, 687, 3, 'LOT#00001', '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(688, 688, 3, 'LOT#00004', '97.0000', 'WH0000201', '0016010100010', NULL, NULL),
(689, 689, 3, 'LOT#00003', '5.0000', 'WH0000201', '0016010100010', NULL, NULL),
(690, 690, 3, 'LOT#00002', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(691, 691, 3, 'LOT#00003', '80.0000', 'WH0000201', '0016010100010', NULL, NULL),
(692, 692, 3, 'LOT#00001', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(693, 693, 3, 'LOT#00002', '10.0000', 'WH0000201', '0016010100010', NULL, NULL),
(694, 694, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(695, 695, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(696, 696, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(697, 697, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(698, 698, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(699, 699, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(700, 700, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(701, 701, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(702, 702, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(703, 703, 3, NULL, '0.0000', 'WH0000201', '0016010100010', NULL, NULL),
(704, 704, 4, 'LOT#00001', '100.0000', 'WH0000101', '0016010100010', NULL, NULL),
(705, 705, 4, 'LOT#00001', '100.0000', 'WH0000101', '0016010100010', NULL, NULL),
(706, 706, 4, 'LOT#00001', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(707, 707, 4, 'LOT#00001', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(708, 708, 4, 'LOT#00002', '3.0000', 'WH0000101', '0016010100010', NULL, NULL),
(709, 709, 4, 'LOT#00002', '3.0000', 'WH0000101', '0016010100010', NULL, NULL),
(710, 710, 4, 'LOT#00004', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(711, 711, 4, 'LOT#00004', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(712, 712, 4, 'LOT#00003', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(713, 713, 4, 'LOT#00003', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(714, 714, 4, 'LOT#00005', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(715, 715, 4, 'LOT#00005', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(716, 716, 4, 'LOT#00007', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(717, 717, 4, 'LOT#00007', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(718, 718, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(719, 719, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(720, 720, 4, 'LOT#00010', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(721, 721, 4, 'LOT#00010', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(722, 722, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(723, 723, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(724, 724, 4, 'LOT#00006', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(725, 725, 4, 'LOT#00006', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(726, 726, 4, 'LOT#00007', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(727, 727, 4, 'LOT#00007', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(728, 728, 4, 'LOT#00001', '3.0000', 'WH0000101', '0016010100010', NULL, NULL),
(729, 729, 4, 'LOT#00001', '3.0000', 'WH0000101', '0016010100010', NULL, NULL),
(730, 730, 4, 'LOT#00008', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(731, 731, 4, 'LOT#00008', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(732, 732, 4, 'LOT#00001', '97.0000', 'WH0000101', '0016010100010', NULL, NULL),
(733, 733, 4, 'LOT#00001', '97.0000', 'WH0000101', '0016010100010', NULL, NULL),
(734, 734, 4, 'LOT#00003', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(735, 735, 4, 'LOT#00003', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(736, 736, 4, 'LOT#00008', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(737, 737, 4, 'LOT#00008', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(738, 738, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(739, 739, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(740, 740, 4, 'LOT#00003', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(741, 741, 4, 'LOT#00003', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(742, 742, 4, 'LOT#00002', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(743, 743, 4, 'LOT#00002', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(744, 744, 4, 'LOT#00005', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(745, 745, 4, 'LOT#00005', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(746, 746, 4, 'LOT#00006', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(747, 747, 4, 'LOT#00006', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(748, 748, 4, '1', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(749, 749, 4, '1', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(750, 750, 4, 'LOT#00006', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(751, 751, 4, 'LOT#00006', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(752, 752, 4, 'LOT#00003', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(753, 753, 4, 'LOT#00003', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(754, 754, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(755, 755, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(756, 756, 4, 'LOT#00007', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(757, 757, 4, 'LOT#00007', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(758, 758, 4, 'LOT#00007', '160046.0000', 'WH0000101', '0016010100010', NULL, NULL),
(759, 759, 4, 'LOT#00007', '160046.0000', 'WH0000101', '0016010100010', NULL, NULL),
(760, 760, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(761, 761, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(762, 762, 4, 'LOT#00002', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(763, 763, 4, 'LOT#00002', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(764, 764, 4, 'LOT#00004', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(765, 765, 4, 'LOT#00004', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(766, 766, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(767, 767, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(768, 768, 4, 'LOT#00001', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(769, 769, 4, 'LOT#00001', '2.0000', 'WH0000101', '0016010100010', NULL, NULL),
(770, 770, 4, 'LOT#00009', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(771, 771, 4, 'LOT#00009', '10.0000', 'WH0000101', '0016010100010', NULL, NULL),
(772, 772, 4, 'LOT#00006', '30042.0000', 'WH0000101', '0016010100010', NULL, NULL),
(773, 773, 4, 'LOT#00006', '30042.0000', 'WH0000101', '0016010100010', NULL, NULL),
(774, 774, 4, 'LOT#00004', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(775, 775, 4, 'LOT#00004', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(776, 776, 4, 'LOT#00004', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(777, 777, 4, 'LOT#00004', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(778, 778, 4, 'LOT#00001', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(779, 779, 4, 'LOT#00001', '1.0000', 'WH0000101', '0016010100010', NULL, NULL),
(780, 780, 4, 'LOT#00005', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(781, 781, 4, 'LOT#00005', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(782, 782, 4, 'LOT#00005', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(783, 783, 4, 'LOT#00005', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(784, 784, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(785, 785, 4, 'LOT#00002', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(786, 786, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(787, 787, 4, 'LOT#00001', '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(788, 788, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(789, 789, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(790, 790, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(791, 791, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(792, 792, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(793, 793, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(794, 794, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(795, 795, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(796, 796, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(797, 797, 4, NULL, '0.0000', 'WH0000101', '0016010100010', NULL, NULL),
(798, 798, 5, 'LOT#00001', '10.0000', 'WH0000501', '0016010100010', NULL, NULL),
(799, 799, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(800, 800, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(801, 801, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(802, 802, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(803, 803, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(804, 804, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(805, 805, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(806, 806, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(807, 807, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(808, 808, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(809, 809, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(810, 810, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(811, 811, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(812, 812, 5, NULL, '0.0000', 'WH0000501', '0016010100010', NULL, NULL),
(813, 813, 6, 'LOT#00002', '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(814, 814, 6, 'LOT#00001', '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(815, 815, 6, 'LOT#00001', '10.0000', 'WH0000401', '0016010100010', NULL, NULL),
(816, 816, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(817, 817, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(818, 818, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(819, 819, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(820, 820, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(821, 821, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(822, 822, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(823, 823, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(824, 824, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(825, 825, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(826, 826, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(827, 827, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(828, 828, 6, NULL, '0.0000', 'WH0000401', '0016010100010', NULL, NULL),
(829, 829, 7, 'LOT#00001', '10.0000', 'WH0000301', '0016010100010', NULL, NULL),
(830, 830, 7, 'LOT#00002', '10.0000', 'WH0000301', '0016010100010', NULL, NULL),
(831, 831, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(832, 832, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(833, 833, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(834, 834, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(835, 835, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(836, 836, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(837, 837, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(838, 838, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(839, 839, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(840, 840, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(841, 841, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(842, 842, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(843, 843, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(844, 844, 7, NULL, '0.0000', 'WH0000301', '0016010100010', NULL, NULL),
(1024, 504, 1, NULL, '49.0000', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_credit_voucher`
--
ALTER TABLE `sma_credit_voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_paypal`
--
ALTER TABLE `sma_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_settings`
--
ALTER TABLE `sma_pos_settings`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sma_settings`
--
ALTER TABLE `sma_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sma_skrill`
--
ALTER TABLE `sma_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_subcategories`
--
ALTER TABLE `sma_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_till`
--
ALTER TABLE `sma_till`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  ADD KEY `group_id_2` (`group_id`,`company_id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_categories`
--
ALTER TABLE `sma_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `sma_costing`
--
ALTER TABLE `sma_costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `sma_credit_voucher`
--
ALTER TABLE `sma_credit_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sma_groups`
--
ALTER TABLE `sma_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_payments`
--
ALTER TABLE `sma_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_products`
--
ALTER TABLE `sma_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=845;
--
-- AUTO_INCREMENT for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1025;
--
-- AUTO_INCREMENT for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sma_sales`
--
ALTER TABLE `sma_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `sma_subcategories`
--
ALTER TABLE `sma_subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `sma_till`
--
ALTER TABLE `sma_till`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_users`
--
ALTER TABLE `sma_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `sma_variants`
--
ALTER TABLE `sma_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1025;
--
-- AUTO_INCREMENT for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
